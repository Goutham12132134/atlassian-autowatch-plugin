define(['util/cachedRequest'], function(CachedRequest) {
    module("util/cachedRequest", {
        setup: function() {
            this.cr = new CachedRequest({
                url: "test"
            });
        }
    });

    test("Test core methods", function(){
        notEqual( this.cr , undefined, "IT WORKS!!!");
        ok( true, "Another trivial test");
    });
});