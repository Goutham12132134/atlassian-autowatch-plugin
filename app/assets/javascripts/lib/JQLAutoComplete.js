// this is JIRA's JQL autocomplete control tweaked to work with Atlassian Connect
(function() {

/*
 * jQuery Form Plugin
 * version: 2.67 (12-MAR-2011)
 * @requires jQuery v1.3.2 or later
 *
 * Examples and documentation at: http://malsup.com/jquery/form/
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 */
(function(b){b.fn.ajaxSubmit=function(t){if(!this.length){a("ajaxSubmit: skipping submit process - no element selected");return this}if(typeof t=="function"){t={success:t}}var h=this.attr("action");var d=(typeof h==="string")?b.trim(h):"";if(d){d=(d.match(/^([^#]+)/)||[])[1]}d=d||window.location.href||"";t=b.extend(true,{url:d,type:this[0].getAttribute("method")||"GET",iframeSrc:/^https/i.test(window.location.href||"")?"javascript:false":"about:blank"},t);var u={};this.trigger("form-pre-serialize",[this,t,u]);if(u.veto){a("ajaxSubmit: submit vetoed via form-pre-serialize trigger");return this}if(t.beforeSerialize&&t.beforeSerialize(this,t)===false){a("ajaxSubmit: submit aborted via beforeSerialize callback");return this}var f,p,m=this.formToArray(t.semantic);if(t.data){t.extraData=t.data;for(f in t.data){if(t.data[f] instanceof Array){for(var i in t.data[f]){m.push({name:f,value:t.data[f][i]})}}else{p=t.data[f];p=b.isFunction(p)?p():p;m.push({name:f,value:p})}}}if(t.beforeSubmit&&t.beforeSubmit(m,this,t)===false){a("ajaxSubmit: submit aborted via beforeSubmit callback");return this}this.trigger("form-submit-validate",[m,this,t,u]);if(u.veto){a("ajaxSubmit: submit vetoed via form-submit-validate trigger");return this}var c=b.param(m);if(t.type.toUpperCase()=="GET"){t.url+=(t.url.indexOf("?")>=0?"&":"?")+c;t.data=null}else{t.data=c}var s=this,l=[];if(t.resetForm){l.push(function(){s.resetForm()})}if(t.clearForm){l.push(function(){s.clearForm()})}if(!t.dataType&&t.target){var r=t.success||function(){};l.push(function(n){var k=t.replaceTarget?"replaceWith":"html";b(t.target)[k](n).each(r,arguments)})}else{if(t.success){l.push(t.success)}}t.success=function(w,n,x){var v=t.context||t;for(var q=0,k=l.length;q<k;q++){l[q].apply(v,[w,n,x||s,s])}};var g=b("input:file",this).length>0;var e="multipart/form-data";var j=(s.attr("enctype")==e||s.attr("encoding")==e);if(t.iframe!==false&&(g||t.iframe||j)){if(t.closeKeepAlive){b.get(t.closeKeepAlive,o)}else{o()}}else{b.ajax(t)}this.trigger("form-submit-notify",[this,t]);return this;function o(){var v=s[0];if(b(":input[name=submit],:input[id=submit]",v).length){alert('Error: Form elements must not have name or id of "submit".');return}var B=b.extend(true,{},b.ajaxSettings,t);B.context=B.context||B;var E="jqFormIO"+(new Date().getTime()),z="_"+E;var w=b('<iframe id="'+E+'" name="'+E+'" src="'+B.iframeSrc+'" />');var A=w[0];w.css({position:"absolute",top:"-1000px",left:"-1000px"});var x={aborted:0,responseText:null,responseXML:null,status:0,statusText:"n/a",getAllResponseHeaders:function(){},getResponseHeader:function(){},setRequestHeader:function(){},abort:function(){a("aborting upload...");var n="aborted";this.aborted=1;w.attr("src",B.iframeSrc);x.error=n;B.error&&B.error.call(B.context,x,"error",n);I&&b.event.trigger("ajaxError",[x,B,n]);B.complete&&B.complete.call(B.context,x,"error")}};var I=B.global;if(I&&!b.active++){b.event.trigger("ajaxStart")}if(I){b.event.trigger("ajaxSend",[x,B])}if(B.beforeSend&&B.beforeSend.call(B.context,x,B)===false){if(B.global){b.active--}return}if(x.aborted){return}var H=0;var y=v.clk;if(y){var F=y.name;if(F&&!y.disabled){B.extraData=B.extraData||{};B.extraData[F]=y.value;if(y.type=="image"){B.extraData[F+".x"]=v.clk_x;B.extraData[F+".y"]=v.clk_y}}}function G(){var O=s.attr("target"),M=s.attr("action");v.setAttribute("target",E);if(v.getAttribute("method")!="POST"){v.setAttribute("method","POST")}if(v.getAttribute("action")!=B.url){v.setAttribute("action",B.url)}if(!B.skipEncodingOverride){s.attr({encoding:"multipart/form-data",enctype:"multipart/form-data"})}if(B.timeout){setTimeout(function(){H=true;D()},B.timeout)}var N=[];try{if(B.extraData){for(var P in B.extraData){N.push(b('<input type="hidden" name="'+P+'" value="'+B.extraData[P]+'" />').appendTo(v)[0])}}w.appendTo("body");A.attachEvent?A.attachEvent("onload",D):A.addEventListener("load",D,false);v.submit()}finally{v.setAttribute("action",M);if(O){v.setAttribute("target",O)}else{s.removeAttr("target")}b(N).remove()}}if(B.forceSync){G()}else{setTimeout(G,10)}var K,L,J=50;function D(){if(x.aborted){return}var R=A.contentWindow?A.contentWindow.document:A.contentDocument?A.contentDocument:A.document;if(!R||R.location.href==B.iframeSrc){return}A.detachEvent?A.detachEvent("onload",D):A.removeEventListener("load",D,false);var N=true;try{if(H){throw"timeout"}var S=B.dataType=="xml"||R.XMLDocument||b.isXMLDoc(R);a("isXml="+S);if(!S&&window.opera&&(R.body==null||R.body.innerHTML=="")){if(--J){a("requeing onLoad callback, DOM not available");setTimeout(D,250);return}}x.responseText=R.body?R.body.innerHTML:R.documentElement?R.documentElement.innerHTML:null;x.responseXML=R.XMLDocument?R.XMLDocument:R;x.getResponseHeader=function(U){var T={"content-type":B.dataType};return T[U]};var Q=/(json|script)/.test(B.dataType);if(Q||B.textarea){var M=R.getElementsByTagName("textarea")[0];if(M){x.responseText=M.value}else{if(Q){var P=R.getElementsByTagName("pre")[0];var n=R.getElementsByTagName("body")[0];if(P){x.responseText=P.textContent}else{if(n){x.responseText=n.innerHTML}}}}}else{if(B.dataType=="xml"&&!x.responseXML&&x.responseText!=null){x.responseXML=C(x.responseText)}}K=k(x,B.dataType,B)}catch(O){a("error caught:",O);N=false;x.error=O;B.error&&B.error.call(B.context,x,"error",O);I&&b.event.trigger("ajaxError",[x,B,O])}if(x.aborted){a("upload aborted");N=false}if(N){B.success&&B.success.call(B.context,K,"success",x);I&&b.event.trigger("ajaxSuccess",[x,B])}I&&b.event.trigger("ajaxComplete",[x,B]);if(I&&!--b.active){b.event.trigger("ajaxStop")}B.complete&&B.complete.call(B.context,x,N?"success":"error");setTimeout(function(){w.removeData("form-plugin-onload");w.remove();x.responseXML=null},100)}var C=b.parseXML||function(n,M){if(window.ActiveXObject){M=new ActiveXObject("Microsoft.XMLDOM");M.async="false";M.loadXML(n)}else{M=(new DOMParser()).parseFromString(n,"text/xml")}return(M&&M.documentElement&&M.documentElement.nodeName!="parsererror")?M:null};var q=b.parseJSON||function(n){return window["eval"]("("+n+")")};var k=function(Q,O,N){var M=Q.getResponseHeader("content-type")||"",n=O==="xml"||!O&&M.indexOf("xml")>=0,P=n?Q.responseXML:Q.responseText;if(n&&P.documentElement.nodeName==="parsererror"){b.error&&b.error("parsererror")}if(N&&N.dataFilter){P=N.dataFilter(P,O)}if(typeof P==="string"){if(O==="json"||!O&&M.indexOf("json")>=0){P=q(P)}else{if(O==="script"||!O&&M.indexOf("javascript")>=0){b.globalEval(P)}}}return P}}};b.fn.ajaxForm=function(c){if(this.length===0){var d={s:this.selector,c:this.context};if(!b.isReady&&d.s){a("DOM not ready, queuing ajaxForm");b(function(){b(d.s,d.c).ajaxForm(c)});return this}a("terminating; zero elements found by selector"+(b.isReady?"":" (DOM not ready)"));return this}return this.ajaxFormUnbind().bind("submit.form-plugin",function(f){if(!f.isDefaultPrevented()){f.preventDefault();b(this).ajaxSubmit(c)}}).bind("click.form-plugin",function(j){var i=j.target;var g=b(i);if(!(g.is(":submit,input:image"))){var f=g.closest(":submit");if(f.length==0){return}i=f[0]}var h=this;h.clk=i;if(i.type=="image"){if(j.offsetX!=undefined){h.clk_x=j.offsetX;h.clk_y=j.offsetY}else{if(typeof b.fn.offset=="function"){var k=g.offset();h.clk_x=j.pageX-k.left;h.clk_y=j.pageY-k.top}else{h.clk_x=j.pageX-i.offsetLeft;h.clk_y=j.pageY-i.offsetTop}}}setTimeout(function(){h.clk=h.clk_x=h.clk_y=null},100)})};b.fn.ajaxFormUnbind=function(){return this.unbind("submit.form-plugin click.form-plugin")};b.fn.formToArray=function(q){var p=[];if(this.length===0){return p}var d=this[0];var g=q?d.getElementsByTagName("*"):d.elements;if(!g){return p}var k,h,f,r,e,m,c;for(k=0,m=g.length;k<m;k++){e=g[k];f=e.name;if(!f){continue}if(q&&d.clk&&e.type=="image"){if(!e.disabled&&d.clk==e){p.push({name:f,value:b(e).val()});p.push({name:f+".x",value:d.clk_x},{name:f+".y",value:d.clk_y})}continue}r=b.fieldValue(e,true);if(r&&r.constructor==Array){for(h=0,c=r.length;h<c;h++){p.push({name:f,value:r[h]})}}else{if(r!==null&&typeof r!="undefined"){p.push({name:f,value:r})}}}if(!q&&d.clk){var l=b(d.clk),o=l[0];f=o.name;if(f&&!o.disabled&&o.type=="image"){p.push({name:f,value:l.val()});p.push({name:f+".x",value:d.clk_x},{name:f+".y",value:d.clk_y})}}return p};b.fn.formSerialize=function(c){return b.param(this.formToArray(c))};b.fn.fieldSerialize=function(d){var c=[];this.each(function(){var h=this.name;if(!h){return}var f=b.fieldValue(this,d);if(f&&f.constructor==Array){for(var g=0,e=f.length;g<e;g++){c.push({name:h,value:f[g]})}}else{if(f!==null&&typeof f!="undefined"){c.push({name:this.name,value:f})}}});return b.param(c)};b.fn.fieldValue=function(h){for(var g=[],e=0,c=this.length;e<c;e++){var f=this[e];var d=b.fieldValue(f,h);if(d===null||typeof d=="undefined"||(d.constructor==Array&&!d.length)){continue}d.constructor==Array?b.merge(g,d):g.push(d)}return g};b.fieldValue=function(c,j){var e=c.name,p=c.type,q=c.tagName.toLowerCase();if(j===undefined){j=true}if(j&&(!e||c.disabled||p=="reset"||p=="button"||(p=="checkbox"||p=="radio")&&!c.checked||(p=="submit"||p=="image")&&c.form&&c.form.clk!=c||q=="select"&&c.selectedIndex==-1)){return null}if(q=="select"){var k=c.selectedIndex;if(k<0){return null}var m=[],d=c.options;var g=(p=="select-one");var l=(g?k+1:d.length);for(var f=(g?k:0);f<l;f++){var h=d[f];if(h.selected){var o=h.value;if(!o){o=(h.attributes&&h.attributes.value&&!(h.attributes.value.specified))?h.text:h.value}if(g){return o}m.push(o)}}return m}return b(c).val()};b.fn.clearForm=function(){return this.each(function(){b("input,select,textarea",this).clearFields()})};b.fn.clearFields=b.fn.clearInputs=function(){return this.each(function(){var d=this.type,c=this.tagName.toLowerCase();if(d=="text"||d=="password"||c=="textarea"){this.value=""}else{if(d=="checkbox"||d=="radio"){this.checked=false}else{if(c=="select"){this.selectedIndex=-1}}}})};b.fn.resetForm=function(){return this.each(function(){if(typeof this.reset=="function"||(typeof this.reset=="object"&&!this.reset.nodeType)){this.reset()}})};b.fn.enable=function(c){if(c===undefined){c=true}return this.each(function(){this.disabled=!c})};b.fn.selected=function(c){if(c===undefined){c=true}return this.each(function(){var d=this.type;if(d=="checkbox"||d=="radio"){this.checked=c}else{if(this.tagName.toLowerCase()=="option"){var e=b(this).parent("select");if(c&&e[0]&&e[0].type=="select-one"){e.find("option").selected(false)}this.selected=c}}})};function a(){if(b.fn.ajaxSubmit.debug){var c="[jquery.form] "+Array.prototype.join.call(arguments,"");if(window.console&&window.console.log){window.console.log(c)}else{if(window.opera&&window.opera.postError){window.opera.postError(c)}}}}})(jQuery);
(function(){var _after=1;var _afterThrow=2;var _afterFinally=3;var _before=4;var _around=5;var _intro=6;var _regexEnabled=true;var _arguments="arguments";var _undef="undefined";var getType=(function(){var toString=Object.prototype.toString,toStrings={},nodeTypes={1:"element",3:"textnode",9:"document",11:"fragment"},types="Arguments Array Boolean Date Document Element Error Fragment Function NodeList Null Number Object RegExp String TextNode Undefined Window".split(" ");for(var i=types.length;i--;){var type=types[i],constructor=window[type];if(constructor){try{toStrings[toString.call(new constructor)]=type.toLowerCase()}catch(e){}}}return function(item){return item==null&&(item===undefined?_undef:"null")||item.nodeType&&nodeTypes[item.nodeType]||typeof item.length=="number"&&(item.callee&&_arguments||item.alert&&"window"||item.item&&"nodelist")||toStrings[toString.call(item)]}})();var isFunc=function(obj){return getType(obj)=="function"};var weaveOne=function(source,method,advice){var old=source[method];if(advice.type!=_intro&&!isFunc(old)){var oldObject=old;old=function(){var code=arguments.length>0?_arguments+"[0]":"";for(var i=1;i<arguments.length;i++){code+=","+_arguments+"["+i+"]"}return eval("oldObject("+code+");")}}var aspect;if(advice.type==_after||advice.type==_afterThrow||advice.type==_afterFinally){aspect=function(){var returnValue,exceptionThrown=null;try{returnValue=old.apply(this,arguments)}catch(e){exceptionThrown=e}if(advice.type==_after){if(exceptionThrown==null){returnValue=advice.value.apply(this,[returnValue,method])}else{throw exceptionThrown}}else{if(advice.type==_afterThrow&&exceptionThrown!=null){returnValue=advice.value.apply(this,[exceptionThrown,method])}else{if(advice.type==_afterFinally){returnValue=advice.value.apply(this,[returnValue,exceptionThrown,method])}}}return returnValue}}else{if(advice.type==_before){aspect=function(){advice.value.apply(this,[arguments,method]);return old.apply(this,arguments)}}else{if(advice.type==_intro){aspect=function(){return advice.value.apply(this,arguments)}}else{if(advice.type==_around){aspect=function(){var invocation={object:this,args:Array.prototype.slice.call(arguments)};return advice.value.apply(invocation.object,[{arguments:invocation.args,method:method,proceed:function(){return old.apply(invocation.object,invocation.args)}}])}}}}}aspect.unweave=function(){source[method]=old;pointcut=source=aspect=old=null};source[method]=aspect;return aspect};var search=function(source,pointcut,advice){var methods=[];for(var method in source){var item=null;try{item=source[method]}catch(e){}if(item!=null&&method.match(pointcut.method)&&isFunc(item)){methods[methods.length]={source:source,method:method,advice:advice}}}return methods};var weave=function(pointcut,advice){var source=typeof(pointcut.target.prototype)!=_undef?pointcut.target.prototype:pointcut.target;var advices=[];if(advice.type!=_intro&&typeof(source[pointcut.method])==_undef){var methods=search(pointcut.target,pointcut,advice);if(methods.length==0){methods=search(source,pointcut,advice)}for(var i in methods){advices[advices.length]=weaveOne(methods[i].source,methods[i].method,methods[i].advice)}}else{advices[0]=weaveOne(source,pointcut.method,advice)}return _regexEnabled?advices:advices[0]};jQuery.aop={after:function(pointcut,advice){return weave(pointcut,{type:_after,value:advice})},afterThrow:function(pointcut,advice){return weave(pointcut,{type:_afterThrow,value:advice})},afterFinally:function(pointcut,advice){return weave(pointcut,{type:_afterFinally,value:advice})},before:function(pointcut,advice){return weave(pointcut,{type:_before,value:advice})},around:function(pointcut,advice){return weave(pointcut,{type:_around,value:advice})},introduction:function(pointcut,advice){return weave(pointcut,{type:_intro,value:advice})},setup:function(settings){_regexEnabled=settings.regexMatch}}})();
if(window.Raphael){Raphael.shadow=function(l,k,m,f,r){r=r||{};var b=jQuery(r.target),o=jQuery("<div/>",{"class":"aui-shadow"}),a=r.shadow||r.color||"#000",q=r.size*10||0,p=r.offsetSize||3,n=r.zindex||0,i=r.radius||0,g="0.4",d=r.blur||3,c,j,e;m+=q+2*d;f+=q+2*d;if(Raphael.shadow.BOX_SHADOW_SUPPORT){b.addClass("aui-box-shadow");return o.addClass("hidden")}if(l===0&&k===0&&b.length>0){e=b.offset();l=p-d+e.left;k=p-d+e.top}if(jQuery.browser.msie&&jQuery.browser.version<"9"){a="#f0f0f0";g="0.2"}o.css({position:"absolute",left:l,top:k,width:m,height:f,zIndex:n});if(b.length>0){o.appendTo(document.body);c=Raphael(o[0],m,f,i)}else{c=Raphael(l,k,m,f,i)}c.canvas.style.position="absolute";j=c.rect(d,d,m-2*d,f-2*d).attr({fill:a,stroke:a,blur:""+d,opacity:g});return o};Raphael.shadow.BOX_SHADOW_SUPPORT=(function(){var c=document.documentElement.style;var a=["boxShadow","MozBoxShadow","WebkitBoxShadow","msBoxShadow"];for(var b=0;b<a.length;b++){if(a[b] in c){return true}}return false})()};
jQuery.os={};var jQueryOSplatform=navigator.platform.toLowerCase();jQuery.os.windows=(jQueryOSplatform.indexOf("win")!=-1);jQuery.os.mac=(jQueryOSplatform.indexOf("mac")!=-1);jQuery.os.linux=(jQueryOSplatform.indexOf("linux")!=-1);
(function(d){d.hotkeys={version:"0.8",specialKeys:{8:"backspace",9:"tab",13:"return",16:"shift",17:"ctrl",18:"alt",19:"pause",20:"capslock",27:"esc",32:"space",33:"pageup",34:"pagedown",35:"end",36:"home",37:"left",38:"up",39:"right",40:"down",45:"insert",46:"del",91:"meta",96:"0",97:"1",98:"2",99:"3",100:"4",101:"5",102:"6",103:"7",104:"8",105:"9",106:"*",107:"+",109:"-",110:".",111:"/",112:"f1",113:"f2",114:"f3",115:"f4",116:"f5",117:"f6",118:"f7",119:"f8",120:"f9",121:"f10",122:"f11",123:"f12",144:"numlock",145:"scroll",188:",",190:".",191:"/",224:"meta",219:"[",221:"]"},keypressKeys:["<",">","?"],shiftNums:{"`":"~","1":"!","2":"@","3":"#","4":"$","5":"%","6":"^","7":"&","8":"*","9":"(","0":")","-":"_","=":"+",";":":","'":'"',",":"<",".":">","/":"?","\\":"|"}};d.each(d.hotkeys.keypressKeys,function(e,f){d.hotkeys.shiftNums[f]=f});function a(e){this.num=0;this.timer=e>0?e:false}a.prototype.val=function(){return this.num};a.prototype.inc=function(){if(this.timer){clearTimeout(this.timeout);this.timeout=setTimeout(d.proxy(a.prototype.reset,this),this.timer)}this.num++};a.prototype.reset=function(){if(this.timer){clearTimeout(this.timeout)}this.num=0};function c(g){if(!(d.isPlainObject(g.data)||d.isArray(g.data)||typeof g.data==="string")){return}var f=g.handler,e={timer:700};(function(h){if(typeof h==="string"){e.combo=[h]}else{if(d.isArray(h)){e.combo=h}else{d.extend(e,h)}}e.combo=d.map(e.combo,function(i){return i.toLowerCase()})})(g.data);g.index=new a(e.timer);g.handler=function(m){if(this!==m.target&&(/textarea|select|input/i.test(m.target.nodeName))){return}var j=m.type!=="keypress"?d.hotkeys.specialKeys[m.which]:null,n=String.fromCharCode(m.which).toLowerCase(),k,l="",i={};if(m.altKey&&j!=="alt"){l+="alt+"}if(m.ctrlKey&&j!=="ctrl"){l+="ctrl+"}if(m.metaKey&&!m.ctrlKey&&j!=="meta"){l+="meta+"}if(m.shiftKey&&j!=="shift"){l+="shift+"}if(j){i[l+j]=true}if(n){i[l+n]=true}if(/shift+/.test(l)){i[l.replace("shift+","")+d.hotkeys.shiftNums[(j||n)]]=true}var h=g.index,o=e.combo;if(b(o[h.val()],i)){if(h.val()===o.length-1){h.reset();return f.apply(this,arguments)}else{h.inc()}}else{h.reset();if(b(o[0],i)){h.inc()}}}}function b(h,f){var j=h.split(" ");for(var g=0,e=j.length;g<e;g++){if(f[j[g]]){return true}}return false}d.each(["keydown","keyup","keypress"],function(){d.event.special[this]={add:c}})})(jQuery);
jQuery.fn.moveTo=function(c){var g={transition:false,scrollOffset:35};var e=jQuery.extend(g,c),a=this,d=a.offset().top,b;if((jQuery(window).scrollTop()+jQuery(window).height()-this.outerHeight()<d||jQuery(window).scrollTop()+e.scrollOffset>d)&&jQuery(window).height()>e.scrollOffset){if(jQuery(window).scrollTop()+e.scrollOffset>d){b=d-(jQuery(window).height()-this.outerHeight())+e.scrollOffset}else{b=d-e.scrollOffset}if(!jQuery.fn.moveTo.animating&&e.transition){jQuery(document).trigger("moveToStarted",this);jQuery.fn.moveTo.animating=true;jQuery("html,body").animate({scrollTop:b},1000,function(){jQuery(document).trigger("moveToFinished",a);delete jQuery.fn.moveTo.animating});return this}else{var f=jQuery("html, body");if(f.is(":animated")){f.stop();delete jQuery.fn.moveTo.animating}jQuery(document).trigger("moveToStarted");jQuery(window).scrollTop(b);setTimeout(function(){jQuery(document).trigger("moveToFinished",a)},100);return this}}jQuery(document).trigger("moveToFinished",this);return this};

var JIRA = {};

//////////////////////////////////////////////////////
/////////////// Object.create ////////////////////////
//////////////////////////////////////////////////////

/**
 * @function begetObject
 * @return {Object} cloned object
 */
function begetObject(obj) {
    var F = function() {};
    F.prototype = obj;
    return new F();
}

//////////////////////////////////////////////////////
/////////////// JIRA.Dropdown ////////////////////////
//////////////////////////////////////////////////////

/**
 * Creates a dropdown list from a JSON object
 *
 * @constructor JIRA.Dropdown
 * @deprecated
 * @author Scott Harwood
 *
 * NOTE: Please use @see AJS.Dropdown instead of this for future dropdown implementations.
 */
JIRA.Dropdown = function() {

    // private

    var instances = [];

    return {

        // public

        /**
         * Adds this instance to private var <em>instances</em>
         * This reference can be used to access all instances
         * @function {public} addInstance
         */
        addInstance: function() {
            instances.push(this);
        },


        /**
         * Calls the hideList method on all instances of <em>dropdown</em>
         * @function {public} hideInstances
         */
        hideInstances: function() {
            var that = this;
            jQuery(instances).each(function(){
                if (that !== this) {
                    this.hideDropdown();
                }
            });
        },


        getHash: function () {
            if (!this.hash) {
                this.hash = {
                    container: this.dropdown,
                    hide: this.hideDropdown,
                    show: this.displayDropdown
                };
            }
            return this.hash;
        },

        /**
         * Calls <em>hideInstances</em> method to hide all other dropdowns.
         * Adds <em>active</em> class to <em>dropdown</em> and styles to make it visible.
         * @function {public} displayDropdown
         */
        displayDropdown: function() {
            if (JIRA.Dropdown.current === this) {
                return;
            }

            this.hideInstances();
            JIRA.Dropdown.current = this;
            this.dropdown.css({display: "block"});

            this.displayed = true;

            var dd = this.dropdown;
        },

        /**
         *
         * Removes <em>active</em> class from <em>dropdown</em> and styles to make it hidden.
         * @function {public} hideDropdown
         */
        hideDropdown: function() {
            if (this.displayed === false) {
                return;
            }

            JIRA.Dropdown.current = null;
            this.dropdown.css({display: "none"});

            this.displayed = false;
        },

        /**
         * Initialises instance by, applying primary handler, user options and a Internet Explorer hack.
         * function {public} init
         * @param {HTMLElement} trigger
         * @param {HTMLElement} dropdown
         */
        init: function(trigger, dropdown) {

            var that = this;

            this.addInstance(this);
            this.dropdown = jQuery(dropdown);

            this.dropdown.css({display: "none"});

            // hide dropdown on tab
            jQuery(document).keydown(function(e){
                if(e.keyCode === 9) {
                    that.hideDropdown();
                }
            });

            // this instance is triggered by a method call
            if (trigger.target) {
                jQuery.aop.before(trigger, function(){
                    if (!that.displayed) {
                        that.displayDropdown();
                    }
                });

                // this instance is triggered by a click event
            } else {
                that.dropdown.css("top",jQuery(trigger).outerHeight() + "px");
                trigger.click(function(e){
                    if (!that.displayed) {
                        that.displayDropdown();
                        e.stopPropagation();
                        // lets not follow the link (if it is a link)
                    } else {
                        that.hideDropdown();
                    }
                    e.preventDefault();
                });
            }

            // hide dropdown when click anywhere other than on this instance
            jQuery(document.body).click(function(){
                if (that.displayed) {
                    that.hideDropdown();
                }
            });
        }
    };

}();

/**
 * Standard dropdown constructor
 * @constucter Standard
 * @param {HTMLElement} trigger
 * @param {HTMLElement} dropdown
 * @return {Object} - instance
 */
JIRA.Dropdown.AutoComplete = function(trigger, dropdown) {

    var that = begetObject(JIRA.Dropdown);

    that.init = function(trigger, dropdown) {

        this.addInstance(this);
        this.dropdown = jQuery(dropdown).click(function(e){
            // lets not hide dropdown when we click on it
            e.stopPropagation();
        });
        this.dropdown.css({display: "none"});

        // this instance is triggered by a method call
        if (trigger.target) {
            jQuery.aop.before(trigger, function(){
                if (!that.displayed) {
                    that.displayDropdown();
                }
            });

            // this instance is triggered by a click event
        } else {
            trigger.click(function(e){
                if (!that.displayed) {
                    that.displayDropdown();
                    e.stopPropagation();
                }
            });
        }

        // hide dropdown when click anywhere other than on this instance
        jQuery(document.body).click(function(){
            if (that.displayed) {
                that.hideDropdown();
            }
        });
    };

    that.init(trigger, dropdown);

    return that;
};

//////////////////////////////////////////////////////
/////////////// JIRA.AutoComplete /////////////////
//////////////////////////////////////////////////////

JIRA.AutoComplete = function() {

    var inFocus;

    /**
     * Calls a callback after specified delay
     * @method {private} delay
     * @param {Number} l - length of delay in <em>seconds</em>
     * @param {Function} callback - function to call after delay
     */
    var delay = function(callback,l) {
        if (delay.t) {
            clearTimeout(delay.t);
            delay.t = undefined;
        }
        delay.t = setTimeout(callback, l * 1000);
    };

    var INVALID_KEYS = {
        9: true,
        13: true,
        14: true,
        25: true,
        27: true,
        38: true,
        40: true,
        224: true
    };

    return {

        /**
         * Checks whether a saved version (cached) of the request exists, if not performs a request and saves response,
         * then dispatches saved response to <em>renderSuggestions</em> method.
         *
         * @method {public} dispatcher
         */
        dispatcher: function() {},


        /**
         * Gets cached response
         *
         * @method {public} getSavedResponse
         * @param {String} val
         * @returns {Object}
         */
        getSavedResponse: function() {},

        /**
         * Saves response
         *
         * @method {public} saveResponse
         * @param {String} val
         * @param {Object} response
         */
        saveResponse: function() {},

        /**
         * Called to render suggestions. Used to define interface only.
         * Rendering is difficult to make generic, best to leave this to extending prototypes.
         *
         * @method {public} renderSuggestions
         * @param {Object} res - results object
         */
        renderSuggestions: function() {},

        /**
         * Disables autocomplete. Useful for shared inputs.
         * i.e The selection of a radio button may disable the instance
         * @method {Public} disable
         */
        disable: function() {
            this.disabled = true;
        },

        /**
         * Enables autocomplete. Useful for shared inputs.
         * i.e The selection of a radio button may disable the instance
         * @method {Public} enable
         */
        enable: function() {
            this.disabled = false;
        },

        /**
         * Sets instance variables from options object
         * to do: make function create getters and setters
         * @method {public} set
         * @param {Object} options
         */
        set: function(options) {
            for (var name in options) {
                // safeguard to stop looping up the inheritance chain
                if (options.hasOwnProperty(name)) {
                    this[name] = options[name];
                }
            }
        },

        /**
         * Adds value to input field
         * @method {public} completeField
         * @param {String} value
         */
        completeField: function(value) {
            if (value) {
                this.field.val(value).focus();
                this.field.trigger("change");
            }
        },

        /**
         * Returns the text from the start of the field up to the end of
         * the position where suggestions are generated from.
         */
        textToSuggestionCursorPosition: function () {
            return this.field.val();
        },


        /**
         * An ajax request filter that only allows one request at a time. If there is another it will abort then issue
         * the new request.
         *
         * @param options - jQuery formatted ajax options
         */
        _makeRequest: function (options) {

            var that = this,
                requestParams = AJS.$.extend({}, options);

            // if we have we are still waiting for an old request, lets abort it as we are firing a new
            if (this.pendingRequest) {
                this.pendingRequest.abort();
            }

            requestParams.complete = function () {
                that.pendingRequest = null;
            };

            requestParams.error = function (xhr) {

                // We abort stale requests and this subsequently throws an error so we need to check if the request is aborted first.
                // We detect this using xhr.aborted property for native XHR requests but for "Microsoft.XMLHTTP" we use the status code, which is 0.
                // Status code is set to 0 when it is an unknown error so sense to fail silently.
                if (!xhr.aborted && xhr.status !== 0 && options.error) {
                    options.error.apply(this, arguments);
                }
            };

            return this.pendingRequest = AP.request(requestParams);
        },

        /**
         * Allows users to navigate/select suggestions using the keyboard
         * @method {public} addSuggestionControls
         */
        addSuggestionControls: function(suggestionNodes) {

            // reference to this for closures
            var that = this;

            /**
             * Make sure the index is within the threshold
             * Looks ugly! Has to be a better way.
             * @method {private} evaluateIndex
             * @param {Integer} idx
             * @param {Integer} max
             * @return {Integer} valid threshold
             */
            var evaluateIndex = function(idx, max) {
                var minBoundary = (that.autoSelectFirst === false) ? -1 : 0;
                if (that.allowArrowCarousel) {
                    if (idx > max) {
                        return minBoundary;
                    } else if (idx < minBoundary) {
                        return max;
                    } else {
                        return idx;
                    }
                }
                else {
                    if (idx > max) {
                        return max;
                    } else if (idx < minBoundary) {
                        that.responseContainer.scrollTop(0);
                        return minBoundary;
                    } else {
                        return idx;
                    }
                }
            };

            /**
             * Highlights focused node and removes highlight from previous.
             * Actual highlight styles to come from css, adding and removing classes here.
             * @method {private} setActive
             * @param {Integer} idx - Index of node to be highlighted
             */
            var setActive = function(idx) {

                // if nothing is selected, select the first suggestion
                if (that.selectedIndex !== undefined && that.selectedIndex > -1) {
                    that.suggestionNodes[that.selectedIndex][0].removeClass("active");
                }
                that.selectedIndex = evaluateIndex(idx, that.suggestionNodes.length-1);
                if (that.selectedIndex > -1) {
                    that.suggestionNodes[that.selectedIndex][0].addClass("active");
                }
            };

            /**
             * Checks to see if there is actually a suggestion in focus before attempting to use it
             * @method {private} evaluateIfActive
             * @returns {boolean}
             */
            var evaluateIfActive = function() {
                return that.suggestionNodes && that.suggestionNodes[that.selectedIndex] &&
                    that.suggestionNodes[that.selectedIndex][0].hasClass("active");
            };


            /**
             * When the responseContainer (dropdown) is visible listen for keyboard events
             * that represent focus or selection.
             * @method {private} keyPressHandler
             * @param {Object} e - event object
             */
            var keyPressHandler = function(e) {
                // only use keyboard events if dropdown is visible
                if (that.responseContainer.is(":visible")) {
                    // if enter key is pressed check that there is a node selected, then hide dropdown and complete field
                    if (e.keyCode === 13) {
                        if (evaluateIfActive() && !that.pendingRequest) {
                            that.completeField(that.suggestionNodes[that.selectedIndex][1]);
                        }
                        e.preventDefault();
                        // hack - stop propagation to prevent dialog from submitting. Looking for eg JIRA.Dropdown.current doesn't work.
                        e.stopPropagation();
                    }
                }
            };

            /**
             * sets focus on suggestion nodes using the "up" and "down" arrows
             * These events need to be fired on mouseup as modifier keys don't register on keypress
             * @method {private} keyUpHandler
             * @param {Object} e - event object
             */
            var keyboardNavigateHandler = function(e) {

                // only use keyboard events if dropdown is visible
                if (that.responseContainer.is(":visible")) {

                    // keep cursor inside input field
                    if (that.field[0] !== document.activeElement){
                        that.field.focus();
                    }
                    // move selection down when down arrow is pressed
                    if (e.keyCode === 40) {
                        setActive(that.selectedIndex + 1);
                        if (that.selectedIndex >= 0) {
                            // move selection up when up arrow is pressed
                            var containerHeight = that.responseContainer.height();
                            var bottom = that.suggestionNodes[that.selectedIndex][0].position().top + that.suggestionNodes[that.selectedIndex][0].outerHeight() ;

                            if (bottom - containerHeight > 0){
                                that.responseContainer.scrollTop(that.responseContainer.scrollTop() + bottom - containerHeight + 2);
                            }
                        } else {
                            that.responseContainer.scrollTop(0);
                        }
                        e.preventDefault();
                    } else if (e.keyCode === 38) {
                        setActive(that.selectedIndex-1);
                        if (that.selectedIndex >= 0) {
                            // if tab key is pressed check that there is a node selected, then hide dropdown and complete field
                            var top = that.suggestionNodes[that.selectedIndex][0].position().top;
                            if (top < 0){
                                that.responseContainer.scrollTop(that.responseContainer.scrollTop() + top - 2);
                            }
                        }
                        e.preventDefault();
                    } else if (e.keyCode === 9) {
                        if (evaluateIfActive()) {
                            that.completeField(that.suggestionNodes[that.selectedIndex][1]);
                            e.preventDefault();
                        } else {
                            that.dropdownController.hideDropdown();
                        }
                    }
                }
            };

            if (suggestionNodes.length) {

                this.selectedIndex = 0;
                this.suggestionNodes = suggestionNodes;

                for (var i=0; i < that.suggestionNodes.length; i++) {
                    var eventData = { instance: this, index: i };
                    this.suggestionNodes[i][0]
                        .bind("mouseover", eventData, activate)
                        .bind("mouseout", eventData, deactivate)
                        .bind("click", eventData, complete);
                }

                // make sure we don't bind more than once
                if (!this.keyboardHandlerBinded) {
                    jQuery(this.field).keypress(keyPressHandler);
                    if (jQuery.browser.mozilla) {
                        jQuery(this.field).keypress(keyboardNavigateHandler);
                    } else {
                        jQuery(this.field).keydown(keyboardNavigateHandler);
                    }
                    this.keyboardHandlerBinded = true;
                }

                // automatically select the first in the list
                if(that.autoSelectFirst === false) {
                    setActive(-1);
                } else {
                    setActive(0);
                }

                // sets the autocomplete singleton infocus var to this instance
                // is used to toggle event propagation. In short, the instance that it is set to will not hide the
                // dropdown each time you click the input field
                inFocus = this;
            }

            function activate(event) {
                if (that.dropdownController.displayed) {
                    setActive(event.data.index);
                }
            }
            function deactivate(event) {
                if (event.data.index === 0) {
                    that.selectedIndex = -1;
                }
                jQuery(this).removeClass("active");
            }
            function complete(event) {
                that.completeField(that.suggestionNodes[event.data.index][1]);
            }
        },


        /**
         * Uses jquery empty command, this is VERY important as it unassigns handlers
         * used for mouseover, click events which expose an opportunity for memory leaks
         * @method {public} clearResponseContainer
         */
        clearResponseContainer: function() {
            this.responseContainer.empty();
            this.suggestionNodes = undefined;
        },

        /**
         * function {Privileged} delay
         * @param response
         */
        delay: delay,

        /**
         * Builds HTML container for suggestions.
         * Positions container top position to be that of the field height
         * @method {public} buildResponseContainer
         */
        buildResponseContainer: function() {
            var inputParent = this.field.parent().addClass('atlassian-autocomplete');
            this.responseContainer = jQuery(document.createElement("div"));
            this.responseContainer.addClass("suggestions")
            this.positionResponseContainer();
            this.responseContainer.appendTo(inputParent);
        },

        positionResponseContainer: function() {
            this.responseContainer.css({ top: this.field.outerHeight() });
        },

        /**
         * Validates the keypress by making sure the field value is beyond the set threshold and the key was either an
         * up or down arrow
         * @method {public} keyUpHandler
         * @param {Object} e - event object
         */
        keyUpHandler: (function () {
            var isIe8 = jQuery.browser.msie && jQuery.browser.version == 8;
            function callback() {
                if (!this.responseContainer) {
                    this.buildResponseContainer();
                }
                // send value to dispatcher to check if we have already got the response or if we need to go
                // back to the server
                this.dispatcher(this.field.val());
            }

            return function (e) {
                // only initialises once the field length is past set length
                if (this.field.val().length >= this.minQueryLength) {
                    // don't do anything if the key pressed is "enter" or "down" or "up" or "right" "left"
                    if (!(e.keyCode in INVALID_KEYS) || (this.responseContainer && !this.responseContainer.is(":visible") && (e.keyCode == 38 || e.keyCode == 40))) {
                        if (isIe8) {
                            // Performance workaround for IE8 (but not IE7): excessive DOM manipulation (JRADEV-3142)
                            delay(jQuery.proxy(callback, this), 0.200);
                        } else {
                            callback.call(this);
                        }
                    }
                }
                return e;
            };
        })(),

        /**
         * Adds in methods via AOP to handle multiple selections
         * @method {Public} addMultiSelectAdvice
         */
        addMultiSelectAdvice: function(delim) {

            // reference to this for closures
            var that = this;

            /**
             * Alerts user if value already exists
             * @method {private} alertUserValueAlreadyExists
             * @param {String} val - value that already exists, will be displayed in message to user.
             */
            var alertUserValueAlreadyExists = function(val) {

                // check if there is an existing alert before adding another
                if (!alertUserValueAlreadyExists.isAlerting) {

                    alertUserValueAlreadyExists.isAlerting = true;

                    // create alert node and append it to the input field's parent, fade it in then out with a short
                    // delay in between.
                    //TODO: JRA-1800 - Needs i18n!
                    var userAlert = jQuery(document.createElement("div"))
                        .css({"float": "left", display: "none"})
                        .addClass("warningBox")
                        .html("Oops! You have already entered the value <em>" + val + "</em>" )
                        .appendTo(that.field.parent())
                        .show("fast", function(){
                            // display message for 4 seconds before fading out
                            that.delay(function(){
                                userAlert.hide("fast",function(){
                                    // removes element from dom
                                    userAlert.remove();
                                    alertUserValueAlreadyExists.isAlerting = false;
                                });
                            }, 4);
                        });
                }
            };

            // rather than request the entire field return the last comma seperated value
            jQuery.aop.before({target: this, method: "dispatcher"}, function(innvocation){
                // matches everything after last comma
                var val = this.field.val();
                innvocation[0] = jQuery.trim(val.slice(val.lastIndexOf(delim) + 1));
                return innvocation;
            });

            // rather than replacing this field just append the new value
            jQuery.aop.before({target: this, method: "completeField"}, function(args){
                var valueToAdd = args[0],
                // create array of values
                    untrimmedVals = this.field.val().split(delim);
                // trim the values in the array so we avoid extra spaces being appended to the usernames - see JRA-20657
                var trimmedVals = jQuery(untrimmedVals).map(function() {
                    return jQuery.trim(this);
                }).get();
                // check if the value to append already exists. If it does then call alert to to tell user and sets
                // the last value to "". The value to add will either appear:
                // 1) at the start of the string
                // 2) after some whitespace; or
                // 3) directly after the delimiter
                // It is assumed that the value is delimited by the delimiter character surrounded by any number of spaces.
                if (!this.allowDuplicates && new RegExp("(?:^|[\\s" + delim + "])" + valueToAdd + "\\s*" + delim).test(this.field.val())) {
                    alertUserValueAlreadyExists(valueToAdd);
                    trimmedVals[trimmedVals.length-1] = "";
                } else {
                    // add the new value to the end of the array and then an empty value so we
                    // can get an extra delimiter at the end of the joined string
                    trimmedVals[trimmedVals.length-1] = valueToAdd;
                    trimmedVals[trimmedVals.length] = "";
                }

                // join the array of values with the delimiter plus an extra space to make the list of values readable
                args[0] = trimmedVals.join(delim.replace(/([^\s]$)/,"$1 "));

                return args;
            });
        },


        /**
         * Adds and manages state of dropdown control
         * @method {Public} addDropdownAdvice
         */
        addDropdownAdvice: function() {
            var that = this;

            // add dropdown functionality to response container
            jQuery.aop.after({target: this, method: "buildResponseContainer"}, function(args){
                this.dropdownController = JIRA.Dropdown.AutoComplete({target: this, method: "renderSuggestions"}, this.responseContainer);

                jQuery.aop.after({ target: this.dropdownController, method: "hideDropdown" }, function () {
                    this.dropdown.removeClass("dropdown-ready");
                });

                return args;
            });

            // display dropdown afer suggestions are updated
            jQuery.aop.after({target: this, method: "renderSuggestions"}, function(args){
                if (args && args.length > 0) {
                    this.dropdownController.displayDropdown();

                    if (this.maxHeight && this.dropdownController.dropdown.prop("scrollHeight") > this.maxHeight) {
                        this.dropdownController.dropdown.css({
                            height: this.maxHeight,
                            overflowX: "visible",
                            overflowY: "scroll"
                        })
                    } else if (this.maxHeight) {
                        this.dropdownController.dropdown.css({
                            height: "",
                            overflowX: "",
                            overflowY: ""
                        });
                    }
                    this.dropdownController.dropdown.addClass("dropdown-ready");
                } else {
                    this.dropdownController.hideDropdown();
                }
                return args;
            });

            // hide dropdown after suggestion value is applied to field
            jQuery.aop.after({target: this, method: "completeField"}, function(args){
                this.dropdownController.hideDropdown();
                return args;
            });

            jQuery.aop.after({target: this, method: "keyUpHandler"}, function(e) {
                // only initialises once the field length is past set length
                if ((!(this.field.val().length >= this.minQueryLength) || e.keyCode === 27)
                    && this.dropdownController && this.dropdownController.displayed) {
                    this.dropdownController.hideDropdown();
                    if (e.keyCode === 27) {
                        e.stopPropagation();
                    }
                }
                return e;
            });
        },

        /**
         * Initialises autocomplete by setting options, and assigning event handler to input field.
         * @method {public} init
         * @param {Object} options
         */
        init: function(options) {
            var that = this;
            this.set(options);
            this.field = this.field || jQuery("#" + this.fieldID);
            // turn off browser default autocomplete
            this.field.attr("autocomplete","off")
                .keyup(function(e){
                    if (!that.disabled) {
                        that.keyUpHandler(e);
                    }
                })
                .keydown(function (e) {
                    var ESC_KEY = 27;
                    // do not clear field in IE
                    if (e.keyCode === ESC_KEY && that.responseContainer && that.responseContainer.is(":visible")) {
                        e.preventDefault();
                    }
                })
                // this will stop the dropdown with the suggestions hiding whenever you click the field
                .click(function(e){
                    if (inFocus === that) {
                        e.stopPropagation();
                    }
                })
                .blur(function () {
                    // we don't want the request to come back and show suggestions if we have already moved away from field
                    if (that.pendingRequest) {
                        that.pendingRequest.abort();
                    }
                });

            this.addDropdownAdvice();

            if (options.delimChar) {
                this.addMultiSelectAdvice(options.delimChar);
            }
        }
    };

}();



//////////////////////////////////////////////////////
/////////////// JIRA.JQLAutoComplete /////////////////
//////////////////////////////////////////////////////
    var jql_order_by = [{value:"ORDER BY", displayName:"ORDER BY"}];
    var jql_operators = [{value:"=", displayName:"="}, {value:"!=", displayName:"!="}, {value:"~", displayName:"~"}, {value:"<=", displayName:"&lt;="}, {value:">=", displayName:"&gt;="}, {value:">", displayName:"&gt;"}, {value:"<", displayName:"&lt;"}, {value:"!~", displayName:"!~"}, {value:"is not", displayName:"is not"}, {value:"is", displayName:"is"}, {value:"not in", displayName:"not in"}, {value:"in", displayName:"in"}, {value: "was", displayName: "was"}, {value: "was not", displayName: "was not"},  {value: "was in", displayName: "was in"}, {value: "was not in", displayName: "was not in"}, {value: "changed", displayName: "changed"}];
    var jql_logical_operators = [{value:"AND", displayName:"AND"}, {value:"OR", displayName:"OR"}];
    var jql_logical_operators_and_order_by = jql_logical_operators.concat(jql_order_by);
    var jql_order_by_direction = [{value:"ASC", displayName:"ASC"}, {value:"DESC", displayName:"DESC"}];
    var empty_operand = [{value:"EMPTY", displayName:"EMPTY", types:["java.lang.Object"]}];
    var jql_not_logical_operator = [{value:"NOT", displayName:"NOT"}];
    var jql_was_predicates= [{value:"AFTER", displayName:"AFTER", type:"java.util.Date", supportsList:"false"}, {value:"BEFORE", displayName:"BEFORE", type:"java.util.Date", supportsList:"false"}, {value:"BY", displayName:"BY", type:"com.atlassian.crowd.embedded.api.User", supportsList:"false", auto:"true"}, {value:"DURING", displayName:"DURING", type:"java.util.Date", supportsList:"true"} ,{value:"ON", displayName:"ON", type:"java.util.Date", supportsList:"false"} ];
    var jql_changed_predicates =   jql_was_predicates.concat([{value:"FROM", displayName:"FROM", type:"java.lang.String", supportsList: "true", auto: "true"}, {value:"TO", displayName:"TO", type:"java.lang.String", supportsList: "true", auto: "true"}]);
    var jql_was_predicates_and_order_by= jql_was_predicates.concat(jql_logical_operators_and_order_by);
    var jql_was_predicates_and_logical_operators= jql_was_predicates.concat(jql_logical_operators);
    var jql_changed_predicates_and_order_by= jql_changed_predicates.concat(jql_logical_operators_and_order_by);
    var jql_changed_predicates_and_logical_operators= jql_changed_predicates.concat(jql_logical_operators);

    var REGEXP_ANDS            = /^AND\s/i;
    var REGEXP_ORS             = /^OR\s/i;
    var REGEXP_NOTS            = /^NOT\s/i;
    var REGEXP_SNOT            = /^\s+not/i;
    var REGEXP_SIN             = /^\s+in/i;
    var REGEXP_SNOT_IN         = /^\s+(not\s+in|not|in)/i;
    var REGEXP_COMMA_DELIMITER = /^\s*,/;
    var REGEXP_ASCENDING       = /^\s+asc/i;
    var REGEXP_DESCENDING      = /^\s+desc/i;
    var REGEXP_ORDER_BY        = /^order\s+by/i;
    var REGEXP_WHITESPACE      = /^\s/;
    var REGEXP_UNICODE         = /^u[a-fA-F0-9]{4}/;
    var REGEXP_FIELD_NAME      = /^\s-\scf\[\d\d\d\d\d\]/;
    var REGEXP_NEW_LINE        = /[\r\n]/;
    var REGEXP_NUMBER          = /\d/;
    var REGEXP_TOKEN_CHAR      = /[^=!~<>(),\s&|]/;
    var REGEXP_SPECIAL_CHAR    = /[{}*\/%+$#@?.;\][]/;
    var REGEXP_SPACE_OR_ELSE   = /[\s(]/;
    var REGEXP_CHARS_TO_ESCAPE = /[^trn"'\\\s]/;
    var REGEXP_NOTSTART        = /^NO?$/i;
    var REGEXP_INSTART         = /^IN?$/i;
    var REGEXP_PREDICATE       = /^(after|before|by|during|from|on|to)/i;

    JIRA.JQLAutoComplete = function (options) {

        var that = begetObject(JIRA.AutoComplete);
        var parser = options.parser;
        var result;
        var jql_field_names = jQuery.grep(options.jqlFieldNames, function(arrValue){
            // We only want the searchable fields
            return arrValue.searchable;
        });
        var jql_order_by_field_names = jQuery.grep(options.jqlFieldNames, function(arrValue){
            // We only want the orderable fields
            return arrValue.orderable;
        });
        var jql_function_names = options.jqlFunctionNames;
        var PARSE_INDICATOR = options.errorEl || jQuery("#" + options.errorID);
        var suggestionCount = 0;
        var jqlcolumnnum = jQuery("#jqlcolumnnum");
        var jqlrownum = jQuery("#jqlrownum");
        var autoCompleteUrl = options.autoCompleteUrl || "/rest/api/2/jql/autocompletedata/suggestions";

        that.textToSuggestionCursorPosition = function () {
            return this.field.selectionRange().textBefore;
        };


        that.pushSuggestionsOnHtmlStack = function(suggestions, suggestionNodes, htmlParts, listItemIdentifier, mayNeedParenthesis)
        {
            var length = Math.min(15, suggestions.length);
            for (var i = 0; i < length; i++)
            {
                var actualValueSug;
                var displayNameSug;
                // We may have an object with displayName and value OR it may just be a string
                if (suggestions[i].value)
                {
                    var resultSug = suggestions[i];
                    if (result && mayNeedParenthesis)
                    {
                        // This is a hack that adds an initial ( when the value is the first completed in a list
                        // We only do this for suggestions and not moreSuggestions since we know that this is
                        // the position that the values will take, moreSuggestions will be function suggestions
                        actualValueSug = ((result.getNeedsOpenParen()) ? "(" : "") + resultSug.value;
                    }
                    else
                    {
                        actualValueSug = resultSug.value;
                    }
                    displayNameSug = resultSug.displayName;
                }
                else
                {
                    displayNameSug = suggestions[i];
                    actualValueSug = displayName;
                }
                suggestionNodes.push(actualValueSug);
                htmlParts.push(
                    '<li id="',
                    listItemIdentifier,
                    i,
                    '">',
                    displayNameSug,
                    '</li>'
                );
            }
        };

        that.renderSuggestions = function ( suggestions, moreSuggestions, operatorSuggestions) {
            var suggestionNodes = [];

            if (suggestions instanceof Array)
            {
                if (suggestions.length < 1 && (moreSuggestions && moreSuggestions.length < 1) && (operatorSuggestions && operatorSuggestions.length < 1))
                {
                    return suggestionNodes;
                }

                var htmlParts = ['<ul>'];
                if (operatorSuggestions && operatorSuggestions.length > 0)
                {
                    that.pushSuggestionsOnHtmlStack(operatorSuggestions, suggestionNodes, htmlParts, "jql_operator_suggest_", false);
                }
                that.pushSuggestionsOnHtmlStack(suggestions, suggestionNodes, htmlParts, "jql_value_suggest_", true);
                if (moreSuggestions && moreSuggestions.length > 0)
                {
                    that.pushSuggestionsOnHtmlStack(moreSuggestions,  suggestionNodes, htmlParts, "jql_function_suggest_", false);
                }
                htmlParts.push('</ul>');
                // TODO Fix help link!
                var syntaxHelpLink = AJS.$('<a class="syntax-help" target="_blank">Syntax help</a>')
                    .attr('href', 'https://confluence.atlassian.com/display/AOD/Advanced+Searching');
                var syntaxHelpContainer = AJS.$('<div class="syntax-help-container">').append(syntaxHelpLink);

                that.responseContainer
                    .html(htmlParts.join(''))
                    .append(syntaxHelpContainer)
                    .find('li').each(function(i) {
                        suggestionNodes[i] = [jQuery(this), suggestionNodes[i]];
                    });

                that.addSuggestionControls(suggestionNodes);
            }

            return suggestionNodes;
        };

        that.completeField = function(value)
        {
            var start = that.getReplacementStartIndex(result, value);

            var end = that.getReplacementEndIndex(result, start);

            that.replaceValue(start, end, value);

            // Parse the whole thing again with the full string so we can set the parse/not parse indicator correctly, possibly a third parse, this is starting to get crazy
            var newToken = parser.parse(this.field.val());
            that.updateParseIndicator(newToken);
        };

        that.prepareOperandSuggestions = function(canAutoComplete, fieldName, functionSuggestions, operatorSuggestions, suggestedValue)
        {
            if (canAutoComplete)
            {
                var currentSuggestionCount = suggestionCount;
                var fieldValue = (suggestedValue == null) ?  ( (result.getLastOperand() === null) ? "" : that.stripEscapeCharacters(result.getLastOperand())) : suggestedValue;
                var fieldNameValueKey = fieldName + ":" + fieldValue;
                var data = {fieldName:that.stripEscapeCharacters(fieldName)};
                if (result.getLastOperand() !== null || suggestedValue !== null)
                {
                    data.fieldValue = fieldValue;
                }
                var includeEmpty = that.isWasOperator(result.getLastOperator());
                if (!that.getSavedResponse(fieldNameValueKey))
                {
                    // We only delay the AJAX request, if it comes back and we have already suggested something else then the results will be dropped because of suggestionCount
                    that.dropdownController.dropdown.removeClass("dropdown-ready");
                    this.delay(function()
                    {
                        that._makeRequest({
                            url: autoCompleteUrl,
                            headers: {"Accept": "application/json"},
                            data: data,
                            success: function (response)
                            {
                                response = JSON.parse(response);
                                var results;
                                if (response && response.results)
                                {
                                    results = response.results;
                                }
                                else
                                {
                                    results = [];
                                }
                                // Cache the results
                                //append empty
                                if (includeEmpty)
                                {
                                    var suggestion = data.fieldValue;
                                    results = that.appendEmpty(suggestion, results);
                                }
                                that.saveResponse(fieldNameValueKey, results);
                                that.renderSuggestionsForOperands(fieldValue, results, functionSuggestions, operatorSuggestions, currentSuggestionCount);
                            },
                            error: function ()
                            {
                                that.renderSuggestionsForOperands(fieldValue, [], functionSuggestions, operatorSuggestions, currentSuggestionCount);
                            }
                        });
                    }, that.queryDelay);
                }
                else
                {
                    that.renderSuggestionsForOperands(fieldValue, that.getSavedResponse(fieldNameValueKey), functionSuggestions, operatorSuggestions, currentSuggestionCount);
                }
            }
            else
            {
                // We should at least render the function suggestions
                that.renderSuggestionsForOperands(that.stripEscapeCharacters(result.getLastOperand()), [], functionSuggestions, operatorSuggestions, suggestionCount);
            }
        };

        that.preparePredicateSuggestions = function(predicateName, suggestedValue)
        {
            var currentSuggestionCount = suggestionCount;
            if (typeof suggestedValue == "undefined")
            {
                suggestedValue = null;
            }
            var functionSuggestions = that.slimListForPredicates(suggestedValue, jql_function_names, predicateName);
            that.boldMatchingString(suggestedValue, functionSuggestions);
            var predicateValue = (suggestedValue == null) ?  ( (result.getLastOperand() === null) ? "" : that.stripEscapeCharacters(result.getLastOperand())) : suggestedValue;
            var predicateNameValueKey = predicateName + ":" + predicateValue;
            var data = {predicateName:that.stripEscapeCharacters(predicateName)};
            if (result.getLastOperand() !== null || suggestedValue !== null)
            {
                data.predicateValue = predicateValue;
            }
            if (result.getLastFieldName() !== null)
            {
                data.fieldName = result.getLastFieldName();
                predicateNameValueKey = result.getLastFieldName()+":"+predicateNameValueKey;
            }
            if (that.predicateSupportsAutoComplete(predicateName))
            {
                if (!that.getSavedResponse(predicateNameValueKey))
                {
                    // We only delay the AJAX request, if it comes back and we have already suggested something else then the results will be dropped because of suggestionCount
                    that.dropdownController.dropdown.removeClass("dropdown-ready");
                    this.delay(function()
                    {
                        that._makeRequest({
                            url: autoCompleteUrl,
                            headers: {"Accept": "application/json"},
                            data: data,
                            success: function (response)
                            {
                                response = JSON.parse(response);
                                var results;
                                if (response && response.results)
                                {
                                    results = response.results;
                                }
                                else
                                {
                                    results = [];
                                }
                                // Cache the results
                                //append empty
                                var suggestion = data.predicateValue;
                                results = that.appendEmpty(suggestion, results);
                                that.saveResponse(predicateNameValueKey, results);
                                that.renderSuggestions(results, functionSuggestions);
                            },
                            error: function ()
                            {
                                that.renderSuggestionsForOperands(predicateValue, []);
                            }
                        });
                    }, that.queryDelay);
                }
                else
                {
                    that.renderSuggestions(that.getSavedResponse(predicateNameValueKey), functionSuggestions);
                }
            }
            else
            {
                // simply render functions
                that.renderSuggestions( [], functionSuggestions);
            }
        };

        that.incompleteOperator = function()
        {
            var tokens = result.getTokens();
            if (tokens == null || tokens.length  < 2)
            {
                return "";
            }
            else
            {
                var tokens = that.textToSuggestionCursorPosition().split(" ");
                if (tokens.length > 2)
                {
                    return tokens[tokens.length-1];
                }
            }
            return "";
        };

        that.incompletePredicateValue = function()
        {
            var tokens = result.getTokens();
            if (tokens == null || tokens.length  < 3)
            {
                return "";
            }
            else
            {

                var tokens = that.textToSuggestionCursorPosition().split(/[\s(,]+/);
                if (tokens.length > 3)
                {
                    return tokens[tokens.length - 1];
                }
            }
            return "";
        };

        that.appendEmpty= function(suggestion, results)
        {
            var empty_list = that.slimListForMapResults(suggestion, empty_operand, true);
            if (suggestion != null)
            {
                that.boldMatchingString(suggestion, empty_list);
            }
            if (results.length < 15)
            {
                results = results.concat(empty_list);
            }
            else
            {
                results[14] = empty_list[0];
            }
            return results;
        }

        that.dispatcher = function (val) {

            var that = this;
            var selectionRange = jQuery(that.field).selectionRange();
            var parseValue = val.substring(0, selectionRange.start);
            result = parser.parse(parseValue).getResult();
            // We will always make a suggestion from here so lets increment the count
            suggestionCount++;
            // In this case we suggest operators
            if (result.getNeedsField())
            {
                that.renderSuggestionsFromMap(that.stripEscapeCharacters(result.getLastFieldName()), jql_field_names, jql_not_logical_operator, true);
            }
            // for was queries you msay need either an operator or operand
            else if (result.getNeedsOperatorOrOperand())
            {
                var operator = result.getLastOperator();
                var operatorLength = operator.length;
                var parsedOperator = (operator.length > 0) ? parseValue.substr(result.getLastOperatorStartIndex()) : null;
                var functionSuggestions = (that.isEmptyOnlyOperator(result.getLastOperator())) ? empty_operand : jql_function_names;
                var operatorSuggestions = that.getSuggestionsForOperators(parsedOperator, jql_operators);

                if (operatorLength > 0)
                {
                    var numSpaces = operator.length - (operator.replace(/\s+/g,'')).length;
                    result.setLastOperandStartIndex(result.getLastOperatorStartIndex()+ operatorLength + numSpaces+1);
                }
                if (operatorSuggestions && operatorSuggestions.length > 0)
                {
                    var tokens = result.getTokens();
                    var operandSuggestion = that.incompleteOperator();
                    that.prepareOperandSuggestions(true, result.getLastFieldName(), functionSuggestions, operatorSuggestions, operandSuggestion);
                }
            }
            else if (result.getNeedsOperator())
            {
                that.renderSuggestionsForOperators(result.getLastOperator(), jql_operators);
            }
            else if (result.getNeedsPredicateOperand())
            {
                var predicate = result.getLastWasPredicate();
                that.preparePredicateSuggestions(predicate, that.stripEscapeCharacters(result.getLastOperand()));
            }
            else if (result.getNeedsLogicalOperator())
            {
                if (result.getNeedsWasPredicate())
                {
                    if (result.getNeedsOrderBy())
                    {
                        var value = (result.getLastOrderBy() === null) ? result.getLastWasPredicate() : result.getLastOrderBy();
                        if (result.getLastOperator() == 'was')
                        {
                            that.renderSuggestionsFromMap(value, jql_was_predicates_and_order_by, false);
                        }
                        else
                        {
                            that.renderSuggestionsFromMap(value, jql_changed_predicates_and_order_by, false);
                        }
                    }
                    else
                    {
                        if (result.getLastOperator() == 'was')
                        {
                            that.renderSuggestionsFromMap(result.getLastWasPredicate(), jql_was_predicates_and_logical_operators, false);
                        }
                        else
                        {
                            that.renderSuggestionsFromMap(result.getLastWasPredicate(), jql_changed_predicates_and_logical_operators, false);
                        }
                    }

                }
                else
                {
                    if (result.getNeedsOrderBy())
                    {
                        var value = (result.getLastOrderBy() === null) ? result.getLastLogicalOperator() : result.getLastOrderBy();
                        that.renderSuggestionsFromMap(value, jql_logical_operators_and_order_by, false);
                    }
                    else
                    {
                        that.renderSuggestionsFromMap(result.getLastLogicalOperator(), jql_logical_operators, false);
                    }
                }
            }
            else if (result.getNeedsOrderByField())
            {
                that.renderSuggestionsFromMap(that.stripEscapeCharacters(result.getLastOrderByFieldName()), jql_order_by_field_names, true);
            }
            else if (result.getNeedsOrderByDirection())
            {
                that.renderSuggestionsFromMap(result.getLastOrderByDirection(), jql_order_by_direction, false);
            }
            else if (result.getNeedsOperand())
            {
                var fieldName = result.getLastFieldName();

                var canAutoComplete = false;
                // If we know that we are only suggesting empty then we do not need to suggest values
                if (!that.isEmptyOnlyOperator(result.getLastOperator()))
                {
                    for (var i = 0; i < jql_field_names.length; i++)
                    {
                        if (that.equalsIgnoreCase(result.getUnquotedString(jql_field_names[i].value), fieldName) ||
                            (jql_field_names[i].cfid && that.equalsIgnoreCase(jql_field_names[i].cfid, fieldName)))
                        {
                            canAutoComplete = jql_field_names[i].auto;
                            break;
                        }
                    }
                }

                var functionSuggestions = (that.isEmptyOnlyOperator(result.getLastOperator())) ? empty_operand : jql_function_names;
                that.prepareOperandSuggestions(canAutoComplete, fieldName, functionSuggestions,{});
            }
            else if (result.getNeedsOrderBy())
            {
                that.renderSuggestionsFromMap(result.getLastOrderBy(), jql_logical_operators_and_order_by, false);
            }
            else
            {
                that.dropdownController.hideDropdown();
            }

            // Need to update the parse/not parse indicator
            that.parse(val);
        };

        that.stripEscapeCharacters = function(val) {

            if (val == null)
            {
                return val;
            }
            var newVal = "";
            var strArr = val.split("");
            for(var i = 0; i < strArr.length; i++)
            {
                if (strArr[i] == '\\')
                {
                    // If we are a unicode string then we just consume it like normal
                    if (!REGEXP_UNICODE.test(val.substring(i, val.length)))
                    {
                        // Just chew past the escape and use the next char, the parser has already made sure this is cool
                        i++;
                        if (i >= val.length)
                        {
                            break;
                        }
                    }
                }
                newVal += strArr[i];
            }
            return newVal;
        };

        that.parse = function(val) {
            var newToken = parser.parse(val);
            that.updateParseIndicator(newToken);
            that.updateColumnLineCount();
            return newToken.getResult();
        };

        that.renderSuggestionsFromMap = function(stringVal, list, otherSuggestions, showFull)
        {
            if (!otherSuggestions)
            {
                otherSuggestions = {};
            }
            var suggestions = that.slimListForMapResults(stringVal, list, showFull);
            var relevantOtherSuggestions = that.slimListForMapResults(stringVal, otherSuggestions, showFull);
            that.boldMatchingString(stringVal, relevantOtherSuggestions);
            that.boldMatchingString(stringVal, suggestions);

            that.renderSuggestions(suggestions, relevantOtherSuggestions);
            if (suggestions.length === 0 && relevantOtherSuggestions.length === 0)
            {
                that.dropdownController.hideDropdown();
            }
        };

        that.getSuggestionsForOperators = function(stringVal, list)
        {
            var suggestions = that.slimListForMapResults(stringVal, list, false);

            var fieldName = result.getLastFieldName();
            // Find the current field, if we know about it and get the supported operators
            var supportedOperators;
            for (var i = 0; i < jql_field_names.length; i++)
            {
                if (that.equalsIgnoreCase(result.getUnquotedString(jql_field_names[i].value), fieldName) ||
                    (jql_field_names[i].cfid && that.equalsIgnoreCase(jql_field_names[i].cfid, fieldName)))
                {
                    supportedOperators = jql_field_names[i].operators;
                    break;
                }
            }

            // Now lets run through the remaining list and if we can identify the field we can see if that field
            // supports which operators
            if (supportedOperators)
            {
                suggestions = jQuery.grep(suggestions, function(arrValue){
                    return jQuery.inArray(arrValue.value, supportedOperators) > -1;
                });
            }
            that.boldMatchingString(stringVal, suggestions);
            return suggestions;
        };

        that.renderSuggestionsForOperators = function(stringVal, list)
        {
            var suggestions = that.getSuggestionsForOperators(stringVal, list);
            that.renderSuggestions(suggestions);
            if (suggestions.length === 0)
            {
                that.dropdownController.hideDropdown();
            }
        };

        that.renderSuggestionsForOperands = function(stringVal, ajaxSuggestions, functions, operatorSuggestions, providedSuggestionCount)
        {
            // Only render the suggestions if we are the current suggestion
            if (providedSuggestionCount === suggestionCount)
            {
                // Don't need to slim the ajaxSuggestions since they came from the server slimmed down
                var functionsSuggestions = that.slimListForFunctionResults(stringVal, functions, result.getLastOperator());
                that.boldMatchingString(stringVal, functionsSuggestions);
                that.renderSuggestions( ajaxSuggestions, functionsSuggestions, operatorSuggestions);

                if (ajaxSuggestions.length === 0 && functionsSuggestions.length === 0 && operatorSuggestions.length == 0)
                {
                    that.dropdownController.hideDropdown();
                }
            }
        };

        // This bolds the beginning portion of the matching string and converts the list to be value/displayName. This
        // assumes that all strings in the list have already been confirmed to match the incomming stringVal
        that.boldMatchingString = function(stringVal, list)
        {
            if (stringVal == null || list.length === 0)
            {
                return;
            }

            var boldLength = stringVal.length;
            // Run through all the characters looking for html escape characters so we can include their extra length in
            // the bold length
            for(var j = 0, n = boldLength; j < n; j++)
            {
                switch (stringVal.charAt(j))
                {
                    case "<":
                    case ">":
                        // We have one character representing this already, lets add the other 3 for &lt; or &gt;
                        boldLength += 3;
                        break;
                    case "&":
                        // We have one character representing this already, lets add the other 4 for &amp;
                        boldLength += 4;
                        break;
                    case '"':
                        // We have one character representing this already, lets add the other 5 for &quot;
                        boldLength += 5;
                        break;
                }
            }

            for(var i = 0; i < list.length; i++)
            {
                if (list[i].displayName)
                {
                    var origVal = list[i].displayName;
                    // Create a new copy of the object so we don't mess up the original list
                    list[i] = {value:list[i].value, displayName:"<b>" + origVal.substring(0, boldLength) + "</b>" + origVal.substring(boldLength)};
                }
                else
                {
                    // Add a displayName so we don't mess up the value
                    list[i] = {value:list[i], displayName:"<b>" + list[i].substring(0, boldLength) + "</b>" + list[i].substring(boldLength)};
                }
            }
        };

        that.htmlEscape = function(stringVal)
        {
            if (stringVal == null)
            {
                return null;
            }
            var escapedVal = "";
            var strArr = stringVal.split("");
            // Run through all the characters looking for html escape characters so we can include their extra length in
            // the bold length
            for(var j = 0; j < strArr.length; j++)
            {
                if (strArr[j] === "<")
                {
                    escapedVal += "&lt;";
                }
                else if(strArr[j] === ">")
                {
                    escapedVal += "&gt;";
                }
                else if (strArr[j] === "&")
                {
                    // We have one character representing this already, lets add the other 4 for &amp;
                    escapedVal += "&amp;";
                }
                else if (strArr[j] === "\"")
                {
                    // We have one character representing this already, lets add the other 5 for &quot;
                    escapedVal += "&quot;";
                }
                else
                {
                    escapedVal += strArr[j];
                }
            }
            return escapedVal;
        },

            that.getReplacementStartIndex = function(result, value)
            {
                var jQueryReference = jQuery(this.field);
                var start;
                if (result.getNeedsField())
                {
                    start = result.getLastFieldNameStartIndex();
                }
                else if(result.getNeedsOperatorOrOperand())
                {
                    // was presents difficulties - it may be followed by either an operand or an operator
                    if(result.getNeedsOperator() && that.isWasOperator(value) )
                    {
                        start = result.getLastOperatorStartIndex();
                    }
                    else
                    {
                        start = result.getLastOperandStartIndex();
                    }
                }
                else if(result.getNeedsOperand())
                {
                    start = result.getLastOperandStartIndex();
                }
                else if(result.getNeedsOperator())
                {
                    start = result.getLastOperatorStartIndex();
                }
                else if(result.getNeedsOrderByField())
                {
                    start = result.getLastOrderByFieldNameStartIndex();
                }
                else if(result.getNeedsOrderByDirection())
                {
                    start = result.getLastOrderByDirectionStartIndex();
                }
                else if (result.getNeedsPredicateOperand())
                {
                    start = result.getLastOperandStartIndex();
                }
                else if(result.getNeedsLogicalOperator())
                {
                    if (result.getLastLogicalOperatorStartIndex() !== null && result.getLastLogicalOperatorStartIndex() !== 0)
                    {
                        start = result.getLastLogicalOperatorStartIndex();
                    }
                    else
                    {
                        if (result.getLastOrderBy() !== null && result.getLastOrderByStartIndex() !== 0)
                        {
                            start = result.getLastOrderByStartIndex();
                        }
                        else if (result.getLastWasPredicate() != null && result.getLastWasPredicateStartIndex() !== 0)
                        {
                            start = result.getLastWasPredicateStartIndex();
                        }
                        else if (result.getMustBeOperatorOrPredicate() === true)
                        {
                            start  =  jQueryReference.selectionRange().start;
                        }
                        else
                        {
                            start = jQueryReference.selectionRange().start - 1;
                        }
                    }
                }
                else if (result.getNeedsWasPredicate())
                {
                    start = result.getLastWasPredicateStartIndex();
                }
                else if (result.getNeedsOrderBy())
                {
                    start = result.getLastOrderByStartIndex();
                }
                else
                {
                    start = jQueryReference.selectionRange().start - 1;
                }
                // sanity check
                return start != null ? start : jQueryReference.selectionRange().start;
            };

        that.getReplacementEndIndex = function(result, start)
        {
            var jQueryReference = jQuery(this.field);
            var selectionRange = jQueryReference.selectionRange();
            var end = null;
            // We only need to do a second parse if we have no highlighted selection AND we are not at the end of the input string
            // otherwise we just use the selectionEnd
            if (selectionRange.start === selectionRange.end && selectionRange.end !== this.field.val().length)
            {
                // Lets get the token number from the first parse, this is the token we are currently on
                var currentTokenIdx = result.getTokens().length - 1;

                // Parse it again, but this time the full string so we know what the full token is that we are trying to replace
                // with the selected completion
                var token = parser.parse(this.field.val());
                if (!token.getParseError())
                {
                    // The user has not highlighted text so lets assume we are completing to the end of the current token
                    // Ask the newly parsed result for the complete token we are in the middle of
                    var fullTokenValue = token.getResult().getTokens()[currentTokenIdx];
                    // This is a special case, we don't want to replace the '(' or ')' instead we want to add inside
                    if (fullTokenValue !== null && fullTokenValue !== '(' && fullTokenValue !== ')' )
                    {
                        var fullTextVal = this.field.val();
                        // Lets get the start position in the string
                        var remainingString = fullTextVal.substring(start, fullTextVal.length);
                        // We know that we are going to see the fullTokenValue next, but there may be some whitespace between
                        // here and there, lets make sure we ditch the whitespace as well.
                        var remainingStringArr = remainingString.split("");
                        var whitespaceCount = 0;
                        for(var i = 0; i < remainingStringArr.length; i++)
                        {
                            if (REGEXP_WHITESPACE.test(remainingStringArr[i]))
                            {
                                whitespaceCount++;
                            }
                            else
                            {
                                // Stop as soon as we no longer see whitespace
                                break;
                            }
                        }
                        end = start + fullTokenValue.length + whitespaceCount;
                    }
                }
            }

            if (end === null)
            {
                end = selectionRange.end;
            }
            // As the start position could have been incremented to account for spaces in operators
            // e.g. was not in, it is feasible that end may be less than start so a quick check is in order.
            // If start is less than end pad with a space (one is sufficient)
            if (end < start)
            {
                that.replaceValue(start,end," ");
                end=start;
            }
            return end;
        };

        that.replaceValue = function(start, end, newValue)
        {
            var jQueryReference = jQuery(this.field);
            // Lets reset the selection range to include the characters that the user has already typed
            jQueryReference.selectionRange(start, end);
            // Lets replace the value with the autocomplete selected value
            jQueryReference.selection(newValue);
            // Lets stop the replaced bit from being highlighted
            var newEnd = jQueryReference.selectionRange().end;
            jQueryReference.selectionRange(newEnd, newEnd);
        };

        that.slimListForMapResults = function (stringVal, list, showFull)
        {
            var escString = that.htmlEscape(stringVal);
            var slimedList = jQuery.grep(list, function(arrValue){
                return that.startsWithIgnoreCaseNullsMeanAll(escString, arrValue.displayName) ;
            });

            if (!showFull)
            {
                // We only want to show the value when the user has fully typed it in IF there are more than one suggestion
                // with this prefix.
                if (slimedList.length === 1 && !that.startsWithNotEqualsIgnoreCaseNullMeansAll(escString, slimedList[0].displayName))
                {
                    return {};
                }
            }
            return slimedList;
        };

        that.slimListForFunctionResults = function (stringVal, list, operator)
        {
            var fieldName = result.getLastFieldName();
            // Find the current field, if we know about it and get the supported types
            var supportedTypes;
            for (var i = 0; i < jql_field_names.length; i++)
            {
                if (that.equalsIgnoreCase(result.getUnquotedString(jql_field_names[i].value), fieldName) ||
                    (jql_field_names[i].cfid && that.equalsIgnoreCase(jql_field_names[i].cfid, fieldName)))
                {
                    supportedTypes = jql_field_names[i].types;
                    break;
                }
            }

            var slimedList = jQuery.grep(list, function(arrValue){
                // For functions we only want to show the is list ones with list operators and vice versa
                if ((arrValue.isList && !that.isListSupportingOperator(operator)) ||
                    (!arrValue.isList && that.isListSupportingOperator(operator)))
                {
                    return false;
                }
                if (supportedTypes)
                {
                    // Need to check for Object since this means we always fit
                    var supportsFunction = jQuery.inArray("java.lang.Object", arrValue.types) > -1 || jQuery.inArray("java.lang.Object", supportedTypes) > -1;
                    for(var i = 0; i < supportedTypes.length && !supportsFunction; i++)
                    {
                        supportsFunction = jQuery.inArray(supportedTypes[i], arrValue.types) !== -1;
                    }
                    if (!supportsFunction)
                    {
                        return false;
                    }
                }
                else
                {
                    // Can't find the field so we know that no functions will work with it
                    return false;
                }
                return that.startsWithIgnoreCaseNullsMeanAll(stringVal, arrValue.value) || that.startsWithIgnoreCaseNullsMeanAll(stringVal, arrValue.displayName) ;
            });

            // We only want to show the value when the user has fully typed it in IF there are more than one suggestion
            // with this prefix.
            if (slimedList.length === 1 && !that.startsWithNotEqualsIgnoreCaseNullMeansAll(stringVal, slimedList[0].displayName))
            {
                return {};
            }
            return slimedList;
        };

        that.slimListForPredicates  = function (stringVal, list, predicate)
        {
            var supportedType;
            var supportsList;
            for (var i = 0; i < jql_changed_predicates.length; i++)
            {
                if (that.equalsIgnoreCase(result.getUnquotedString(jql_changed_predicates[i].value), predicate))
                {
                    supportedType = jql_changed_predicates[i].type;
                    supportsList = jql_changed_predicates[i].supportsList;
                    break;
                }
            }

            var slimedList = jQuery.grep(list, function(arrValue){
                // For functions we only want to show the is list ones with list operators and vice versa

                if (typeof arrValue.isList != "undefined")
                {
                    if ((arrValue.isList && !!supportsList) || (!arrValue.isList && supportsList))
                    {
                        return false;
                    }
                }
                var supportsFunction =  jQuery.inArray(supportedType, arrValue.types) !== -1;
                return supportsFunction ? that.startsWithIgnoreCaseNullsMeanAll(stringVal, arrValue.value) || that.startsWithIgnoreCaseNullsMeanAll(stringVal, arrValue.displayName) : false;
            });

            // We only want to show the value when the user has fully typed it in IF there are more than one suggestion
            // with this prefix.
            if (slimedList.length === 1 && !that.startsWithNotEqualsIgnoreCaseNullMeansAll(stringVal, slimedList[0].displayName))
            {
                return {};
            }
            return slimedList;
        };

        that.predicateSupportsAutoComplete = function(predicate)
        {
            var auto;
            for (var i = 0; i < jql_changed_predicates.length; i++)
            {
                if (that.equalsIgnoreCase(result.getUnquotedString(jql_changed_predicates[i].value), predicate))
                {
                    auto = jql_changed_predicates[i].auto;
                    break;
                }
            }
            return auto;

        };

        that.isListSupportingOperator = function (operator)
        {
            return operator === 'in' || operator === 'not in' || operator == 'was not in' || operator == 'was in';
        };

        that.isEmptyOnlyOperator = function (operator)
        {
            return operator === 'is' || operator === 'is not';
        };

        that.isWasOperator = function (operator)
        {
            return operator === 'was' || operator === 'was in' || operator == 'was not in' || operator == 'was not';
        };

        that.isChangedOperator = function (operator)
        {
            return operator === 'changed';
        };

        that.isHistoryOperator = function (operator)
        {
            return this.isWasOperator(operator) || this.isChangedOperator(operator);
        };

        that.startsWithIgnoreCaseNullsMeanAll = function(startStr, str)
        {
            // In this case we want all elements of the list included
            if (str === null || startStr === null)
            {
                return true;
            }
            if (str.length < startStr.length)
            {
                return false;
            }
            else
            {
                return startStr.toLowerCase() == str.substr(0, startStr.length).toLowerCase();
            }
        };

        that.startsWithNotEqualsIgnoreCaseNullMeansAll = function(startStr, str)
        {
            // In this case we want all elements of the list included
            if (str === null || startStr === null)
            {
                return true;
            }
            if (str.length < startStr.length)
            {
                return false;
            }
            else
            {
                var subStrEquals = startStr.toLowerCase() == str.substr(0, startStr.length).toLowerCase();
                if (subStrEquals)
                {
                    // HACK!! This is a hack so that the custom field display values will not show up as a suggestion when they
                    // are completely typed in.
                    var equalsString = null;
                    if (REGEXP_FIELD_NAME.test(str.substr(startStr.length, str.length)))
                    {
                        equalsString = str.substr(0, startStr.length);
                    }
                    else
                    {
                        equalsString = str;
                    }
                    return startStr.toLowerCase() != equalsString.toLowerCase();
                }
                return false;
            }
        };

        that.equalsIgnoreCase = function (str1, str2)
        {
            if (str1 === null && str2 === null)
            {
                return true;
            }
            else if (str1 === null || str1 === null)
            {
                return false;
            }
            else
            {
                return str1.toLowerCase() === str2.toLowerCase();
            }
        };

        /**
         * Gets cached response from <em>requested</em> object
         * @method {public} getSavedResponse
         * @param {String} val
         * @returns {Object}
         */
        that.getSavedResponse = function(val) {
            if (!this.requested) {
                this.requested = {};
            }
            return this.requested[val];
        };

        /**
         * Saves response to <em>requested</em> object
         * @method {public} saveResponse
         * @param {String} val
         * @param {Object} response
         */
        that.saveResponse = function(val, response) {
            if (typeof val === "string" && typeof response === "object") {
                if (!this.requested) {
                    this.requested = {};
                }
                this.requested[val] = response;
            }
        };

        that.startsWithIgnoreCase = function(startStr, str)
        {
            if (str === null || startStr === null || str.length < startStr.length)
            {
                return false;
            }
            else
            {
                return startStr.toLowerCase() == str.substr(0, startStr.length).toLowerCase();
            }
        };

        that.updateParseIndicator = function(token)
        {
            if (token.getParseError())
            {
                PARSE_INDICATOR.attr("title", token.getResult().getParseErrorMsg()).removeClass("jqlgood").addClass("jqlerror");
            }
            else
            {
                PARSE_INDICATOR.attr("title", "").removeClass("jqlerror").addClass("jqlgood");
            }
        };

        that.updateColumnLineCount = function()
        {
            var jQueryReference = jQuery(that.field);
            var totalCharCountToCursor = 0;

            if(that.field[0] === document.activeElement) {
                var selectionRange = jQueryReference.selectionRange();
                totalCharCountToCursor = selectionRange.start;
            } else {
                totalCharCountToCursor = that.field[0].value.length;
            }
            var rowCount = 1;
            var colCount = 1;

            var fieldValue = that.field.val();

            for (var i = 0; i < totalCharCountToCursor; i++)
            {
                if (REGEXP_NEW_LINE.test(fieldValue.charAt(i)))
                {
                    rowCount++;
                    colCount = 1;
                }
                else
                {
                    colCount++;
                }
            }

            // Update our counts for where our cursor is at the moment
            jqlcolumnnum.text(colCount);
            jqlrownum.text(rowCount);
        };

        that.init(options);

        return that;
    };

    JIRA.JQLAutoComplete.MyParser = function(jqlReservedWords) {

        var jql_reserved_words = jqlReservedWords;
        return {
            parse: function(input) {
                var token = JIRA.JQLAutoComplete.Token();

                token.init(input);
                this.jql(token);
//            token.toString();
                return token;
            },

            orderByClause: function(token)
            {
                var remainingString = token.remainingString();
                // Lets consume the 'order' token
                var matchArray = remainingString.match(REGEXP_ORDER_BY);

                if (matchArray)
                {
                    var orderByString = remainingString.substring(0, matchArray[0].length);
                    token.consumeCharacters(orderByString.length);
                    token.getResult().setLastOrderBy(orderByString, token);

                    if (!token.isComplete())
                    {
                        // We must have some space in order to parse an order by fields
                        remainingString = token.remainingString();
                        if (REGEXP_WHITESPACE.test(remainingString))
                        {
                            this.chewWhitespace(token);
                            // Look for order by fields
                            this.orderByFields(token);
                        }
                        else
                        {
                            token.getResult().resetLogicalOperators();
                            token.setParseError();
                        }
                    }
                    else
                    {
                        token.getResult().resetLogicalOperators();
                        token.setParseError();
                    }
                }
                else
                {
                    // Consume the remaining string
                    token.consumeCharacters(remainingString.length);
                    token.getResult().setLastOrderBy(remainingString, token);
                    token.setParseError();
                }
            },

            orderByFields: function(token)
            {
                // Look for fields followed by 'asc' or 'desc' or commas
                this.orderByField(token);
                this.chewWhitespace(token);
                if (token.isComplete())
                {
                    // We always need to check to see if we need to put in a place-holder for the order by direction
                    if (!token.getResult().getNeedsOrderByField())
                    {
                        token.getResult().setLastOrderByDirection("", token);
                        // This is not a parse error, it is just a token place holder so we will complete correctly
                    }
                }
                else
                {
                    // Look for a comma
                    var remainingString = token.remainingString();
                    if (this.startsWithIgnoreCase(",", remainingString))
                    {
                        token.consumeCharacter();
                        // Lets recurse back looking for more order by fields
                        this.orderByFields(token);
                        this.chewWhitespace(token);
                    }
                    else
                    {
                        remainingString = token.remainingString();
                        if (remainingString !== null)
                        {
                            token.consumeCharacters(remainingString.length);
                        }
                        token.getResult().setNeedsOrderByDirection();
                        token.getResult().setLastOrderByDirection(remainingString, token);
                        token.setParseError();
                    }
                }
            },

            orderByField: function(token)
            {
                this.chewWhitespace(token);
                var fieldName = this.fieldName(token);
                if (fieldName.length !== 0)
                {
                    token.getResult().setLastOrderByFieldName(fieldName, token);

                    // If we see a comma then we don't have an order by direction
                    var remainingString = token.remainingString();

                    if (!token.isComplete() && !REGEXP_COMMA_DELIMITER.test(remainingString))
                    {
                        // Better look for a direction, but it is optional so no big deal if we do not find it
                        // We need a space between the order by field name and the direction
                        if (REGEXP_ASCENDING.test(remainingString))
                        {
                            this.chewWhitespace(token);
                            // consume and move on
                            token.consumeCharacters(3);
                            token.getResult().setLastOrderByDirection("asc", token);
                            token.getResult().setNeedsOrderByComma();
                        }
                        else if (REGEXP_DESCENDING.test(remainingString))
                        {
                            this.chewWhitespace(token);
                            // consume and move on
                            token.consumeCharacters(4);
                            token.getResult().setLastOrderByDirection("desc", token);
                            token.getResult().setNeedsOrderByComma();
                        }
                        else
                        {
                            token.getResult().setNeedsOrderByDirection();
                        }
                    }
                    else
                    {
                        token.getResult().setNeedsOrderByField();
                        this.chewWhitespace(token);
                    }
                }
                else
                {
                    token.getResult().setLastOrderByFieldName("", token);
                    token.getResult().setNeedsOrderByField();
                    token.setParseError();
                }
            },

            jql: function(token)
            {
                this.orClause(token);
                var remainingString = token.remainingString();
                if (this.startsWithIgnoreCase("ord", remainingString))
                {
                    this.orderByClause(token);
                }
            },

            orClause: function(token)
            {

                while(!token.isComplete() && !this.startsWithIgnoreCase(")", token.remainingString()))
                {
                    this.chewWhitespace(token);
                    var remainingString = token.remainingString();
                    if (this.startsWithIgnoreCase("ord", remainingString))
                    {
                        // lets give control back to the jql function so we can end up in the orderByClause
                        break;
                    }
                    if (token.getResult().getMustBeOperatorOrPredicate())
                    {
                        token.getResult().resetLastPredicates();
                    }
                    else
                    {
                        token.getResult().resetLogicalOperators();
                        this.andClause(token);
                    }
                    if (!token.isComplete() && token.getResult().getLastLogicalOperator() === null)
                    {
                        if (token.getResult().getNeedsWasPredicate())
                        {
                            this.predicateClause(token);
                        }
                        // we may as well exit early if the predicate clause has been handled
                        // and the token is complete
                        if (token.isComplete())
                        {
                            return;
                        }
                        // Look for an OR clause
                        remainingString = token.remainingString();
                        if (this.startsWithIgnoreCase("ord", remainingString))
                        {
                            // lets give control back to the jql function so we can end up in the orderByClause
                            break;
                        }
                        else if (remainingString !== null && (REGEXP_ORS.test(remainingString) || this.startsWithIgnoreCase("|", remainingString) || this.startsWithIgnoreCase("||", remainingString)))
                        {
                            if (this.startsWithIgnoreCase("||", remainingString))
                            {
                                token.getResult().setLastLogicalOperator("||", token.getTokenStringIdx());
                                token.consumeCharacters(2);
                            }
                            else if (REGEXP_ORS.test(remainingString))
                            {
                                token.getResult().setLastLogicalOperator("OR", token.getTokenStringIdx());
                                token.consumeCharacters(3);
                            }
                            else
                            {
                                token.getResult().setLastLogicalOperator("|", token.getTokenStringIdx());
                                token.consumeCharacters(1);
                            }

                            token.getResult().resetTerminalClause();
                            if (token.isComplete())
                            {
                                token.setParseError();
                            }
                            token.getResult().setNeedsField();
                        }
                        // This block of code is very confusing. The reason it is here is that when we are in a nested
                        // block of '('s it is the terminalClause that handles the parens and it calls off to this orClause.
                        // The orClause needs to ignore the close paren so that the terminalClause can handle it, BUT only
                        // when we are currently in a set of parens. So, ff the remaining character is a close paren and
                        // we are not in parens then we are NOT in error, otherwise we are
                        else if (!this.startsWithIgnoreCase(")", remainingString) || !token.getInParens())
                        {
                            this.chewWhitespace(token);
                            var errorIdx = (remainingString === null) ? token.getMaxTokenStringIdx(): token.getMaxTokenStringIdx() - remainingString.length;
                            // let's tokenise this to simplify space handling
                            var tokens;
                            if (remainingString != null)
                            {
                                tokens = remainingString.split(" ");
                                token.getResult().setLastLogicalOperator(tokens[0], errorIdx);
                                token.getResult().setLastWasPredicate(tokens[0], token);
                                token.getResult().setNeedsLogicalOperator(token);
                            }

                            // may need to go around again to check for predicate
                            if (token.getResult().getLastWasOperator() == null)
                            {
                                token.setParseError();
                            }
                            else
                            {
                                if (tokens != null)
                                {
                                    // JRADEV-6053 fails after and
                                    if (REGEXP_PREDICATE.test(tokens[0]))
                                    {
                                        token.getResult().setLastLogicalOperator(null);
                                        // JRADEV-7204 deleting back to predicate still leaves suggestions
                                        // in place, check this condition and reset
                                        token.getResult().resetNeedsPredicateOperand();
                                    }
                                    else
                                    {
                                        if (REGEXP_ANDS.test(remainingString))
                                        {
                                            token.consumeCharacters(4);
                                            token.getResult().resetTerminalClause();
                                            token.getResult().setNeedsField();
                                        }
                                        if (token.isComplete() || tokens.length == 1)
                                        {
                                            token.setParseError();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            },

            andClause: function(token)
            {
                this.chewWhitespace(token);
                this.notClause(token);
                if (!token.isComplete() && token.getResult().getLastLogicalOperator() === null)
                {
                    // Look for an AND clause
                    this.chewWhitespace(token);

                    var remainingString = token.remainingString();
                    if (remainingString !== null && (REGEXP_ANDS.test(remainingString) || this.startsWithIgnoreCase("&", remainingString) || this.startsWithIgnoreCase("&&", remainingString)))
                    {
                        if (this.startsWithIgnoreCase("&&", remainingString))
                        {
                            token.getResult().setLastLogicalOperator("&&", token.getTokenStringIdx());
                            token.consumeCharacters(2);
                        }
                        else if (this.startsWithIgnoreCase("&", remainingString))
                        {
                            token.getResult().setLastLogicalOperator("&", token.getTokenStringIdx());
                            token.consumeCharacters(1);
                        }
                        else
                        {
                            token.getResult().setLastLogicalOperator("AND", token.getTokenStringIdx());
                            token.consumeCharacters(4);
                        }
                        token.getResult().resetTerminalClause();
                        if (token.isComplete())
                        {
                            token.setParseError();
                        }

                        token.getResult().setNeedsField();
                    }
                    else
                    {
                        token.getResult().setNeedsLogicalOperator(token);
                        // This is a special case for when we are about to place a logical operator into an existing string
                        // so that we will have a null place-holder token for the logical operator we expect.
                        if (token.isComplete())
                        {
                            token.getResult().setLastLogicalOperator(null, token.getMaxTokenStringIdx());
                        }
                    }
                }
            },

            notClause: function(token)
            {
                this.chewWhitespace(token);
                if (!token.isComplete())
                {
                    // Look for a NOT clause
                    this.chewWhitespace(token);
                    var remainingString = token.remainingString();
                    if (remainingString !== null && (REGEXP_NOTS.test(remainingString) || this.startsWithIgnoreCase("!", remainingString)))
                    {
                        // This is a bit of a hack that makes it so that you do not get a suggestion for a field until
                        // after you have typed a space after the NOT
                        token.getResult().needsField = false;
                        if (this.startsWithIgnoreCase("!", remainingString))
                        {
                            token.getResult().setLastLogicalOperator("!", token.getTokenStringIdx());
                            token.consumeCharacters(1);
                        }
                        else
                        {
                            token.getResult().setLastLogicalOperator("NOT", token.getTokenStringIdx());
                            token.consumeCharacters(4);
                        }
                        token.getResult().resetTerminalClause();
                        if (token.isComplete())
                        {
                            token.setParseError();
                        }

                        token.getResult().setNeedsField();
                    }
                    else
                    {
                        this.terminalClause(token);
                    }
                }
                else
                {
                    // Let the terminal clause set all the error conditions and needs
                    this.terminalClause(token);
                    token.setParseError();
                }
            },

            predicateClause: function(token)
            {
                this.chewWhitespace(token);
                if (!token.isComplete()) {
                    var remainingString = token.remainingString();
                    var tokens = remainingString.split(/[\s(]+/);
                    if (remainingString !== null)
                    {
                        token.getResult().setMustBeOperatorOrPredicate(false);
                        // if the string starts with logical operator - return control to the
                        // not clause, however check if this is the last token, and if it is set the parse error flag
                        if (this.startsWithLogicalOperator(remainingString))
                        {
                            if (tokens.length == 1)
                            {
                                token.getResult().setLastWasPredicate(tokens[0], token );
                                token.setParseError();
                                return;
                            }
                            return;
                        }
                        token.getResult().resetNeedsPredicateOperand();
                        token.consumeCharacters(tokens[0].length);
                        token.getResult().setLastWasPredicate(tokens[0], token );
                        // if there is only 1 token then there is no whitespace, so get out of Dodge
                        if (tokens.length == 1)
                        {
                            token.setParseError();
                            return;
                        }
                        if (this.wasPredicateComplete(tokens[0]))
                        {
                            token.getResult().setNeedsPredicateOperand();
                            token.getResult().setLastOperand(null, token);
                            this.predicateOperand(token);
                        }
                        else
                        {
                            token.setParseError();
                        }
                    }
                }
                else
                {
                    // Let the terminal clause set all the error conditions and needs
                    token.setParseError();
                }

            },

            predicateOperand: function(token)
            {
                this.chewWhitespace(token);
                var predicateOperand = this.collectPredicateOperands(token);
                if (predicateOperand == null || predicateOperand == "")
                {
                    token.getResult().setLastOperandStartIndex(token.getTokenStringIdx());
                    token.setParseError();
                }
                // need a workaround to ensure you reset the needsPredicateOperand
                // easiest way is to check if you have entered a space after the
                // operand - meaning you want to go on parsing
                if (token.remainingString() != null)
                {
                    token.getResult().resetNeedsPredicateOperand();
                    token.getResult().resetLastPredicates();
                    token.getResult().setMustBeOperatorOrPredicate(true);
                    token.getResult().setNeedsLogicalOperator(token);
                    this.chewWhitespace(token);
                }

            },

            collectPredicateOperands: function(token)
            {
                // If we have and in or not in operator then we need either a function or an open paren
                if (token.getResult().getLastWasPredicate() === 'DURING'  )
                {
                    token.getResult().setNeedsOpenParen(true);
                }

                var predicateOperand = this.listOperand(token, true);
                if (predicateOperand.length === 0 )
                {
                    predicateOperand = this.functionOperand(token);
                    if (predicateOperand.length === 0)
                    {
                        predicateOperand = this.singleValueOperand(token);
                    }
                    else
                    {
                        // We were handled by a function so we no longer need the open paren
                        token.getResult().setNeedsOpenParen(false);
                    }
                }
                else
                {
                    // We were handled by a list so we no longer need the open paren
                    token.getResult().setNeedsOpenParen(false);
                }
                return token.getResult().getUnquotedString(predicateOperand);
            },

            terminalClause: function(token)
            {
                // Lets always reset the last field variables
                token.getResult().resetTerminalClause();
                this.chewWhitespace(token);

                var remainingString = token.remainingString();
                // If we see a ( then we need to hand off to the or clause again
                if (this.startsWithIgnoreCase("(", remainingString))
                {
                    token.getResult().addToken('(');
                    token.setInParens();
                    token.consumeCharacter();
                    this.orClause(token);
                    this.chewWhitespace(token);
                    remainingString = token.remainingString();
                    if (this.startsWithIgnoreCase(")", remainingString))
                    {
                        token.getResult().addToken(')');
                        token.consumeCharacter();
                        token.setOutOfParens();
                        if (token.isComplete())
                        {
                            // We don't want to suggest a logical operator to the user if the query parses
                            token.getResult().resetLogicalOperators();
                            token.getResult().resetLastPredicates();
                        }
                    }
                    else
                    {
                        token.setParseError();
                    }
                }
                else
                {
                    this.field(token);
                    if (!token.isComplete())
                    {
                        this.oper(token);
                        if (!token.isComplete())
                        {
                            if (token.getResult().getLastWasOperator() === 'changed')
                            {
                                token.getResult().setNeedsLogicalOperator(token);
                                return;
                            }
                            this.operand(token);

                            // This will only happen when we have seen a list operand that is properly closed
                            if (token.getResult().getOperandComplete())
                            {
                                // JRADEV-6372 Autocomplete does not parse after was in (or indeed was not in)
                                var lastWasOperator = token.getResult().getLastWasOperator();
                                token.getResult().setNeedsNothing();
                                if (lastWasOperator)
                                {
                                    token.getResult().setLastWasOperator(lastWasOperator);
                                }
                            }
                            else if (token.isComplete() && !token.getResult().getNeedsListComma())
                            {
                                // This is not a parse error since we have found everything we were looking for
                                token.getResult().setNeedsOperand();
                            }
                        }
                        else
                        {
                            //if (token.getResult().get that.ge token.getResult().setNeedsOperator();
                            if (token.getResult().getLastOperator() == "was" || token.getResult().getLastOperator() == "was not")
                            {
                                token.getResult().setNeedsOperandOrOperator();
                            }
                            else if (token.getResult().getLastWasOperator() === 'changed')
                            {
                                // JRADEV-7179 Need to make sure that there is a space before calling for an autocomplete
                                var currentChar = token.currentCharacter();
                                if(currentChar !== null && REGEXP_SPACE_OR_ELSE.test(currentChar))
                                {
                                    token.getResult().setNeedsLogicalOperator(token);
                                }
                                else
                                {
                                    token.getResult().resetLogicalOperators();
                                    token.getResult().setNeedsOrderBy(false);
                                }
                                return;
                            }
                            else
                            {
                                token.getResult().setNeedsOperator();
                            }
                            token.setParseError();
                        }
                    }
                    else
                    {
                        token.getResult().setNeedsField();
                        token.setParseError();
                    }
                }
            },

            field: function(token)
            {
                var fieldName = this.fieldName(token);
                if (fieldName.length !== 0 )
                {
                    token.getResult().setLastFieldName(fieldName, token);
                }
                else if (token.getResult().getLastFieldName() === null)
                {
                    remainingString = token.remainingString();
                    token.getResult().setNeedsField();
                    token.getResult().setLastFieldName(remainingString, token);
                    token.setParseError();
                }
            },

            fieldName: function(token)
            {
                this.chewWhitespace(token);

                // Field is either a string or cf[12345]
                var remainingString = token.remainingString();
                if (this.startsWithIgnoreCase("cf", remainingString))
                {
                    var origIdx = token.getTokenStringIdx();
                    var origString = token.remainingString();
                    token.consumeCharacters(2);
                    this.chewWhitespace(token);
                    remainingString = token.remainingString();
                    // Now we must find a [ or the show is off
                    if (this.startsWithIgnoreCase("[", remainingString))
                    {
                        token.consumeCharacter();
                        // We must find a number in here until we encounter a ]
                        remainingString = token.remainingString();
                        var custFieldId = this.numberValue(token);
                        if (custFieldId.length !== 0)
                        {
                            // Lets find our closing ]
                            this.chewWhitespace(token);
                            remainingString = token.remainingString();
                            if (this.startsWithIgnoreCase("]", remainingString))
                            {
                                token.consumeCharacter();
                                return origString.substring(0, token.getTokenStringIdx() - origIdx);
                            }
                        }
                    }
                    token.setParseError();
                    return origString;
                }
                else
                {
                    return this.fieldOrFunctionName(token);
                }
            },

            oper: function(token)
            {
                this.chewWhitespace(token);
                var remainingString = token.remainingString();
                var operator = this.getLongestOperatorMatch(remainingString, jql_operators);
                if (operator !== null)
                {
                    // We found an operator, record it and consume the right amount of characters
                    token.getResult().setLastOperator(operator, token.getTokenStringIdx());
                    if (this.isWasOperator(operator) || this.isChangedOperator(operator))
                    {
                        token.getResult().setLastWasOperator(operator);
                    }
                    token.consumeCharacters(operator.length);
                    // If we are one of the word operators we need to enforce a space here
                    if (operator == "in" || operator == "is" || operator == "is not" || operator == "not in" || operator == "was" || operator == "was not" || operator == "was in" || operator == "was not in")
                    {
                        // We need a space or else
                        var currentChar = token.currentCharacter();
                        if(currentChar !== null && !REGEXP_SPACE_OR_ELSE.test(currentChar))
                        {
                            token.setParseError();
                        }
                    }
                    // was may be followed by not, so if it ihas a n or a no then the parser is still
                    // in error

                    if (operator  == "was")
                    {
                        token.getResult().setNeedsOperandOrOperator();
                        this.chewWhitespace(token);
                        var remainingString = token.remainingString();

                        if (remainingString != null && (REGEXP_NOTSTART.test(remainingString) || REGEXP_INSTART.test(remainingString)))
                        {
                            token.setParseError();
                        }
                    }
                    else if (operator == "was not")
                    {
                        token.getResult().setNeedsOperandOrOperator();
                        this.chewWhitespace(token);
                        var remainingString = token.remainingString();

                        if (remainingString != null && (REGEXP_INSTART.test(remainingString)))
                        {
                            token.setParseError();
                        }
                    }
                    else if (operator == "changed")
                    {
                        token.getResult().setNeedsLogicalOperator(token);
                    }
                    else
                    {
                        token.getResult().setNeedsOperand();
                    }
                }
                else if (token.getResult().getLastOperator() === null)
                {
                    var errorIdx = (remainingString === null) ? token.getMaxTokenStringIdx(): token.getMaxTokenStringIdx() - remainingString.length;
                    token.getResult().setLastOperator(remainingString, errorIdx);
                    token.getResult().setNeedsOperator();
                    token.setParseError();
                }
            },

            operand: function(token)
            {
                this.chewWhitespace(token);

                // If we have and in or not in operator then we need either a function or an open paren
                if (token.getResult().getLastOperator() === 'in' || token.getResult().getLastOperator() === 'not in' || token.getResult().getLastOperator() === 'was not in' || token.getResult().getLastOperator() === 'was in' )
                {
                    token.getResult().setNeedsOpenParen(true);
                }

                var operand = this.listOperand(token, true);
                if (operand.length === 0 )
                {
                    operand = this.functionOperand(token);
                    if (operand.length === 0)
                    {
                        operand = this.singleValueOperand(token);
                    }
                    else
                    {
                        // We were handled by a function so we no longer need the open paren
                        token.getResult().setNeedsOpenParen(false);
                    }
                }
                else
                {
                    // We were handled by a list so we no longer need the open paren
                    token.getResult().setNeedsOpenParen(false);
                }

                if (operand === null || operand.length === 0)
                {
                    var remainingString = token.remainingString();
                    token.getResult().setLastOperand(remainingString, token);
                    token.getResult().setNeedsOperand();
                    token.setParseError();
                }
                if (operand.length !== 0)
                {
                    return operand;
                }
                return "";
            },

            singleValueOperand: function(token)
            {
                var operand = this.stringValue(token);
                if (operand.length !== 0)
                {
                    token.getResult().setLastOperand(operand, token);
                    // Lets exclude empty and null, even though it is reserved
                    if (operand.toLowerCase() != "empty" && operand.toLowerCase() != "null" && this.isReservedWord(operand.toLowerCase()))
                    {
                        // These are reserved words
                        token.setParseError();
                    }
                    return operand;
                }
                return "";
            },

            functionOperand: function(token)
            {
                var startIdx = token.getTokenStringIdx();
                var functionName = this.fieldOrFunctionName(token);
                // There can be whitespace between function name and arguments
                this.chewWhitespace(token);
                var listArguments = this.listOperand(token, false);
                if (functionName.length !== 0 && listArguments.length !== 0)
                {
                    var operand = functionName + listArguments;
                    // read in the whole value until we reach a close )
                    token.getResult().setLastOperand(operand, token);
                    return operand;
                }
                else
                {
                    // back track
                    token.backTrackToIdx(startIdx);
                    return "";
                }
            },

            listOperand: function(token, treatAsOperands)
            {
                if (token.currentCharacter() == '(')
                {
                    token.consumeCharacter();
                    var listValue = this.collectListValues(token, treatAsOperands);
                    var operandVal = "(" + listValue;

                    this.chewWhitespace(token);
                    if (token.currentCharacter() == ')')
                    {
                        token.consumeCharacter();
                        operandVal += ")";
                        if (operandVal == "()" && treatAsOperands)
                        {
                            // Special case of an empty list which is still valid
                            token.getResult().setLastOperand(operandVal, token);
                        }
                        if(treatAsOperands)
                        {
                            token.getResult().setOperandComplete();
                            token.getResult().resetNeedsPredicateOperand();
                        }
                    }
                    else
                    {
                        token.setParseError();
                    }
                    return operandVal;
                }
                else
                {
                    return "";
                }
            },

            collectListValues: function(token, treatAsOperands)
            {
                if (treatAsOperands)
                {
                    token.getResult().setNeedsOperand();
                }
                this.chewWhitespace(token);

                // grab the contents of the list, they should be singleValueOperands separated by commas, we only
                // need to keep track of the last encountered operand.
                var currentOperand = (treatAsOperands) ? this.operand(token) : this.stringValue(token);

                if (currentOperand.length !== 0)
                {
                    // If there is whitespace then lets remember we need a comma
                    if (this.chewWhitespace(token) && treatAsOperands)
                    {
                        token.getResult().setNeedsListComma();
                    }
                    if (token.currentCharacter() == ',')
                    {
                        // Consume the comma and recurse so we can collect the other values
                        token.consumeCharacter();
                        var nextValue = this.collectListValues(token, treatAsOperands);
                        if (nextValue.length === 0)
                        {
                            token.setParseError();
                        }
                        return currentOperand + ", " + nextValue;
                    }
                    else
                    {
                        return currentOperand;
                    }
                }
                else
                {
                    return "";
                }
            },

            startsWithIgnoreCase: function(startStr, str)
            {
                if (str === null || startStr === null || str.length < startStr.length)
                {
                    return false;
                }
                else
                {
                    return startStr.toLowerCase() == str.substr(0, startStr.length).toLowerCase();
                }
            },

            startsWithLogicalOperator: function(str)
            {
                if (str === null)
                {
                    return false;
                }
                else
                {
                    return this.startsWithOr(str) ||  this.startsWithAnd(str) || this.startsWithNot(str) || this.startsWithBraces(str);
                }
            },

            startsWithOr: function(str)
            {
                return this.startsWithIgnoreCase("|", str) || this.startsWithIgnoreCase("||", str) ||
                    this.startsWithIgnoreCase("or", str);
            },

            startsWithAnd: function(str)
            {
                return this.startsWithIgnoreCase("&", str)  || this.startsWithIgnoreCase("&&", str)  ||
                    this.startsWithIgnoreCase("and", str);
            },

            startsWithNot: function(str)
            {
                return this.startsWithIgnoreCase("!", str) ||
                    this.startsWithIgnoreCase("not", str);
            },

            startsWithBraces: function(str)
            {
                return this.startsWithIgnoreCase("(", str) ||
                    this.startsWithIgnoreCase(")", str);
            },

            chewWhitespace: function(token)
            {
                var foundWhiteSpace = false;
                var currentChar = token.currentCharacter();
                while(currentChar !== null && REGEXP_WHITESPACE.test(currentChar))
                {
                    token.consumeCharacter();
                    currentChar = token.currentCharacter();
                    foundWhiteSpace = true;
                }
                return foundWhiteSpace;
            },

            getLongestOperatorMatch: function(value, listOfValues)
            {
                var longestMatch = null;
                // These first three cases are special since they might have more to them
                if (this.startsWithIgnoreCase("is", value))
                {
                    // Look ahead for NOT, if we don't find it next then we must just be is
                    var matchArray = value.substring(2).match(REGEXP_SNOT);

                    if (matchArray)
                    {
                        longestMatch = value.substring(0, matchArray[0].length + 2);
                    }
                    else
                    {
                        longestMatch = "is";
                    }
                }
                if (this.startsWithIgnoreCase("was", value))
                {
                    // Look ahead for NOT or IN , if we don't find it next then we must just be was
                    var matchArray = value.substring(3).match(REGEXP_SNOT_IN);

                    if (matchArray)
                    {
                        longestMatch = value.substring(0, matchArray[0].length + 3);
                    }
                    else
                    {
                        longestMatch = "was";
                    }
                }
                else if (this.startsWithIgnoreCase("not", value))
                {
                    // Look ahead for IN, if we don't find it next then we are not an operator
                    var matchArrayNot = value.substring(3).match(REGEXP_SIN);

                    if (matchArrayNot)
                    {
                        longestMatch = value.substring(0, matchArrayNot[0].length + 3);
                    }
                }
                else
                {
                    for(var i = 0; i < listOfValues.length; i++)
                    {
                        if(this.startsWithIgnoreCase(listOfValues[i].value, value))
                        {
                            // We found a match
                            if (longestMatch === null || jql_operators[i].value.length > longestMatch)
                            {
                                longestMatch = jql_operators[i].value;
                            }
                        }
                    }
                }
                return longestMatch;
            },

            getValueMinusExtraWhitespace: function(value)
            {
                if (value === null)
                {
                    return null;
                }
                var newValue = "";
                var firstWhitespace = true;
                var valueArr = value.split("");
                for (var i = 0; i < valueArr.length; i++)
                {
                    var currentChar = valueArr[i];
                    if (REGEXP_WHITESPACE.test(currentChar))
                    {
                        // We want to ignore extra whitespace, keeping only the first
                        if (firstWhitespace)
                        {
                            firstWhitespace = false;
                            newValue = newValue + currentChar;
                        }
                    }
                    else
                    {
                        // If we encounter a non-whitespace then we want to reset our firstWhitespace test
                        firstWhitespace = true;
                        newValue = newValue + currentChar;
                    }
                }
                return newValue;
            },

            fieldOrFunctionName: function(token)
            {
                var stringValue = this.stringValue(token);
                // Field or function names can not be the empty string
                if (stringValue === "\"\"" || stringValue === "''")
                {
                    token.setParseError();
                }
                if (this.isReservedWord(stringValue.toLowerCase()))
                {
                    // These are reserved words
                    token.setParseError();
                }
                return stringValue;
            },

            isReservedWord: function(word)
            {
                return jQuery.inArray(word, jql_reserved_words) !== -1;
            },

            isWasOperator: function (operator)
            {
                return operator === 'was' || operator === 'was in' || operator == 'was not in' || operator == 'was not';
            },

            isChangedOperator: function (operator)
            {
                return operator === 'changed';
            },

            stringValue: function(token)
            {
                var stringValue = "";

                var inQuote = false;
                var inSingleQuote = false;
                var currentChar = token.currentCharacter();
                while(currentChar !== null && (inQuote || inSingleQuote|| REGEXP_TOKEN_CHAR.test(currentChar)))
                {
                    // Read the escape character into the string
                    stringValue = stringValue + currentChar;
                    token.consumeCharacter();
                    // Handle the escape character
                    if (currentChar == '\\')
                    {
                        // Just consume the next char as well
                        currentChar = token.currentCharacter();
                        if (currentChar === null)
                        {
                            token.setParseError();
                            break;
                        }
                        // These are the only valid characters to escape
                        else if (REGEXP_CHARS_TO_ESCAPE.test(currentChar))
                        {
                            // Check for unicode escapes
                            var remainingString = token.remainingString();
                            if (!REGEXP_UNICODE.test(remainingString))
                            {
                                token.setParseError();
                                break;
                            }
                        }

                        stringValue = stringValue + currentChar;
                        token.consumeCharacter();
                    }
                    // Check for illegal characters and kill the whole parse
                    else if (REGEXP_SPECIAL_CHAR.test(currentChar) && !(inQuote || inSingleQuote))
                    {
                        token.setParseError();
                        break;
                    }
                    // We need to keep track if we are in a quote or not
                    else if (currentChar == '"' && !inSingleQuote)
                    {
                        inQuote = !inQuote;
                    }
                    else if (currentChar == "'" && !inQuote)
                    {
                        inSingleQuote = !inSingleQuote;
                    }
                    currentChar = token.currentCharacter();
                }
                // We should never get left in a quote or single quote
                if (token.isComplete() && (inQuote || inSingleQuote))
                {
                    token.setParseError();
                }
                return stringValue;
            },

            numberValue: function(token)
            {
                var numberVal = "";

                this.chewWhitespace(token);
                var currentChar = token.currentCharacter();
                while(currentChar !== null)
                {
                    if (REGEXP_NUMBER.test(currentChar))
                    {
                        numberVal = numberVal + currentChar;
                        token.consumeCharacter();
                    }
                    else
                    {
                        // found not a number time to return
                        break;
                    }
                    currentChar = token.currentCharacter();
                }
                return numberVal;
            },

            wasPredicateComplete: function(predicate)
            {
                var foundPredicate = false;
                var arLen=jql_changed_predicates.length;
                for ( var i=0, len=arLen; i<len; ++i )
                {
                    if (predicate.toLowerCase() == jql_changed_predicates[i].value.toLowerCase())
                    {
                        foundPredicate = true;
                        break;
                    }

                }
                return foundPredicate;
            }

        };
    };

    JIRA.JQLAutoComplete.ParseResult = function() {

        var tokens = [];
        var tokenIdx = 0;
        return {

            getTokens: function()
            {
                return tokens;
            },

            addToken: function(token)
            {
                tokens[tokenIdx++] = token;
            },

            setLastFieldName: function(lastFieldName, token)
            {
                this.fieldNameStartIndex = (lastFieldName === null) ? token.getMaxTokenStringIdx() : (token.getTokenStringIdx() - lastFieldName.length);
                // Get rid of quotes if we need to
                this.lastFieldName = this.getUnquotedString(lastFieldName);
                tokens[tokenIdx++] = lastFieldName;
            },

            getLastFieldName: function()
            {
                return this.lastFieldName;
            },

            getLastFieldNameStartIndex: function()
            {
                return this.fieldNameStartIndex;
            },

            setLastOrderByFieldName: function(lastFieldName, token)
            {
                this.orderByFieldNameStartIndex = (lastFieldName === null) ? token.getMaxTokenStringIdx() : (token.getTokenStringIdx() - lastFieldName.length);
                // Get rid of quotes if we need to
                this.lastOrderByFieldName = this.getUnquotedString(lastFieldName);
                this.lastOrderByDirection = null;
                tokens[tokenIdx++] = lastFieldName;
            },

            getLastOrderByFieldName: function()
            {
                return this.lastOrderByFieldName;
            },

            getLastOrderByFieldNameStartIndex: function()
            {
                return this.orderByFieldNameStartIndex;
            },

            setLastOrderByDirection: function(lastDirection, token)
            {
                this.orderByDirectionStartIndex = (lastDirection === null) ? token.getMaxTokenStringIdx() : (token.getTokenStringIdx() - lastDirection.length);
                this.lastOrderByDirection = lastDirection;
                tokens[tokenIdx++] = lastDirection;
            },

            getLastOrderByDirection: function()
            {
                return this.lastOrderByDirection;
            },

            getLastOrderByDirectionStartIndex: function()
            {
                return this.orderByDirectionStartIndex;
            },

            setNeedsField: function()
            {
                this.needsOperator = false;
                this.needsOperand = false;
                this.needsLogicalOperator = false;
                this.needsOrderBy = false;
                this.needsField = true;
                this.needsOrderByField = false;
                this.needsOrderByDirection = false;
                this.needsListComma = false;
                this.needsWasPredicate = false;
                this.lastWasOperator = null;
            },

            getNeedsField: function()
            {
                return this.needsField;
            },

            setNeedsOrderByField: function()
            {
                this.needsOperator = false;
                this.needsOperand = false;
                this.needsLogicalOperator = false;
                this.needsOrderBy = false;
                this.needsField = false;
                this.needsOrderByField = true;
                this.needsOrderByDirection = false;
                this.lastOrderByDirection = null;
                this.needsListComma = false;
            },

            getNeedsOrderByField: function()
            {
                return this.needsOrderByField;
            },

            setNeedsOrderByDirection: function()
            {
                this.needsOperator = false;
                this.needsOperand = false;
                this.needsLogicalOperator = false;
                this.needsOrderBy = false;
                this.needsField = false;
                this.needsOrderByField = false;
                this.needsOrderByDirection = true;
                this.needsListComma = false;
            },

            getNeedsOrderByDirection: function()
            {
                return this.needsOrderByDirection;
            },

            setNeedsOrderByComma: function()
            {
                this.needsOperator = false;
                this.needsOperand = false;
                this.needsLogicalOperator = false;
                this.needsOrderBy = false;
                this.needsField = false;
                this.needsOrderByField = false;
                this.needsOrderByDirection = false;
                this.needsListComma = false;
            },

            setNeedsListComma: function()
            {
                this.needsOperator = false;
                this.needsOperand = false;
                this.needsLogicalOperator = false;
                this.needsOrderBy = false;
                this.needsField = false;
                this.needsOrderByField = false;
                this.needsOrderByDirection = false;
                this.needsListComma = true;
            },

            getNeedsListComma: function()
            {
                return this.needsListComma;
            },

            setLastOperator: function(lastOperator, startIndex)
            {
                this.lastOperator = lastOperator;
                this.operatorStartIndex = startIndex;
                tokens[tokenIdx++] = lastOperator;
            },

            setLastWasOperator: function(lastWasOperator)
            {
                this.lastWasOperator = lastWasOperator;
            },

            getLastWasOperator: function()
            {
                return this.lastWasOperator;
            },

            getLastOperator: function()
            {
                return this.lastOperator;
            },

            getLastOperatorStartIndex: function()
            {
                return this.operatorStartIndex;
            },

            setLastOperand: function(lastOperand, token)
            {
                this.operandStartIndex = (lastOperand === null) ? token.getMaxTokenStringIdx() : (token.getTokenStringIdx() - lastOperand.length);
                // Get rid of quotes if we need to
                this.lastOperand = this.getUnquotedString(lastOperand);
                tokens[tokenIdx++] = lastOperand;
            },

            getLastOperand: function()
            {
                return this.lastOperand;
            },

            setNeedsOperand: function()
            {
                this.needsField = false;
                this.needsOperator = false;
                this.needsLogicalOperator = false;
                this.needsOperand = true;
                this.needsOrderBy = false;
                this.needsOrderByField = false;
                this.needsOrderByDirection = false;
                this.needsListComma = false;
            },

            setNeedsOperandOrOperator: function()
            {
                this.needsField = false;
                this.needsOperator = true;
                this.needsLogicalOperator = false;
                this.needsOperand = true;
                this.needsOrderBy = false;
                this.needsOrderByField = false;
                this.needsOrderByDirection = false;
                this.needsListComma = false;
            },

            getNeedsOperand: function()
            {
                return this.needsOperand;
            },

            setNeedsPredicateOperand: function()
            {
                this.needsPredicateOperand = true;
            },

            resetNeedsPredicateOperand: function()
            {
                this.needsPredicateOperand = false;
                this.needsOperand = false;
            },

            getNeedsPredicateOperand: function()
            {
                return this.needsPredicateOperand;
            },

            getLastOperandStartIndex: function()
            {
                return this.operandStartIndex;
            },

            setLastOperandStartIndex: function(index)
            {
                this.operandStartIndex = index;
            },

            setLastLogicalOperator: function(lastLogicalOperator, startIndex)
            {
                this.lastLogicalOperator = lastLogicalOperator;
                this.logicalOperatorStartIndex = startIndex;
                tokens[tokenIdx++] = lastLogicalOperator;
            },

            setNeedsOperator: function()
            {
                this.needsField = false;
                this.needsOperand = false;
                this.needsLogicalOperator = false;
                this.needsOrderBy = false;
                this.needsOperator = true;
                this.needsOrderByField = false;
                this.needsOrderByDirection = false;
                this.needsListComma = false;
            },

            getNeedsOperatorOrOperand:  function()
            {
                return this.needsOperator && this.needsOperand;
            },

            getNeedsOperator: function()
            {
                return this.needsOperator;
            },

            getLastLogicalOperator: function()
            {
                return this.lastLogicalOperator;
            },

            getLastLogicalOperatorStartIndex: function()
            {
                return this.logicalOperatorStartIndex;
            },

            setNeedsLogicalOperator: function(token)
            {
                this.needsLogicalOperator = true;
                this.needsOperator = false;
                this.needsOperand = false;
                this.needsField = false;
                this.needsOrderByField = false;
                this.needsOrderByDirection = false;
                this.needsListComma = false;
                // Every time we need a logical operator we also could need an order by as long as we are not in parens
                this.needsOrderBy = !token.getInParens();
                // Every time a logical operator is needed you may need a predicate, but only if the last clause
                // was a was clause
                if (this.lastWasOperator != null)
                {
                    this.needsWasPredicate = true;
                }
            },

            setNeedsOpenParen: function(value)
            {
                this.needsOpenParen = value;
            },

            getNeedsOpenParen: function()
            {
                return this.needsOpenParen;
            },

            getNeedsLogicalOperator: function()
            {
                return this.needsLogicalOperator;
            },

            setNeedsOrderBy: function(value)
            {
                this.needsOrderBy = value;
            },

            getNeedsOrderBy: function()
            {
                return this.needsOrderBy;
            },

            getNeedsWasPredicate: function()
            {
                return this.needsWasPredicate;
            },

            setLastWasPredicate: function(lastWasPredicate, token)
            {
                this.wasPredicateStartIndex = (lastWasPredicate === null) ? token.getMaxTokenStringIdx() : (token.getTokenStringIdx() - lastWasPredicate.length);
                this.lastWasPredicate = lastWasPredicate;
                tokens[tokenIdx++] = lastWasPredicate;
            },

            getLastWasPredicate: function()
            {
                return this.lastWasPredicate;
            },

            resetLastPredicates : function()
            {
                this.lastLogicalOperator = null;
                this.lastWasPredicate = null;
                this.lastOperand = null;
                this.wasPredicateStartIndex = null;
                this.operandStartIndex = null;
            },

            getLastWasPredicateStartIndex: function()
            {
                return this.wasPredicateStartIndex;
            },

            setLastOrderBy: function(lastOrderBy, token)
            {
                this.orderByStartIndex = (lastOrderBy === null) ? token.getMaxTokenStringIdx() : (token.getTokenStringIdx() - lastOrderBy.length);
                this.lastOrderBy = lastOrderBy;
                tokens[tokenIdx++] = lastOrderBy;
            },

            getLastOrderBy: function()
            {
                return this.lastOrderBy;
            },

            getLastOrderByStartIndex: function()
            {
                return this.orderByStartIndex;
            },

            resetLogicalOperators: function()
            {
                this.lastLogicalOperator = null;
                this.logicalOperatorStartIndex = null;
                this.needsLogicalOperator = null;
            },

            getUnquotedString: function(value)
            {
                // We only remove the last quote if it is NOT preceeded by an escape character
                var secondToLastNotEsacape = value != null && value.length >= 3 && value.charAt(value.length - 2) != '\\';

                if (value != null && value.charAt(0) == '"')
                {
                    value = value.substring(1, value.length);

                    if (value.charAt(value.length - 1) == '"' && secondToLastNotEsacape)
                    {
                        value = value.substring(0, value.length - 1);
                    }
                }
                else if (value != null && value.charAt(0) == "'")
                {
                    value = value.substring(1, value.length);

                    if (value.charAt(value.length - 1) == "'" && secondToLastNotEsacape)
                    {
                        value = value.substring(0, value.length - 1);
                    }
                }

                return value;
            },

            setParseError: function(message)
            {
                this.parseError = true;
                this.parseErrorMsg = message;
            },

            getParseError: function()
            {
                return this.parseError;
            },

            getParseErrorMsg: function()
            {
                return this.parseErrorMsg;
            },

            setNeedsNothing: function()
            {
                this.needsOperator = false;
                this.needsOperand = false;
                this.needsLogicalOperator = false;
                this.needsOrderBy = false;
                this.needsField = false;
                this.needsOrderByField = false;
                this.needsOrderByDirection = false;
                this.needsOpenParen = false;
                this.needsListComma = false;
                this.needsWasPredicate = null;
                this.needsPredicateOperand = null;
                this.mustBeOperatorOrPredicate = null;
                this.lastWasOperator = null;
            },

            setOperandComplete: function()
            {
                this.operandComplete = true;
            },

            getOperandComplete: function()
            {
                return this.operandComplete;
            },

            setMustBeOperatorOrPredicate: function(state)
            {
                this.mustBeOperatorOrPredicate = state;
            },

            getMustBeOperatorOrPredicate: function()
            {
                return this.mustBeOperatorOrPredicate;
            },

            resetTerminalClause: function()
            {
                this.lastFieldName = null;
                this.fieldNameStartIndex = null;
                this.needsField = null;
                this.lastOperator = null;
                this.operatorStartIndex = null;
                this.needsOperator = null;
                this.lastOperand = null;
                this.operandStartIndex = null;
                this.needsOperand = null;
                this.operandComplete = null;
                this.needsOpenParen = null;
                this.needsListComma = false;
                this.mustBeOperatorOrPredicate = null;
            },

            init: function()
            {
                this.lastFieldName = null;
                this.fieldNameStartIndex = null;
                this.needsField = null;
                this.lastOperator = null;
                this.operatorStartIndex = null;
                this.needsOperator = null;
                this.lastOperand = null;
                this.operandStartIndex = null;
                this.needsOperand = null;
                this.lastLogicalOperator = null;
                this.logicalOperatorStartIndex = null;
                this.lastOrderByFieldName = null;
                this.lastOrderByFieldNameStartIndex = null;
                this.lastOrderByDirection = null;
                this.lastOrderByDirectionStartIndex = null;
                this.orderByStartIndex = null;
                this.lastOrderBy = null;
                this.needsOrderBy = null;
                this.needsOrderByField = null;
                this.needsOrderByDirection = null;
                this.operandComplete = null;
                this.needsOpenParen = null;
                this.needsListComma = null;
                this.wasPredicateStartIndex = null;
                this.lastWasPredicate = null;
                this.needsWasPredicate = null;
                this.needsPredicateOperand = null;
                this.mustBeOperatorOrPredicate = null;
                this.lastWasOperator = null;
            }
        };
    };

    JIRA.JQLAutoComplete.Token = function() {

        return {

            init: function(tokenString)
            {
                this.tokenStringIdx = 0;
                this.tokenString = tokenString;
                this.parseError = false;
                this.parseErrorMsg = null;
                this.result = JIRA.JQLAutoComplete.ParseResult();
                this.result.init();
                this.inParens = 0;
            },

            consumeCharacter: function()
            {
                this.tokenStringIdx ++;
            },

            consumeCharacters: function(numChars)
            {
                this.tokenStringIdx = this.tokenStringIdx + numChars;
            },

            backTrackToIdx: function(backTrackIdx)
            {
                this.tokenStringIdx = backTrackIdx;
                // Lets clear any parse errors that might have occurred as well
                this.parseError = false;
                this.parseErrorMsg = null;
                this.result.parseError = false;
                this.result.parseErrorMsg = null;
            },

            getTokenStringIdx: function()
            {
                return this.tokenStringIdx;
            },

            currentCharacter: function()
            {
                if (this.tokenStringIdx >= this.tokenString.length)
                {
                    return null;
                }
                return this.tokenString.charAt(this.tokenStringIdx);
            },

            remainingString: function()
            {
                if (this.tokenStringIdx >= this.tokenString.length)
                {
                    return null;
                }
                return this.tokenString.substr(this.tokenStringIdx, this.tokenString.length);
            },

            getMaxTokenStringIdx: function()
            {
                return this.tokenString.length;
            },

            isComplete: function()
            {
                if (this.parseError)
                {
                    return true;
                }
                return this.tokenStringIdx >= this.tokenString.length;
            },

            setInParens: function()
            {
                this.inParens++;
            },

            setOutOfParens: function()
            {
                // Lets never go into negative here
                if (this.inParens !== 0)
                {
                    this.inParens--;
                }
            },

            getInParens: function()
            {
                return this.inParens !== 0;
            },

            setParseError: function()
            {
                this.parseError = true;
                var preFixIdx = ((this.tokenStringIdx - 9) < 0) ? 0 : this.tokenStringIdx - 9;
                var errorPrefix = this.tokenString.substring(preFixIdx, this.tokenStringIdx - 1);
                this.result.setParseError("..." + errorPrefix + "^" + this.tokenString.substring(this.tokenStringIdx, this.tokenString.length));
            },

            getParseError: function()
            {
                return this.parseError;
            },

            getResult: function()
            {
                return this.result;
            }

        };

    };

    // Module definition
    define && define(function() {
        return JIRA.JQLAutoComplete;
    });
})();
