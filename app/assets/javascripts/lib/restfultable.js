/* globals AJS, Backbone, _ */

(function ($) {

    /**
     * Triggers a custom event on the AJS object
     *
     * @param {String} name - name of event
     * @param {Array} args - args for event handler
     */
    AJS.triggerEvt = function (name, args) {
        $(AJS).trigger(name, args);
    };

    /**
     * Binds handler to the AJS object
     *
     * @param {String} name
     * @param {Function} func
     */
    AJS.bindEvt = function (name, func) {
        $(AJS).bind(name, func);
    };

    /**
     * Some generic error handling that fires event in multiple contexts
     * - on AJS object
     * - on Instance
     * - on AJS object with prefixed id.
     *
     * @param evt
     * @param inst
     * @param args
     */
    AJS.triggerEvtForInst = function (evt, inst, args) {
        $(inst).trigger(evt, args);
        AJS.triggerEvt(evt, args);
        if (inst.id) {
            AJS.triggerEvt(inst.id + "_" + evt, args);
        }
    };

})(AJS.$);

/**
 * Serializes form fields within the given element to a JSON object
 *
 * {
 *    fieldName: "fieldValue"
 * }
 *
 * @returns {Object}
 */
jQuery.fn.serializeObject = function () {

    var data = {};

    this.find(":input:not(:button):not(:submit):not(:radio):not('select[multiple]')").each(function () {

        if (this.name === "") {
            return;
        }

        if (this.value === null) {
            this.value = "";
        }

        data[this.name] = this.value.match(/^(tru|fals)e$/i) ?
            this.value.toLowerCase() == "true" : this.value;
    });

    this.find("input:radio:checked").each(function(){
        data[this.name] = this.value;
    });

    this.find("select[multiple]").each(function(){

        var $select = jQuery(this),
            val = $select.val();

        if ($select.data("aui-ss")) {
            if (val) {
                data[this.name] = val[0];
            } else {
                data[this.name] = "";
            }
        } else {

            if (val !== null) {
                data[this.name] = val;
            } else {
                data[this.name] = [];
            }
        }
    });

    return data;
};

/*
 * jQuery Color Animations
 * Copyright 2007 John Resig
 * Released under the MIT and GPL licenses.
 */

(function(jQuery){

    // We override the animation for all of these color styles
    jQuery.each(['backgroundColor', 'borderBottomColor', 'borderLeftColor', 'borderRightColor', 'borderTopColor', 'color', 'outlineColor'], function(i,attr){
        jQuery.fx.step[attr] = function(fx){
            if ( fx.state == 0 ) {
                fx.start = getColor( fx.elem, attr );
                fx.end = getRGB( fx.end );
            }

            fx.elem.style[attr] = "rgb(" + [
                Math.max(Math.min( parseInt((fx.pos * (fx.end[0] - fx.start[0])) + fx.start[0]), 255), 0),
                Math.max(Math.min( parseInt((fx.pos * (fx.end[1] - fx.start[1])) + fx.start[1]), 255), 0),
                Math.max(Math.min( parseInt((fx.pos * (fx.end[2] - fx.start[2])) + fx.start[2]), 255), 0)
            ].join(",") + ")";
        }
    });

    // Color Conversion functions from highlightFade
    // By Blair Mitchelmore
    // http://jquery.offput.ca/highlightFade/

    // Parse strings looking for color tuples [255,255,255]
    function getRGB(color) {
        var result;

        // Check if we're already dealing with an array of colors
        if ( color && color.constructor == Array && color.length == 3 )
            return color;

        // Look for rgb(num,num,num)
        if (result = /rgb\(\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*,\s*([0-9]{1,3})\s*\)/.exec(color))
            return [parseInt(result[1]), parseInt(result[2]), parseInt(result[3])];

        // Look for rgb(num%,num%,num%)
        if (result = /rgb\(\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*,\s*([0-9]+(?:\.[0-9]+)?)\%\s*\)/.exec(color))
            return [parseFloat(result[1])*2.55, parseFloat(result[2])*2.55, parseFloat(result[3])*2.55];

        // Look for #a0b1c2
        if (result = /#([a-fA-F0-9]{2})([a-fA-F0-9]{2})([a-fA-F0-9]{2})/.exec(color))
            return [parseInt(result[1],16), parseInt(result[2],16), parseInt(result[3],16)];

        // Look for #fff
        if (result = /#([a-fA-F0-9])([a-fA-F0-9])([a-fA-F0-9])/.exec(color))
            return [parseInt(result[1]+result[1],16), parseInt(result[2]+result[2],16), parseInt(result[3]+result[3],16)];

        // Otherwise, we're most likely dealing with a named color
        return colors[jQuery.trim(color).toLowerCase()];
    }

    function getColor(elem, attr) {
        var color;

        do {
            color = jQuery.curCSS(elem, attr);

            // Keep going until we find an element that has color, or we hit the body
            if ( color != '' && color != 'transparent' || jQuery.nodeName(elem, "body") )
                break;

            attr = "backgroundColor";
        } while ( elem = elem.parentNode );

        return getRGB(color);
    };

    // Some named colors to work with
    // From Interface by Stefan Petre
    // http://interface.eyecon.ro/

    var colors = {
        aqua:[0,255,255],
        azure:[240,255,255],
        beige:[245,245,220],
        black:[0,0,0],
        blue:[0,0,255],
        brown:[165,42,42],
        cyan:[0,255,255],
        darkblue:[0,0,139],
        darkcyan:[0,139,139],
        darkgrey:[169,169,169],
        darkgreen:[0,100,0],
        darkkhaki:[189,183,107],
        darkmagenta:[139,0,139],
        darkolivegreen:[85,107,47],
        darkorange:[255,140,0],
        darkorchid:[153,50,204],
        darkred:[139,0,0],
        darksalmon:[233,150,122],
        darkviolet:[148,0,211],
        fuchsia:[255,0,255],
        gold:[255,215,0],
        green:[0,128,0],
        indigo:[75,0,130],
        khaki:[240,230,140],
        lightblue:[173,216,230],
        lightcyan:[224,255,255],
        lightgreen:[144,238,144],
        lightgrey:[211,211,211],
        lightpink:[255,182,193],
        lightyellow:[255,255,224],
        lime:[0,255,0],
        magenta:[255,0,255],
        maroon:[128,0,0],
        navy:[0,0,128],
        olive:[128,128,0],
        orange:[255,165,0],
        pink:[255,192,203],
        purple:[128,0,128],
        violet:[128,0,128],
        red:[255,0,0],
        silver:[192,192,192],
        white:[255,255,255],
        yellow:[255,255,0]
    };

})(jQuery);

(function ($) {

    /**
     * A table whose entries/rows can be retrieved, added and updated via REST (CRUD).
     * It uses backbone.js to sync the table's state back to the server, avoiding page refreshes.
     *
     * @class RestfulTable
     */
    AJS.RestfulTable = Backbone.View.extend({

        /**
         * @constructor
         * @param {!Object} options
         * ... {!Object} resources
         * ... ... {(string|function(function(Array.<Object>)))} all - URL of REST resource OR function that retrieves all entities.
         * ... ... {string} self - URL of REST resource to sync a single entities state (CRUD).
         * ... {!(selector|Element|jQuery)} el - Table element or selector of the table element to populate.
         * ... {!Array.<Object>} columns - Which properties of the entities to render. The id of a column maps to the property of an entity.
         * ... {Object} views
         * ... ... {AJS.RestfulTable.EditRow} editRow - Backbone view that renders the edit & create row. Your view MUST extend AJS.RestfulTable.EditRow.
         * ... ... {AJS.RestfulTable.Row} row - Backbone view that renders the readonly row. Your view MUST extend AJS.RestfulTable.Row.
         * ... {boolean} allowEdit - Is the table editable. If true, clicking row will switch it to edit state. Default true.
         * ... {boolean} allowDelete - Can entries be removed from the table, default true.
         * ... {boolean} allowCreate - Can new entries be added to the table, default true.
         * ... {boolean} allowReorder - Can we drag rows to reorder them, default false.
         * ... {boolean} autoFocus - Automatically set focus to first field on init, default false.
         * ... {boolean} reverseOrder - Reverse the order of rows, default false.
         * ... {boolean} silent - Do not trigger a "refresh" event on sort, default false.
         * ... {String} id - The id for the table. This id will be used to fire events specific to this instance.
         * ... {string} createPosition - If set to "bottom", place the create form at the bottom of the table instead of the top.
         * ... {string} addPosition - If set to "bottom", add new rows at the bottom of the table instead of the top. If undefined, createPosition will be used to define where to add the new row.
         * ... {string} noEntriesMsg - Text to display under the table header if it is empty, default empty.
         * ... {string} loadingMsg - Text/HTML to display while loading, default "Loading".
         * ... {string} submitAccessKey - Access key for submitting.
         * ... {string} cancelAccessKey - Access key for canceling.
         * ... {function(Object): (string|function(number, string): string)} deleteConfirmation - HTML to display in popup to confirm deletion.
         * ... {function(string): (selector|jQuery|Element)} fieldFocusSelector - Element to focus on given a name.
         * ... {AJS.RestfulTable.EntryModel} model - Backbone model representing a row, default AJS.RestfulTable.EntryModel.
         * ... {Backbone.Collection} Collection - Backbone collection representing the entire table, default Backbone.Collection.
         */
        initialize: function (options) {

            var instance = this;


            // combine default and user options
            instance.options = $.extend(true, instance._getDefaultOptions(options), options);

            // Prefix events for this instance with this id.
            instance.id = this.options.id;

            // faster lookup
            instance._event = AJS.RestfulTable.Events;
            instance.classNames = AJS.RestfulTable.ClassNames;
            instance.dataKeys = AJS.RestfulTable.DataKeys;

            // shortcuts to popular elements
            this.$table = $(options.el)
                .addClass(this.classNames.RESTFUL_TABLE)
                .addClass(this.classNames.ALLOW_HOVER)
                .addClass("aui")
                .addClass(instance.classNames.LOADING);

            this.$table.wrapAll("<form class='aui' action='#' />");

            this.$thead = $("<thead/>");
            this.$theadRow = $("<tr />").appendTo(this.$thead);
            this.$tbody = $("<tbody/>");

            if (!this.$table.length) {
                throw new Error("AJS.RestfulTable: Init failed! The table you have specified [" + this.$table.selector + "] cannot be found.")
            }

            if (!this.options.columns) {
                throw new Error("AJS.RestfulTable: Init failed! You haven't provided any columns to render.")
            }

            // Let user know the table is loading
            this.showGlobalLoading();

            $.each(this.options.columns, function (i, column) {
                var header = $.isFunction(column.header) ? column.header() : column.header;
                if (typeof header === "undefined") {
                    console.warn("You have not specified [header] for column [" + column.id + "]. Using id for now...");
                    header = column.id;
                }

                instance.$theadRow.append("<th>" + header + "</th>");
            });

            // columns for submit buttons and loading indicator used when editing
            instance.$theadRow.append("<th></th><th></th>");

            // create a new Backbone collection to represent rows (http://documentcloud.github.com/backbone/#Collection)
            this._models = this._createCollection();

            // shortcut to the class we use to create rows
            this._rowClass = this.options.views.row;

            this.editRows = []; // keep track of rows that are being edited concurrently

            this.$table.closest("form").submit(function (e) {
                if (instance.focusedRow) {
                    // Delegates saving of row. See AJS.RestfulTable.EditRow.submit
                    instance.focusedRow.trigger(instance._event.SAVE);
                }
                e.preventDefault();
            });

            if (this.options.allowReorder) {

                // Add allowance for another cell to the thead
                this.$theadRow.prepend("<th />");

                // Allow drag and drop reordering of rows
                this.$tbody.sortable({
                    handle: "." +this.classNames.DRAG_HANDLE,
                    helper: function(e, elt) {
                        var helper = $("<div/>").attr("class", elt.attr("class")).addClass(instance.classNames.MOVEABLE);
                        elt.children().each(function (i) {
                            var $td = $(this);
                            var width = $td.outerWidth();
                            helper.append($("<div/>").html($td.html()).attr("class", $td.attr("class")).width(width));
                        });
                        helper = $("<div class='aui-restfultable-readonly'/>").append(helper); // Basically just to get the styles.
                        helper.css({left: elt.offset().left}); // To align with the other table rows, since we've locked scrolling on x.
                        helper.appendTo(document.body);
                        return helper;
                    },
                    start: function (event, ui) {
                        var cachedHeight = ui.helper[0].clientHeight;
                        var $this = ui.placeholder.find("td");

                        // Make sure that when we start dragging widths do not change
                        ui.item
                            .addClass(instance.classNames.MOVEABLE)
                            .children().each(function (i) {
                                $(this).width($this.eq(i).width());
                            });

                        // Create a <td> to add to the placeholder <tr> to inherit CSS styles.
                        var td = '<td colspan="' + instance.getColumnCount() + '">&nbsp;</td>';

                        ui.placeholder.html(td).css({
                            height: cachedHeight,
                            visibility: 'visible'
                        });

                        // Stop hover effects etc from occuring as we move the mouse (while dragging) over other rows
                        instance.getRowFromElement(ui.item[0]).trigger(instance._event.MODAL);
                    },
                    stop: function (event, ui) {
                        if (AJS.$(ui.item[0]).is(":visible")) {
                            ui.item
                                .removeClass(instance.classNames.MOVEABLE)
                                .children().attr("style", "");

                            ui.placeholder.removeClass(instance.classNames.ROW);

                            // Return table to a normal state
                            instance.getRowFromElement(ui.item[0]).trigger(instance._event.MODELESS);
                        }
                    },
                    update: function (event, ui) {

                        var nextModel,
                            nextRow,
                            data = {},
                            row = instance.getRowFromElement(ui.item[0]);

                        if (row) {

                            if (instance.options.reverseOrder) {
                                // Everything is backwards here because on the client we are in reverse order.
                                nextRow = ui.item.next();
                                if (!nextRow.length) {
                                    data.position = "First";
                                } else {
                                    nextModel = instance.getRowFromElement(nextRow).model;
                                    data.after = nextModel.url();
                                }
                            } else {
                                nextRow = ui.item.prev();
                                if (!nextRow.length) {
                                    data.position = "First";
                                } else {
                                    nextModel = instance.getRowFromElement(nextRow).model;
                                    data.after = nextModel.url();
                                }
                            }


                            $.ajax({
                                url: row.model.url() + "/move",
                                type: "POST",
                                dataType: "json",
                                contentType: "application/json",
                                data: JSON.stringify(data),
                                complete: function () {
                                    // hides loading indicator (spinner)
                                    row.hideLoading();
                                },
                                success: function (xhr) {
                                    AJS.triggerEvtForInst(instance._event.REORDER_SUCCESS, instance, [xhr]);
                                },
                                error: function (xhr) {
                                    var responseData = $.parseJSON(xhr.responseText || xhr.data);
                                    AJS.triggerEvtForInst(instance._event.SERVER_ERROR, instance, [responseData, xhr, this]);
                                }
                            });

                            // shows loading indicator (spinner)
                            row.showLoading();
                        }
                    },
                    axis: "y",
                    delay: 0,
                    containment: "document",
                    cursor: "move",
                    scroll: true,
                    zIndex: 8000
                });

                // Prevent text selection while reordering.
                this.$tbody.bind("selectstart mousedown", function (event) {
                    return !$(event.target).is("." + instance.classNames.DRAG_HANDLE);
                });
            }


            if (this.options.allowCreate !== false) {

                // Create row responsible for adding new entries ...
                this._createRow = new this.options.views.editRow({
                    columns: this.options.columns,
                    isCreateRow: true,
                    model: this.options.model.extend({
                        url: function () {
                            return instance.options.resources.self;
                        }
                    }),
                    cancelAccessKey: this.options.cancelAccessKey,
                    submitAccessKey: this.options.submitAccessKey,
                    allowReorder: this.options.allowReorder,
                    fieldFocusSelector: this.options.fieldFocusSelector
                })
                    .bind(this._event.CREATED, function (values) {
                        if ((instance.options.addPosition == undefined && instance.options.createPosition === "bottom")
                            || instance.options.addPosition === "bottom") {
                            instance.addRow(values);
                        } else {
                            instance.addRow(values, 0);
                        }
                    })
                    .bind(this._event.VALIDATION_ERROR, function () {
                        this.trigger(instance._event.FOCUS);
                    })
                    .render({
                        errors: {},
                        values: {}
                    });

                // ... and appends it as the first row
                this.$create = $('<tbody class="' + this.classNames.CREATE + '" />')
                    .append(this._createRow.el);

                // Manage which row has focus
                this._applyFocusCoordinator(this._createRow);

                // focus create row
                this._createRow.trigger(this._event.FOCUS);
            }

            // when a model is removed from the collection, remove it from the viewport also
            this._models.bind("remove", function (model) {
                $.each(instance.getRows(), function (i, row) {
                    if (row.model === model) {
                        if (row.hasFocus() && instance._createRow) {
                            instance._createRow.trigger(instance._event.FOCUS);
                        }
                        instance.removeRow(row);
                    }
                });
            });

            if ($.isFunction(this.options.resources.all)) {
                this.options.resources.all(function (entries) {
                    instance.populate(entries);
                });
            } else {
                $.get(this.options.resources.all, function (entries) {
                    instance.populate(entries);
                });
            }
        },

        _createCollection: function() {
            var instance = this;

            // create a new Backbone collection to represent rows (http://documentcloud.github.com/backbone/#Collection)
            var rowsAwareCollection = this.options.Collection.extend({
                // Force the collection to re-sort itself. You don't need to call this under normal
                // circumstances, as the set will maintain sort order as each item is added.
                sort:function (options) {
                    options || (options = {});
                    if (!this.comparator) {
                        throw new Error('Cannot sort a set without a comparator');
                    }
                    this.tableRows = instance.getRows();
                    this.models = this.sortBy(this.comparator);
                    this.tableRows = undefined;
                    if (!options.silent) {
                        this.trigger('refresh', this, options);
                    }
                    return this;
                },
                remove:function (models, options) {
                    this.tableRows = instance.getRows();
                    Backbone.Collection.prototype.remove.apply(this, arguments);
                    this.tableRows = undefined;
                    return this;
                }
            });

            return new rowsAwareCollection([], {
                comparator:function (row) {
                    // sort models in collection based on dom ordering
                    var index;
                    $.each(this.tableRows !== undefined ? this.tableRows : instance.getRows(), function (i) {
                        if (this.model.id === row.id) {
                            index = i;
                            return false;
                        }
                    });
                    return index;
                }
            });
        },

        /**
         * Refreshes table with entries
         *
         * @param entries
         */
        populate: function (entries) {

            if (this.options.reverseOrder) {
                entries.reverse();
            }

            this.hideGlobalLoading();
            if (entries && entries.length) {
                // Empty the models collection
                this._models.reset([], { silent: true });
                // Add all the entries to collection and render them
                this.renderRows(entries);
                // show message to user if we have no entries
                if (this.isEmpty()) {
                    this.showNoEntriesMsg();
                }
            } else {
                this.showNoEntriesMsg();
            }

            // Ok, lets let everyone know that we are done...
            this.$table
                .append(this.$thead);

            if (this.options.createPosition === "bottom") {
                this.$table.append(this.$tbody)
                    .append(this.$create);
            } else {
                this.$table
                    .append(this.$create)
                    .append(this.$tbody);
            }

            this.$table.removeClass(this.classNames.LOADING)
                .trigger(this._event.INITIALIZED, [this]);

            AJS.triggerEvtForInst(this._event.INITIALIZED, this, [this]);

            if (this.options.autoFocus) {
                this.$table.find(":input:text:first").focus(); // set focus to first field
            }
        },

        /**
         * Shows loading indicator and text
         *
         * @return {AJS.RestfulTable}
         */
        showGlobalLoading: function () {

            if (!this.$loading) {
                this.$loading =  $('<div class="aui-restfultable-init">' + AJS.RestfulTable.throbber() +
                    '<span class="aui-restfultable-loading">' + this.options.loadingMsg + '</span></div>');
            }
            if (!this.$loading.is(":visible")) {
                this.$loading.insertAfter(this.$table);
            }

            return this
        },

        /**
         * Hides loading indicator and text
         * @return {AJS.RestfulTable}
         */
        hideGlobalLoading: function () {
            if (this.$loading) {
                this.$loading.remove();
            }
            return this;
        },


        /**
         * Adds row to collection and renders it
         *
         * @param {Object} values
         * @param {number} index
         * @return {AJS.RestfulTable}
         */
        addRow: function (values, index) {

            var view,
                model;

            if (!values.id) {
                throw new Error("AJS.RestfulTable.addRow: to add a row values object must contain an id. "
                    + "Maybe you are not returning it from your restend point?"
                    + "Recieved:" + JSON.stringify(values));
            }

            model = new this.options.model(values);


            view = this._renderRow(model, index);

            this._models.add(model);
            this.removeNoEntriesMsg();

            // Let everyone know we added a row
            AJS.triggerEvtForInst(this._event.ROW_ADDED, this, [view, this]);
            return this;
        },

        /**
         * Provided a view, removes it from display and backbone collection
         *
         * @param {AJS.RestfulTable.Row}
         */
        removeRow: function (row) {

            this._models.remove(row.model);
            row.remove();

            if (this.isEmpty()) {
                this.showNoEntriesMsg();
            }

            // Let everyone know we removed a row
            AJS.triggerEvtForInst(this._event.ROW_REMOVED, this, [row, this]);
        },

        /**
         * Is there any entries in the table
         *
         * @return {Boolean}
         */
        isEmpty: function () {
            return this._models.length === 0;
        },

        /**
         * Gets all models
         *
         * @return {Backbone.Collection}
         */
        getModels: function () {
            return this._models;
        },

        /**
         * Gets table body
         *
         * @return {jQuery}
         */
        getTable: function () {
            return this.$table;
        },

        /**
         * Gets table body
         *
         * @return {jQuery}
         */
        getTableBody: function () {
            return this.$tbody;
        },

        /**
         * Gets create Row
         *
         * @return {B
         */
        getCreateRow: function () {
            return this._createRow;
        },

        /**
         * Gets the number of table colums
         *
         * @return {Number}
         */
        getColumnCount: function () {
            var staticFieldCount = 2; // accounts for the columns allocated to submit buttons and loading indicator
            if (this.allowReorder) ++staticFieldCount;
            return this.options.columns.length + staticFieldCount;
        },

        /**
         * Get the AJS.RestfulTable.Row that corresponds to the given <tr> element.
         *
         * @param {HTMLElement} tr
         * @return {?AJS.RestfulTable.Row}
         */
        getRowFromElement: function (tr) {
            return $(tr).data(this.dataKeys.ROW_VIEW);
        },

        /**
         * Shows message {options.noEntriesMsg} to the user if there are no entries
         *
         * @return {AJS.RestfulTable}
         */
        showNoEntriesMsg: function () {

            if (this.$noEntries) {
                this.$noEntries.remove();
            }

            this.$noEntries = $("<tr>")
                .addClass(this.classNames.NO_ENTRIES)
                .append($("<td>")
                    .attr("colspan", this.getColumnCount())
                    .text(this.options.noEntriesMsg)
                )
                .appendTo(this.$tbody);

            return this;
        },

        /**
         * Removes message {options.noEntriesMsg} to the user if there ARE entries
         *
         * @return {AJS.RestfulTable}
         */
        removeNoEntriesMsg: function () {
            if (this.$noEntries && this._models.length > 0) {
                this.$noEntries.remove();
            }
            return this;
        },

        /**
         * Gets the AJS.RestfulTable.Row from their associated <tr> elements
         *
         * @return {Array<AJS.RestfulTable.Row>}
         */
        getRows: function () {

            var instance = this,
                views = [];

            this.$tbody.find("." + this.classNames.READ_ONLY).each(function () {

                var $row = $(this),
                    view = $row.data(instance.dataKeys.ROW_VIEW);

                if (view) {
                    views.push(view);
                }
            });

            return views;
        },

        /**
         * Appends entry to end or specified index of table
         *
         * @param {AJS.RestfulTable.EntryModel} model
         * @param index
         * @return {jQuery}
         */
        _renderRow: function (model, index) {

            var instance = this,
                $rows = this.$tbody.find("." + this.classNames.READ_ONLY),
                $row,
                view;

            view = new this._rowClass({
                model: model,
                columns: this.options.columns,
                allowEdit: this.options.allowEdit,
                allowDelete: this.options.allowDelete,
                allowReorder: this.options.allowReorder,
                deleteConfirmation: this.options.deleteConfirmation
            });

            this.removeNoEntriesMsg();

            view.bind(this._event.ROW_EDIT, function (field) {
                AJS.triggerEvtForInst(this._event.EDIT_ROW, {}, [this, instance]);
                instance.edit(this, field);
            });

            $row = view.render().$el;

            if (index !== -1) {

                if (typeof index === "number" && $rows.length !== 0) {
                    $row.insertBefore($rows[index]);
                } else {
                    this.$tbody.append($row);
                }
            }

            $row.data(this.dataKeys.ROW_VIEW, view);

            // deactivate all rows - used in the cases, such as opening a dropdown where you do not want the table editable
            // or any interactions
            view.bind(this._event.MODAL, function () {
                instance.$table.removeClass(instance.classNames.ALLOW_HOVER);
                instance.$tbody.sortable("disable");
                $.each(instance.getRows(), function () {
                    if (!instance.isRowBeingEdited(this)) {
                        this.delegateEvents({}); // clear all events
                    }
                });
            });

            view.bind(this._event.ANIMATION_STARTED, function () {
                instance.$table.removeClass(instance.classNames.ALLOW_HOVER);
            });

            view.bind(this._event.ANIMATION_FINISHED, function () {
                instance.$table.addClass(instance.classNames.ALLOW_HOVER);
            });

            // activate all rows - used in the cases, such as opening a dropdown where you do not want the table editable
            // or any interactions
            view.bind(this._event.MODELESS, function () {
                instance.$table.addClass(instance.classNames.ALLOW_HOVER);
                instance.$tbody.sortable("enable");
                $.each(instance.getRows(), function () {
                    if (!instance.isRowBeingEdited(this)) {
                        this.delegateEvents(); // rebind all events
                    }
                });
            });

            // ensure that when this row is focused no other are
            this._applyFocusCoordinator(view);

            this.trigger(this._event.ROW_INITIALIZED, view);

            return view;
        },

        /**
         * Returns if the row is edit mode or note
         *
         * @param {AJS.RestfulTable.Row} - read onyl row to check if being edited
         * @return {Boolean}
         */
        isRowBeingEdited: function (row) {

            var isBeingEdited = false;

            $.each(this.editRows, function () {
                if (this.el === row.el) {
                    isBeingEdited = true;
                    return false;
                }
            });

            return isBeingEdited;
        },

        /**
         * Ensures that when supplied view is focused no others are
         *
         * @param {Backbone.View} view
         * @return {AJS.RestfulTable}
         */
        _applyFocusCoordinator: function (view) {

            var instance = this;

            if (!view.hasFocusBound) {

                view.hasFocusBound = true;

                view.bind(this._event.FOCUS, function () {
                    if (instance.focusedRow && instance.focusedRow !== view) {
                        instance.focusedRow.trigger(instance._event.BLUR);
                    }
                    instance.focusedRow = view;
                    if (view instanceof AJS.RestfulTable.Row && instance._createRow) {
                        instance._createRow.enable();
                    }
                });
            }

            return this;
        },

        /**
         * Remove specificed row from collection holding rows being concurrently edited
         *
         * @param {AJS.RestfulTable.EditRow} editView
         * @return {AJS.RestfulTable}
         */
        _removeEditRow: function (editView) {
            var index = $.inArray(editView, this.editRows);
            this.editRows.splice(index, 1);
            return this;
        },

        /**
         * Focuses last row still being edited or create row (if it exists)
         *
         * @return {AJS.RestfulTable}
         */
        _shiftFocusAfterEdit: function () {

            if (this.editRows.length > 0) {
                this.editRows[this.editRows.length-1].trigger(this._event.FOCUS);
            } else if (this._createRow) {
                this._createRow.trigger(this._event.FOCUS);
            }

            return this;
        },

        /**
         * Evaluate if we save row when we blur. We can only do this when there is one row being edited at a time, otherwise
         * it causes an infinate loop JRADEV-5325
         *
         * @return {boolean}
         */
        _saveEditRowOnBlur: function () {
            return this.editRows.length <= 1;
        },

        /**
         * Dismisses rows being edited concurrently that have no changes
         */
        dismissEditRows: function () {
            var instance = this;
            $.each(this.editRows, function () {
                if (!this.hasUpdates()) {
                    this.trigger(instance._event.FINISHED_EDITING);
                }
            });
        },

        /**
         * Converts readonly row to editable view
         *
         * @param {Backbone.View} row
         * @param {String} field - field name to focus
         * @return {Backbone.View} editRow
         */
        edit: function (row, field) {

            var instance = this,
                editRow = new this.options.views.editRow({
                    el: row.el,
                    columns: this.options.columns,
                    isUpdateMode: true,
                    allowReorder: this.options.allowReorder,
                    fieldFocusSelector: this.options.fieldFocusSelector,
                    model: row.model,
                    cancelAccessKey: this.options.cancelAccessKey,
                    submitAccessKey: this.options.submitAccessKey
                }),
                values = row.model.toJSON();
            values.update = true;
            editRow.render({
                errors: {},
                update: true,
                values: values
            })
                .bind(instance._event.UPDATED, function (model, focusUpdated) {
                    instance._removeEditRow (this);
                    this.unbind();
                    row.render().delegateEvents(); // render and rebind events
                    row.trigger(instance._event.UPDATED); // trigger blur fade out
                    if (focusUpdated !== false) {
                        instance._shiftFocusAfterEdit();
                    }
                })
                .bind(instance._event.VALIDATION_ERROR, function () {
                    this.trigger(instance._event.FOCUS);
                })
                .bind(instance._event.FINISHED_EDITING, function () {
                    instance._removeEditRow(this);
                    row.render().delegateEvents();
                    this.unbind();  // avoid any other updating, blurring, finished editing, cancel events being fired
                })
                .bind(instance._event.CANCEL, function () {
                    instance._removeEditRow(this);
                    this.unbind();  // avoid any other updating, blurring, finished editing, cancel events being fired
                    row.render().delegateEvents(); // render and rebind events
                    instance._shiftFocusAfterEdit();
                })
                .bind(instance._event.BLUR, function () {
                    instance.dismissEditRows(); // dismiss edit rows that have no changes
                    if (instance._saveEditRowOnBlur()) {
                        this.trigger(instance._event.SAVE, false);  // save row, which if successful will call the updated event above
                    }
                });

            // Ensure that if focus is pulled to another row, we blur the edit row
            this._applyFocusCoordinator(editRow);

            // focus edit row, which has the flow on effect of blurring current focused row
            editRow.trigger(instance._event.FOCUS, field);

            // disables form fields
            if (instance._createRow) {
                instance._createRow.disable();
            }

            this.editRows.push(editRow);

            return editRow;
        },


        /**
         * Renders all specified rows
         *
         * @param {Array} array of objects describing Backbone.Model's to render
         * @return {AJS.RestfulTable}
         */
        renderRows: function (rows) {
            var comparator = this._models.comparator, els = [];

            this._models.comparator = undefined; // disable temporarily, assume rows are sorted

            var models = _.map(rows, function(row) {
                var model = new this.options.model(row);
                els.push(this._renderRow(model, -1).el);
                return model;
            }, this);
            this._models.add(models, {silent:true});

            this._models.comparator = comparator;

            this.removeNoEntriesMsg();

            this.$tbody.append(els);

            return this;
        },

        /**
         * Gets default options
         *
         * @param {Object} options
         */
        _getDefaultOptions: function (options) {
            return {
                model: options.model || AJS.RestfulTable.EntryModel,
                allowEdit: true,
                views: {
                    editRow: AJS.RestfulTable.EditRow,
                    row: AJS.RestfulTable.Row
                },
                Collection: Backbone.Collection.extend({
                    url: options.resources.self,
                    model: options.model || AJS.RestfulTable.EntryModel
                }),
                allowReorder: false,
                fieldFocusSelector: function(name) {
                    return ":input[name=" + name + "], #" + name;
                },
                loadingMsg: options.loadingMsg || "Loading"
            }
        }

    });

    AJS.RestfulTable.throbber = function throbberHtml() { return '<span class="aui-restfultable-throbber"></span>'; }

    // jQuery data keys (http://api.jquery.com/jQuery.data/)
    AJS.RestfulTable.DataKeys = {
        ENABLED_SUBMIT: "enabledSubmit",
        ROW_VIEW: "RestfulTable_Row_View"
    };

    // CSS style classes. DON'T hard code
    AJS.RestfulTable.ClassNames = {
        NO_VALUE: "aui-restfultable-editable-no-value",
        NO_ENTRIES: "aui-restfultable-no-entires",
        RESTFUL_TABLE: "aui-restfultable",
        ROW: "aui-restfultable-row",
        READ_ONLY: "aui-restfultable-readonly",
        ACTIVE: "aui-restfultable-active",
        ALLOW_HOVER: "aui-restfultable-allowhover",
        FOCUSED: "aui-restfultable-focused",
        MOVEABLE: "aui-restfultable-movable",
        ANIMATING: "aui-restfultable-animate",
        DISABLED: "aui-restfultable-disabled",
        SUBMIT: "aui-restfultable-submit",
        CANCEL: "aui-restfultable-cancel",
        EDIT_ROW: "aui-restfultable-editrow",
        CREATE: "aui-restfultable-create",
        DRAG_HANDLE: "aui-restfultable-draghandle",
        ORDER: "aui-restfultable-order",
        EDITABLE: "aui-restfultable-editable",
        ERROR: "error",
        DELETE: "aui-restfultable-delete",
        LOADING: "loading"
    };

    // Custom events
    AJS.RestfulTable.Events = {

        // AJS events
        REORDER_SUCCESS: "RestfulTable.reorderSuccess",
        ROW_ADDED: "RestfulTable.rowAdded",
        ROW_REMOVED: "RestfulTable.rowRemoved",
        EDIT_ROW: "RestfulTable.switchedToEditMode",
        SERVER_ERROR: "RestfulTable.serverError",

        // backbone events
        CREATED: "created",
        UPDATED: "updated",
        FOCUS: "focus",
        BLUR: "blur",
        SUBMIT: "submit",
        SAVE: "save",
        MODAL: "modal",
        MODELESS: "modeless",
        CANCEL: "cancel",
        CONTENT_REFRESHED: "contentRefreshed",
        RENDER: "render",
        FINISHED_EDITING: "finishedEditing",
        VALIDATION_ERROR: "validationError",
        SUBMIT_STARTED: "submitStarted",
        SUBMIT_FINISHED: "submitFinished",
        ANIMATION_STARTED: "animationStarted",
        ANIMATION_FINISHED: "animationFinisehd",
        INITIALIZED: "initialized",
        ROW_INITIALIZED: "rowInitialized",
        ROW_EDIT: "editRow"
    };

})(AJS.$);

(function ($) {

    /**
     * A class provided to fill some gaps with the out of the box Backbone.Model class. Most notiably the inability
     * to send ONLY modified attributes back to the server.
     *
     * @class EntryModel
     * @namespace AJS.RestfulTable
     */
    AJS.RestfulTable.EntryModel = Backbone.Model.extend({


        sync: function (method, model, options) {
            var instance = this;
            var oldError = options.error;

            options.error = function (xhr) {
                instance._serverErrorHandler(xhr, this);
                if (oldError) {
                    oldError.apply(this, arguments);
                }
            };

            return Backbone.sync.apply(Backbone, arguments);
        },

        /**
         * Overrides default save handler to only save (send to server) attributes that have changed.
         * Also provides some default error handling.
         *
         * @override
         * @param attributes
         * @param options
         */
        save: function (attributes, options) {


            options = options || {};

            var instance = this,
                Model,
                syncModel,
                error = options.error, // we override, so store original
                success = options.success;


            // override error handler to provide some defaults
            options.error = function (model, xhr) {

                var data = $.parseJSON(xhr.responseText || xhr.data);

                // call original error handler
                if (error) {
                    error.call(instance, instance, data, xhr);
                }
            };

            // if it is a new model, we don't have to worry about updating only changed attributes because they are all new
            if (this.isNew()) {

                // call super
                Backbone.Model.prototype.save.call(this, attributes, options);

                // only go to server if something has changed
            } else if (attributes) {

                // create temporary model
                Model = AJS.RestfulTable.EntryModel.extend({
                    url: this.url()
                });

                syncModel = new Model({
                    id: this.id
                });

                syncModel.save = Backbone.Model.prototype.save;

                options.success = function (model, xhr) {

                    // update original model with saved attributes
                    instance.clear().set(model.toJSON());

                    // call original success handler
                    if (success) {
                        success.call(instance, instance, xhr);
                    }
                };

                // update temporary model with the changed attributes
                syncModel.save(attributes, options);
            }
        },

        /**
         * Destroys the model on the server. We need to override the default method as it does not support sending of
         * query paramaters.
         *
         * @override
         * @param options
         * ... {function} success - Server success callback
         * ... {function} error - Server error callback
         * ... {object} data
         *
         * @return AJS.RestfulTable.EntryModel
         */
        destroy: function (options) {

            options = options || {};

            var instance = this,
                url = this.url(),
                data;

            if (options.data) {
                data = $.param(options.data);
            }

            if (typeof data == "string" && data !== "") {
                // we need to add to the url as the data param does not work for jQuery DELETE requests
                url = url + "?" + data;
            }

            $.ajax({
                url: url,
                type: "DELETE",
                dataType: "json",
                contentType: "application/json",
                success: function (data) {
                    if(instance.collection){
                        instance.collection.remove(instance);
                    }
                    if (options.success) {
                        options.success.call(instance, data);
                    }
                },
                error: function (xhr) {
                    instance._serverErrorHandler(xhr, this);
                    if (options.error) {
                        options.error.call(instance, xhr);
                    }
                }
            });

            return this;
        },


        /**
         * A more complex lookup for changed attributes then default backbone one.
         *
         * @param attributes
         */
        changedAttributes: function (attributes) {

            var changed = {},
                current = this.toJSON();

            $.each(attributes, function (name, value) {

                if (!current[name]) {
                    if (typeof value === "string") {
                        if ($.trim(value) !== "") {
                            changed[name] = value;
                        }
                    } else if ($.isArray(value)) {
                        if (value.length !== 0) {
                            changed[name] = value;
                        }
                    } else {
                        changed[name] = value;
                    }
                } else if (current[name] && current[name] !== value) {

                    if (typeof value === "object") {
                        if (!_.isEqual(value, current[name])) {
                            changed[name] = value;
                        }
                    } else {
                        changed[name] = value;
                    }
                }
            });

            if (!_.isEmpty(changed)) {
                this.addExpand(changed);
                return changed;
            }
        },

        /**
         * Useful point to override if you always want to add an expand to your rest calls.
         *
         * @param changed attributes that have already changed
         */
        addExpand: function (changed){},

        /**
         * Throws a server error event unless user input validation error (status 400)
         *
         * @param xhr
         */
        _serverErrorHandler: function (xhr, ajaxOptions) {
            var data;
            if (xhr.status !== 400) {
                data = $.parseJSON(xhr.responseText || xhr.data);
                AJS.triggerEvtForInst(AJS.RestfulTable.Events.SERVER_ERROR, this, [data, xhr, ajaxOptions]);
            }
        },

        /**
         * Fetches values, with some generic error handling
         *
         * @override
         * @param options
         */
        fetch: function (options) {

            options = options || {};

            this.clear(); // clear the model, so we do not merge the old with the new

            // call super
            Backbone.Model.prototype.fetch.call(this, options);
        }
    });

})(AJS.$);

(function ($) {

    /**
     * An abstract class that gives the required behaviour for the creating and editing entries. Extend this class and pass
     * it as the {views.row} property of the options passed to AJS.RestfulTable in construction.
     *
     * @class EditRow
     * @namespace AJS.RestfulTable
     */
    AJS.RestfulTable.EditRow = Backbone.View.extend({

        tagName: "tr",

        // delegate events
        events: {
            "focusin" : "_focus",
            "click" : "_focus",
            "keyup" : "_handleKeyUpEvent"
        },

        /**
         * @constructor
         * @param {Object} options
         */
        initialize: function (options) {

            this.$el = $(this.el);

            // faster lookup
            this._event = AJS.RestfulTable.Events;
            this.classNames = AJS.RestfulTable.ClassNames;
            this.dataKeys = AJS.RestfulTable.DataKeys;
            this.columns = options.columns;
            this.isCreateRow = options.isCreateRow;

            this.allowReorder = options.allowReorder;

            // Allow cancelling an edit with support for setting a new element.
            this.events["click ." + this.classNames.CANCEL] = "_cancel";
            this.delegateEvents();

            if (options.isUpdateMode) {
                this.isUpdateMode = true;
            } else {
                this._modelClass = options.model;
                this.model = new this._modelClass();
            }

            this.fieldFocusSelector = options.fieldFocusSelector;

            this.bind(this._event.CANCEL, function () {
                this.disabled = true;
            })
                .bind(this._event.SAVE, function (focusUpdated) {
                    if (!this.disabled) {
                        this.submit(focusUpdated);
                    }
                })
                .bind(this._event.FOCUS, function (name) {
                    this.focus(name);
                })
                .bind(this._event.BLUR, function () {
                    this.$el.removeClass(this.classNames.FOCUSED);
                    this.disable();
                })
                .bind(this._event.SUBMIT_STARTED, function () {
                    this._submitStarted();
                })
                .bind(this._event.SUBMIT_FINISHED, function () {
                    this._submitFinished();
                });
        },

        /**
         * Renders default cell contents
         *
         * @param data
         */
        defaultColumnRenderer: function (data) {
            if (data.allowEdit !== false) {
                return $("<input type='text' />")
                    .addClass("text")
                    .attr({
                        name: data.name,
                        value: data.value
                    });
            } else if (data.value) {
                return document.createTextNode(data.value);
            }
        },

        /**
         * Renders drag handle
         * @return jQuery
         */
        renderDragHandle: function () {
            return '<span class="' + this.classNames.DRAG_HANDLE + '"></span></td>';

        },

        /**
         * Executes cancel event if ESC is pressed
         *
         * @param {Event} e
         */
        _handleKeyUpEvent: function (e) {
            if (e.keyCode === 27) {
                this.trigger(this._event.CANCEL);
            }
        },

        /**
         * Fires cancel event
         *
         * @param {Event} e
         * @return AJS.RestfulTable.EditRow
         */
        _cancel: function (e) {
            this.trigger(this._event.CANCEL);
            e.preventDefault();
            return this;
        },


        /**
         * Disables events/fields and adds safe guard against double submitting
         *
         * @return AJS.RestfulTable.EditRow
         */
        _submitStarted: function () {
            this.submitting = true;
            this.showLoading()
                .disable()
                .delegateEvents({});

            return this;
        },

        /**
         * Enables events & fields
         *
         * @return AJS.RestfulTable.EditRow
         */
        _submitFinished: function () {
            this.submitting = false;
            this.hideLoading()
                .enable()
                .delegateEvents(this.events);

            return this;
        },

        /**
         * Handles dom focus event, by only focusing row if it isn't already
         *
         * @param {Event} e
         * @return AJS.RestfulTable.EditRow
         */
        _focus: function (e) {
            if (!this.hasFocus()) {
                this.trigger(this._event.FOCUS, e.target.name);
            }
            return this;
        },


        /**
         * Returns true if row has focused class
         *
         * @return Boolean
         */
        hasFocus: function () {
            return this.$el.hasClass(this.classNames.FOCUSED);
        },

        /**
         * Focus specified field (by name or id - first argument), first field with an error or first field (DOM order)
         *
         * @param name
         * @return AJS.RestfulTable.EditRow
         */
        focus: function (name) {

            var $focus,
                $error;

            this.enable();

            if (name) {
                $focus = this.$el.find(this.fieldFocusSelector(name));
            } else {

                $error = this.$el.find(this.classNames.ERROR + ":first");

                if ($error.length === 0) {
                    $focus = this.$el.find(":input:text:first");
                } else {
                    $focus = $error.parent().find(":input");
                }
            }

            this.$el.addClass(this.classNames.FOCUSED);

//            if (this.$el.find(":input").isInView()) {
            $focus.focus().trigger("select");
//            }

            return this;
        },

        /**
         * Disables all fields
         *
         * @return AJS.RestfulTable.EditRow
         */
        disable: function () {

            var $replacementSubmit,
                $submit;

            // firefox does not allow you to submit a form if there are 2 or more submit buttons in a form, even if all but
            // one is disabled. It also does not let you change the type="submit' to type="button". Therfore he lies the hack.
            if ($.browser.mozilla) {

                $submit = this.$el.find(":submit");

                if ($submit.length) {

                    $replacementSubmit = $("<input type='submit' class='" + this.classNames.SUBMIT + "' />")
                        .addClass($submit.attr("class"))
                        .val($submit.val())
                        .data(this.dataKeys.ENABLED_SUBMIT, $submit);

                    $submit.replaceWith($replacementSubmit);
                }
            }

            this.$el.addClass(this.classNames.DISABLED)
                .find(":submit")
                .attr("disabled", "disabled");

            return this;
        },

        /**
         * Enables all fields
         *
         * @return AJS.RestfulTable.EditRow
         */
        enable: function () {

            var $placeholderSubmit,
                $submit;

            // firefox does not allow you to submit a form if there are 2 or more submit buttons in a form, even if all but
            // one is disabled. It also does not let you change the type="submit' to type="button". Therfore he lies the hack.
            if ($.browser.mozilla) {
                $placeholderSubmit = this.$el.find(this.classNames.SUBMIT),
                    $submit = $placeholderSubmit.data(this.dataKeys.ENABLED_SUBMIT);

                if ($submit && $placeholderSubmit.length) {
                    $placeholderSubmit.replaceWith($submit);
                }
            }


            this.$el.removeClass(this.classNames.DISABLED)
                .find(":submit")
                .removeAttr("disabled");

            return this;
        },

        /**
         * Shows loading indicator
         * @return AJS.RestfulTable.EditRow
         */
        showLoading: function () {
            this.$el.addClass(this.classNames.LOADING);
            return this;
        },

        /**
         * Hides loading indicator
         * @return AJS.RestfulTable.EditRow
         */
        hideLoading: function () {
            this.$el.removeClass(this.classNames.LOADING);
            return this;
        },

        /**
         * If any of the fields have changed
         * @return {Boolean}
         */
        hasUpdates: function () {
            return !!this.mapSubmitParams(this.serializeObject());
        },

        /**
         * Serializes the view into model representation.
         * Default implementation uses simple jQuery plugin to serialize form fields into object
         * @return Object
         */
        serializeObject: function() {
            return this.$el.serializeObject();
        },

        mapSubmitParams: function (params) {
            return this.model.changedAttributes(params);
        },

        /**
         *
         * Handle submission of new entries and editing of old.
         *
         * @param {Boolean} focusUpdated - flag of whether to focus read-only view after succssful submission
         * @return AJS.RestfulTable.EditRow
         */
        submit: function (focusUpdated) {


            var instance = this,
                values;

            // IE doesnt like it when the focused element is removed

            if (document.activeElement !== window) {
                $(document.activeElement).blur();
            }

            if (this.isUpdateMode) {

                values = this.mapSubmitParams(this.serializeObject()); // serialize form fields into JSON

                if (!values) {
                    return instance.trigger(instance._event.CANCEL);
                }
            } else {
                this.model.clear();
                values = this.mapSubmitParams(this.serializeObject()); // serialize form fields into JSON
            }

            this.trigger(this._event.SUBMIT_STARTED);

            /* Attempt to add to server model. If fail delegate to createView to render errors etc. Otherwise,
             add a new model to this._models and render a row to represent it. */
            this.model.save(values, {

                success: function () {

                    if (instance.isUpdateMode) {
                        instance.trigger(instance._event.UPDATED, instance.model, focusUpdated);
                    } else {
                        instance.trigger(instance._event.CREATED, instance.model.toJSON());
                        instance.model = new instance._modelClass(); // reset

                        instance.render({errors: {}, values: {}}); // pulls in instance's model for create row
                        instance.trigger(instance._event.FOCUS);
                    }

                    instance.trigger(instance._event.SUBMIT_FINISHED);
                },

                error: function (model, data, xhr) {

                    if (xhr.status === 400) {

                        instance.renderErrors(data.errors)
                            .trigger(instance._event.VALIDATION_ERROR, data.errors);
                    }

                    instance.trigger(instance._event.SUBMIT_FINISHED);
                },

                silent: true
            });

            return this;
        },
        /**
         * Render an error message
         * @param msg
         * @return {jQuery}
         */
        renderError: function (name, msg) {
            return $("<div />").attr("data-field", name).addClass(this.classNames.ERROR).text(msg);
        },

        /**
         * Render and append error messages. The property name will be matched to the input name to determine which cell to
         * append the error message to. If this does not meet your needs please extend this method.
         *
         * @param errors
         */
        renderErrors: function (errors) {

            var instance = this;

            this.$("." + this.classNames.ERROR).remove(); // avoid duplicates

            if (errors) {
                $.each(errors, function (name, msg) {
                    instance.$el.find("[name='" + name + "']")
                        .closest("td")
                        .append(instance.renderError(name, msg));
                });
            }

            return this;
        },


        /**
         * Handles rendering of row
         *
         * @param {Object} renderData
         * ... {Object} vales - Values of fields
         */
        render: function  (renderData) {

            var instance = this;

            this.$el.empty();

            if (this.allowReorder) {
                $('<td  class="' + this.classNames.ORDER + '" />').append(this.renderDragHandle()).appendTo(instance.$el);
            }

            $.each(this.columns, function (i, column) {

                var contents,
                    $cell,
                    value = renderData.values[column.id],
                    args = [
                        {name: column.id, value: value, allowEdit: column.allowEdit},
                        renderData.values,
                        instance.model
                    ];

                if (value) {
                    instance.$el.attr("data-" + column.id, value); // helper for webdriver testing
                }

                if (instance.isCreateRow && column.createView) {
                    // TODO AUI-1058 - The row's model should be guaranteed to be in the correct state by this point.
                    contents = new column.createView({
                        model: instance.model
                    }).render(args[0]);

                } else if (column.editView) {
                    contents = new column.editView({
                        model: instance.model
                    }).render(args[0]);
                } else {
                    contents = instance.defaultColumnRenderer.apply(instance, args);
                }

                $cell = $("<td />");

                if (typeof contents === "object" && contents.done) {
                    contents.done(function (contents) {
                        $cell.append(contents);
                    });
                } else {
                    $cell.append(contents);
                }

                if (column.styleClass) {
                    $cell.addClass(column.styleClass);
                }

                $cell.appendTo(instance.$el);
            });

            this.$el
                .append(this.renderOperations(renderData.update, renderData.values)) // add submit/cancel buttons
                .addClass(this.classNames.ROW + " " + this.classNames.EDIT_ROW);

            this.trigger(this._event.RENDER, this.$el, renderData.values);

            this.$el.trigger(this._event.CONTENT_REFRESHED, [this.$el]);

            return this;
        },

        /**
         *
         * Gets markup for add/update and cancel buttons
         *
         * @param {Boolean} update
         */
        renderOperations: function (update) {

            var $operations = $('<td class="aui-restfultable-operations" />');

            if (update) {
                $operations.append($('<input class="aui-button" type="submit" />').attr({
                        accesskey: this.submitAccessKey,
                        value: "Update"
                    }))
                    .append($('<a class="aui-button aui-button-link" href="#" />')
                        .addClass(this.classNames.CANCEL)
                        .text("Cancel")
                        .attr({
                            accesskey:  this.cancelAccessKey
                        }));
            } else {
                $operations.append($('<input class="aui-button" type="submit" />').attr({
                    accesskey: this.submitAccessKey,
                    value: "Add"
                }))
            }
            return $operations.add($('<td class="aui-restfultable-status" />').append(AJS.RestfulTable.throbber()));
        }
    });

})(AJS.$);

/*
 * Defining Custom renderer classes for people to extend.
 * We do this to
 * - Hide implementation (backbone)
 * - Future proof ourselves. We can modify peoples custom renderers easy in the future by adding to base class.
 */
AJS.RestfulTable.CustomEditView = AJS.RestfulTable.CustomCreateView = AJS.RestfulTable.CustomReadView = Backbone.View;

(function ($) {

    /**
     * An abstract class that gives the required behaviour for RestfulTable rows.
     * Extend this class and pass it as the {views.row} property of the options passed to AJS.RestfulTable in construction.
     *
     * @class Row
     * @namespace AJS.RestfulTable
     */
    AJS.RestfulTable.Row = Backbone.View.extend({

        // Static Const
        tagName: "tr",

        // delegate events
        events: {
            "click .aui-restfultable-editable" : "edit"
        },

        /**
         * @constructor
         * @param {object} options
         */
        initialize: function (options) {

            var instance = this;

            options = options || {};

            // faster lookup
            this._event = AJS.RestfulTable.Events;
            this.classNames = AJS.RestfulTable.ClassNames;
            this.dataKeys = AJS.RestfulTable.DataKeys;

            this.columns = options.columns;
            this.allowEdit = options.allowEdit;
            this.allowDelete = options.allowDelete;

            if (!this.events["click .aui-restfultable-editable"]) {
                throw new Error("It appears you have overridden the events property. To add events you will need to use"
                    + "a work around. https://github.com/documentcloud/backbone/issues/244")
            }
            this.index = options.index || 0;
            this.deleteConfirmation = options.deleteConfirmation;
            this.allowReorder = options.allowReorder;

            this.$el = $(this.el);

            this.bind(this._event.CANCEL, function () {
                this.disabled = true;
            })
                .bind(this._event.FOCUS, function (field) {
                    this.focus(field);
                })
                .bind(this._event.BLUR, function () {
                    this.unfocus();
                })
                .bind(this._event.UPDATED, function () {
                    this._showUpdated();
                })
                .bind(this._event.MODAL, function () {
                    this.$el.addClass(this.classNames.ACTIVE);
                })
                .bind(this._event.MODELESS, function () {
                    this.$el.removeClass(this.classNames.ACTIVE)
                });
        },

        /**
         * Renders drag handle
         * @return jQuery
         */
        renderDragHandle: function () {
            return '<span class="' + this.classNames.DRAG_HANDLE + '"></span></td>';

        },

        /**
         * Renders default cell contents
         *
         * @param data
         * @return {undefiend, String}
         */
        defaultColumnRenderer: function (data) {
            if (data.value) {
                return document.createTextNode(data.value.toString());
            }
        },

        /**
         * Fades row from blue to transparent
         */
        _showUpdated: function () {

            var instance = this,
                cells = this.$el
                    .addClass(this.classNames.ANIMATING)
                    .find("td")
                    .css("backgroundColor","#ebf1fd");

            this.trigger(this._event.ANIMATION_STARTED);

            instance.delegateEvents({});

            setTimeout(function () {
                cells.animate({
                    backgroundColor: "white"
                }, function () {
                    cells.css("backgroundColor", "");
                    instance.trigger(instance._event.ANIMATION_FINISHED);
                    $(document).one("mousemove", function () {
                        instance.delegateEvents();
                        instance.$el.removeClass(instance.classNames.ANIMATING);
                    });
                });
            }, 500)
        },

        /**
         * Save changed attributes back to server and re-render
         *
         * @param attr
         * @return {AJS.RestfulTable.Row}
         */
        sync: function (attr) {

            this.model.addExpand(attr);

            var instance = this;

            this.showLoading();

            this.model.save(attr, {
                success: function () {
                    instance.hideLoading().render();
                    instance.trigger(instance._event.UPDATED);
                },
                error: function () {
                    instance.hideLoading();
                }
            });

            return this;
        },

        /**
         * Get model from server and re-render
         *
         * @return {AJS.RestfulTable.Row}
         */
        refresh: function (success, error) {

            var instance = this;

            this.showLoading();

            this.model.fetch({
                success: function () {
                    instance.hideLoading().render();
                    if (success) {
                        success.apply(this, arguments);
                    }
                },
                error: function () {
                    instance.hideLoading();
                    if (error) {
                        error.apply(this, arguments);
                    }
                }
            });

            return this;
        },

        /**
         * Returns true if row has focused class
         *
         * @return Boolean
         */
        hasFocus: function () {
            return this.$el.hasClass(this.classNames.FOCUSED);
        },

        /**
         * Adds focus class (Item has been recently updated)
         *
         * @return AJS.RestfulTable.Row
         */
        focus: function () {
            $(this.el).addClass(this.classNames.FOCUSED);
            return this;
        },

        /**
         * Removes focus class
         *
         * @return AJS.RestfulTable.Row
         */
        unfocus: function () {
            $(this.el).removeClass(this.classNames.FOCUSED);
            return this;

        },

        /**
         * Adds loading class (to show server activity)
         *
         * @return AJS.RestfulTable.Row
         */
        showLoading: function () {
            this.$el.addClass(this.classNames.LOADING);
            return this;
        },

        /**
         * Hides loading class (to show server activity)
         *
         * @return AJS.RestfulTable.Row
         */
        hideLoading: function () {
            this.$el.removeClass(this.classNames.LOADING);
            return this;
        },

        /**
         * Switches row into edit mode
         *
         * @param e
         */
        edit: function (e) {
            var field;
            if ($(e.target).is("." + this.classNames.EDITABLE)) {
                field = $(e.target).attr("data-field-name");
            } else {
                field = $(e.target).closest("." + this.classNames.EDITABLE).attr("data-field-name");
            }
            this.trigger(this._event.ROW_EDIT, field);
            return this;
        },

        /**
         * Can be overriden to add custom options
         *
         */
        renderOperations: function () {
            var instance = this;
            if (this.allowDelete !== false) {
                return $("<a href='#' class='aui-button' />")
                    .addClass(this.classNames.DELETE)
                    .text("Delete").click(function (e) {
                        e.preventDefault();
                        instance.destroy();
                    });
            }
        },

        /**
         * Removes entry from table
         */
        destroy: function () {
            if (this.deleteConfirmation) {
                var popup = AJS.popup(400, 200, "delete-entity-" + this.model.get("id"));
                popup.element.html(this.deleteConfirmation(this.model.toJSON()));
                popup.show();
                popup.element.find(".cancel").click(function () {
                    popup.hide()
                });
                popup.element.find("form").submit(_.bind(function (e) {
                    popup.hide();
                    this.model.destroy();
                    e.preventDefault();
                }, this));
            } else {
                this.model.destroy();
            }

        },

        /**
         * Renders a generic edit row. You probably want to override this in a sub class.
         *
         * @return AJS.RestfulTable.Row
         */
        render: function  () {

            var instance = this,
                renderData = this.model.toJSON(),
                $opsCell = $("<td class='aui-restfultable-operations' />").append(this.renderOperations({}, renderData)),
                $throbberCell = $("<td class='aui-restfultable-status' />").append(AJS.RestfulTable.throbber());

            // restore state
            this.$el
                .removeClass(this.classNames.DISABLED + " " + this.classNames.FOCUSED + " " + this.classNames.LOADING + " " + this.classNames.EDIT_ROW)
                .addClass(this.classNames.READ_ONLY)
                .empty();


            if (this.allowReorder) {
                $('<td  class="' + this.classNames.ORDER + '" />').append(this.renderDragHandle()).appendTo(instance.$el);
            }

            this.$el.attr("data-id", this.model.id); // helper for webdriver testing

            $.each(this.columns, function (i, column) {

                var contents,
                    $cell = $("<td />"),
                    value = renderData[column.id],
                    fieldName = column.fieldName || column.id,
                    args = [{name: fieldName, value: value, allowEdit: column.allowEdit}, renderData, instance.model];

                if (value) {
                    instance.$el.attr("data-" + column.id, value); // helper for webdriver testing

                }

                if (column.readView) {
                    contents = new column.readView({
                        model: instance.model
                    }).render(args[0]);
                } else {
                    contents = instance.defaultColumnRenderer.apply(instance, args);
                }

                if (instance.allowEdit !== false && column.allowEdit !== false) {
                    var $editableRegion = $("<span />")
                        .addClass(instance.classNames.EDITABLE)
                        .append(aui.icons.icon({useIconFont: true, icon: 'edit'}))
                        .append(contents)
                        .attr("data-field-name", fieldName);

                    $cell  = $("<td />").append($editableRegion).appendTo(instance.$el);

                    if (!contents || $.trim(contents) == "") {
                        $cell.addClass(instance.classNames.NO_VALUE);
                        $editableRegion.html($("<em />").text(this.emptyText || "Enter value"));
                    }

                } else {
                    $cell.append(contents);
                }

                if (column.styleClass) {
                    $cell.addClass(column.styleClass);
                }

                $cell.appendTo(instance.$el);
            });

            this.$el
                .append($opsCell)
                .append($throbberCell)
                .addClass(this.classNames.ROW + " " + this.classNames.READ_ONLY);

            this.trigger(this._event.RENDER, this.$el, renderData);
            this.$el.trigger(this._event.CONTENT_REFRESHED, [this.$el]);
            return this;
        }
    });

})(AJS.$);


// Define AMD module.
define(function() {
    return AJS.RestfulTable;
});
