package com.atlassian.addon.connect.autowatch;

import com.atlassian.addon.connect.autowatch.service.AutoWatchModule;
import com.atlassian.addon.connect.autowatch.service.NewIssuesScannerService;
import com.google.inject.Guice;
import com.google.inject.Injector;
import play.Application;
import play.GlobalSettings;
import play.Logger;
import play.Play;
import play.libs.Akka;
import scala.concurrent.duration.Duration;

import java.util.concurrent.TimeUnit;

public class Global extends GlobalSettings
{
    private static final Injector INJECTOR = createInjector();

    @Override
    public void onStart(Application app)
    {
        Logger.info("Application has started");

        // TODO come up with some sensible values here
        final Integer interval = Play.application().configuration().getInt("autowatch.synchronise.interval");
        Logger.info(String.format("Setting synchronise interval to %d s", interval));
        final NewIssuesScannerService newIssuesScannerService = INJECTOR.getInstance(NewIssuesScannerService.class);
        Akka.system().scheduler().schedule(
                Duration.create(0, TimeUnit.MINUTES), //Initial delay
                Duration.create(interval, TimeUnit.SECONDS), //Refresh interval
                new Runnable()
                {
                    @Override
                    public void run()
                    {
                        try
                        {
                            newIssuesScannerService.synchronize();
                        }
                        catch (Exception e)
                        {
                            Logger.error("Error in synchronisation task", e);
                        }
                    }
                },
                Akka.system().dispatcher()
        );
    }

    @Override
    public void onStop(Application app)
    {
        Logger.info("Application shutdown...");
    }

    @Override
    public <A> A getControllerInstance(Class<A> controllerClass) throws Exception
    {
        return INJECTOR.getInstance(controllerClass);
    }

    private static Injector createInjector()
    {
        return Guice.createInjector(new AutoWatchModule());
    }
}
