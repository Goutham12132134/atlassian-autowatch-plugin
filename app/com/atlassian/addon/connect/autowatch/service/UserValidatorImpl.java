package com.atlassian.addon.connect.autowatch.service;

import com.atlassian.addon.connect.autowatch.model.JiraUser;
import com.atlassian.addon.connect.autowatch.util.ValidationErrors;
import com.atlassian.fugue.Option;

import java.util.List;

public class UserValidatorImpl implements UserValidator
{

    @Override
    public Option<ValidationErrors.ValidationError> validate(List<JiraUser> users)
    {
        if (users.isEmpty())
        {
            return Option.some(new ValidationErrors.ValidationError("users", "You must select at least one watcher."));
        }
        else
        {
            return Option.none();
        }
    }
}
