package com.atlassian.addon.connect.autowatch.service;

import com.atlassian.addon.connect.autowatch.model.JiraUser;
import com.atlassian.fugue.Option;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Supplier;
import com.google.common.collect.Iterables;
import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;

import java.util.List;
import javax.annotation.Nullable;

public class UserServiceImpl implements UserService
{
    @Override
    public List<JiraUser> getUsers(JsonNode jsonNode, String tenant)
    {
        if (jsonNode.has("users"))
        {
            final List<String> usersKeys = Lists.newArrayList(Iterators.transform(jsonNode.get("users").elements(), new Function<JsonNode, String>()
            {
                @Nullable
                @Override
                public String apply(JsonNode jsonNode)
                {
                    return jsonNode.asText();
                }
            }));

            final List<JiraUser> users = JiraUser.getUsers(usersKeys, tenant);
            final List<JiraUser> newUsers = JiraUser.saveUsers(Iterables.filter(usersKeys, new Predicate<String>()
            {
                @Override
                public boolean apply(final String userKey)
                {
                    return !Iterables.any(users, new Predicate<JiraUser>()
                    {
                        @Override
                        public boolean apply(final JiraUser jiraUser)
                        {
                            return jiraUser.key.equals(userKey);
                        }
                    });
                }
            }), tenant);
            return Lists.newArrayList(Iterables.concat(newUsers, users));
        }
        else
        {
            return Lists.newArrayList();
        }
    }

    @Override
    public JiraUser getUser(final String userKey, final String tenant)
    {
        final Option<JiraUser> user = JiraUser.getUser(userKey, tenant);
        return user.getOrElse(new Supplier<JiraUser>()
        {
            @Override
            public JiraUser get()
            {
                return Iterables.getOnlyElement(JiraUser.saveUsers(Lists.newArrayList(userKey), tenant));
            }
        });
    }
}
