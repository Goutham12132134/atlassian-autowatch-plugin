package com.atlassian.addon.connect.autowatch.util;


import com.atlassian.connect.play.java.AC;
import com.atlassian.connect.play.java.AcHost;
import com.atlassian.fugue.Option;
import play.libs.WS;

import java.util.concurrent.TimeUnit;

public class JiraClientImpl implements JiraClient
{
    private static final Long DEFAULT_TIMEOUT = TimeUnit.SECONDS.convert(5, TimeUnit.MILLISECONDS);
    private static final String USER_ID_QUERY_PARAMETER = "user_id";

    public WS.WSRequestHolder url(String url, AcHost acHost, Option<String> userId)
    {

        final String absoluteUrl = acHost.getBaseUrl() + url;
//
//
//        final WS.WSRequestHolder request = WS.url(absoluteUrl)
//                .setTimeout(DEFAULT_TIMEOUT.intValue())
//                .setFollowRedirects(false) // because we need to sign again in those cases.
//                .sign(new JwtSignatureCalculator());
//
//        if (userId.isDefined())
//        {
//            request.setQueryParameter(USER_ID_QUERY_PARAMETER, userId.get());
//        }
//        return request;
        return AC.url(absoluteUrl, acHost, userId);
    }
}
