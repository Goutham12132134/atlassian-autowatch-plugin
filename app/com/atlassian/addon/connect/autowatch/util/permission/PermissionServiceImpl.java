package com.atlassian.addon.connect.autowatch.util.permission;

import com.atlassian.addon.connect.autowatch.util.JiraClient;
import com.atlassian.connect.play.java.AcHost;
import com.atlassian.fugue.Option;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import org.apache.http.HttpStatus;
import play.libs.WS;

public class PermissionServiceImpl implements PermissionService
{
    private final JiraClient client;

    @Inject
    public PermissionServiceImpl(final JiraClient client)
    {
        this.client = client;
    }

    @Override
    public boolean hasAnyPermission(final String userId, final Iterable<Permission> permissions, final String projectKey, final AcHost host)
    {
        final WS.Response response = client.url("/rest/api/2/mypermissions", host, Option.some(userId)).get().get();
        if (response.getStatus() != HttpStatus.SC_OK)
        {
            return false;
        }
        final JiraPermissions jiraPermissions = JiraPermissions.fromJson(response.getBodyAsStream());

        System.out.println("jiraPermission " + jiraPermissions);

        return Iterables.any(permissions, new Predicate<Permission>()
        {
            @Override
            public boolean apply(final Permission permission)
            {
                final JiraPermissions.JiraPermission remotePermission = jiraPermissions.getPermission(permission.getJiraPermissionName());
                if (remotePermission != null)
                {
                    System.out.println("has permission " + permission.toString() + remotePermission.isHavePermission());
                }
                return remotePermission != null && remotePermission.isHavePermission();
            }
        });
    }
}
