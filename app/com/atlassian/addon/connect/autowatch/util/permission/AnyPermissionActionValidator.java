package com.atlassian.addon.connect.autowatch.util.permission;

import com.atlassian.connect.play.java.AC;
import com.atlassian.connect.play.java.AcHost;
import play.libs.F;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.SimpleResult;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;

public class AnyPermissionActionValidator extends Action<HasAnyPermission>
{
    private final String NO_PERMISSIONS_MESSAGE = "You do not have the required permission to view this page.";
    private final PermissionService permissionService;

    @Inject
    public AnyPermissionActionValidator(final PermissionService permissionService)
    {
        this.permissionService = permissionService;
    }

    @Override
    public F.Promise<SimpleResult> call(Http.Context context) throws Throwable
    {
        final List<Permission> permissions = getPermissions();
        final String userId = AC.getUser().getOrNull();
        final String projectKey = context.request().getQueryString("projectKey");
        final AcHost host = AC.getAcHost();
        if (host == null)
        {
            return F.Promise.promise(new F.Function0<SimpleResult>()
            {
                @Override
                public SimpleResult apply() throws Throwable
                {
                    return unauthorized(NO_PERMISSIONS_MESSAGE);
                }
            });
        }

        final boolean hasPermission = permissionService.hasAnyPermission(userId, permissions, projectKey, host);

        if (!hasPermission)
        {
            return F.Promise.promise(new F.Function0<SimpleResult>()
            {
                @Override
                public SimpleResult apply() throws Throwable
                {
                    return unauthorized(NO_PERMISSIONS_MESSAGE);
                }
            });
        }
        return delegate.call(context);
    }

    protected List<Permission> getPermissions()
    {
        return Arrays.asList(configuration.permissions());
    }
}
