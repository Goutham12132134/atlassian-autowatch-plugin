package com.atlassian.addon.connect.autowatch.util.permission;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Maps;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
public class JiraPermissions
{
    @JsonProperty
    private Map<String, JiraPermission> permissions = Maps.newHashMap();

    public JiraPermissions(@JsonProperty ("permissions") final Map<String, JiraPermission> permissions)
    {
        this.permissions = permissions;
    }

    public JiraPermission getPermission(final String permission)
    {
        System.out.println("getting permissions " + permission);
        System.out.println("permissions keys are " + permissions.keySet());
        System.out.println("returned " + permissions.get(permission));
        return permissions.get(permission);
    }

    @JsonIgnoreProperties (ignoreUnknown = true)
    public static class JiraPermission
    {
        @JsonProperty
        private String id;
        @JsonProperty
        private String key;
        @JsonProperty
        private String name;
        @JsonProperty
        private String description;
        @JsonProperty
        private boolean havePermission;

        @JsonCreator
        public JiraPermission(@JsonProperty ("id") final String id,
                @JsonProperty ("key") final String key,
                @JsonProperty ("name") final String name,
                @JsonProperty ("description") final String description,
                @JsonProperty ("havePermission") final boolean havePermission)
        {
            this.id = id;
            this.key = key;
            this.name = name;
            this.description = description;
            this.havePermission = havePermission;
        }

        public String getId()
        {
            return id;
        }

        public String getKey()
        {
            return key;
        }

        public String getName()
        {
            return name;
        }

        public String getDescription()
        {
            return description;
        }

        public boolean isHavePermission()
        {
            return havePermission;
        }

        @Override
        public String toString() {
            return "JiraPermission{" +
                    "id='" + id + '\'' +
                    ", key='" + key + '\'' +
                    ", name='" + name + '\'' +
                    ", description='" + description + '\'' +
                    ", havePermission=" + havePermission +
                    '}';
        }
    }

    public static JiraPermissions fromJson(final InputStream inputStream)
    {
        try
        {
            return new ObjectMapper().readValue(inputStream, JiraPermissions.class);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String toString() {
        return "JiraPermissions{" +
                "permissions=" + permissions +
                '}';
    }
}
