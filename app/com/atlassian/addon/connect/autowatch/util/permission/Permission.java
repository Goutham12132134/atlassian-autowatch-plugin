package com.atlassian.addon.connect.autowatch.util.permission;

public enum Permission
{
    MANAGE_WATCHER_LIST("MANAGE_WATCHERS"),
    PROJECT_ADMIN("ADMINISTER_PROJECTS"),
    ADMINISTER("ADMINISTER"),
    SYSTEM_ADMIN("SYSTEM_ADMIN");

    private final String jiraPermissionName;

    private Permission(String jiraPermissionName)
    {
        this.jiraPermissionName = jiraPermissionName;
    }

    public String getJiraPermissionName()
    {
        return jiraPermissionName;
    }
}
