package com.atlassian.addon.connect.autowatch.model;

import com.fasterxml.jackson.databind.JsonNode;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Issue
{

    public static final String JIRA_DATE_TIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    public static final String READABLE_DATE_TIME_PATTERN = "dd/MMM/yy K:mm a";
    public static final DateTimeFormatter JIRA_DATE_TIME_FORMATTER = DateTimeFormat.forPattern(JIRA_DATE_TIME_PATTERN);

    private final Long id;
    private final String key;
    private final Project project;
    private final String summary;
    private final String description;
    private final IconisedIssueField issueType;
    private final UserIssueField reporter;
    private final UserIssueField assignee;
    private final IconisedIssueField priority;
    private final String createdDate;

    private Issue(final Long id, final String key, final String summary, final Project project, final String description, IconisedIssueField type, UserIssueField reporter, UserIssueField assignee, IconisedIssueField priority, String createdDate)
    {
        this.id = id;
        this.key = key;
        this.summary = summary;
        this.project = project;
        this.description = description;
        this.issueType = type;
        this.reporter = reporter;
        this.assignee = assignee;
        this.priority = priority;
        this.createdDate = createdDate;
    }

    public String getUrl()
    {
        return "";
    }

    public Long getId()
    {
        return id;
    }

    public String getKey()
    {
        return key;
    }

    public Project getProject()
    {
        return project;
    }

    public UserIssueField getReporter()
    {
        return reporter;
    }

    public UserIssueField getAssignee()
    {
        return assignee;
    }

    public String getCreatedDate()
    {
        return createdDate;
    }

    public String getDescription()
    {
        return description;
    }

    public String getSummary()
    {
        return summary;
    }

    public IconisedIssueField getPriority()
    {
        return priority;
    }

    public IconisedIssueField getIssueType()
    {
        return issueType;
    }

    public static Issue toIssue(JsonNode issueJson)
    {
        final Long issueId = issueJson.get("id").asLong();
        final String key = saveText(issueJson, "key");

        final JsonNode fields = issueJson.get("fields");

        final Long projectId = fields.get("project").get("id").asLong();
        final String projectKey = fields.get("project").get("key").asText();
        final String projectName = fields.get("project").get("name").asText();
        final Project project = new Project(projectId, projectKey, projectName);

        final String summary = saveText(fields, "summary");
        final String desc = saveText(fields, "description");

        final JsonNode type = fields.get("issuetype");
        final String typeName = saveText(type, "name");
        final URI typeIconUrl = URI.create(saveText(type, "iconUrl"));
        final IconisedIssueField issueType = new IconisedIssueField(typeName, typeIconUrl);

        final JsonNode reporter = fields.get("reporter");
        final UserIssueField issueReporter;
        if (!reporter.isNull())
        {
            final String reporterName = saveText(reporter, "name");
            final String reporterDisplayName = saveText(reporter, "displayName");
            final URI reporterAvatarUrl = URI.create(reporter.get("avatarUrls").get("32x32").asText());
            issueReporter = new UserIssueField(reporterName, reporterDisplayName, reporterAvatarUrl);
        }
        else
        {
            issueReporter = null;
        }

        final JsonNode priority = fields.get("priority");
        IconisedIssueField issuePriority = null;
        if (priority != null)
        {
            final String priorityName = priority.get("name").asText();
            final URI priorityIconUrl = URI.create(priority.get("iconUrl").asText());
            issuePriority = new IconisedIssueField(priorityName, priorityIconUrl);
        }

        final JsonNode assignee = fields.get("assignee");
        final UserIssueField issueAssignee;
        if (!assignee.isNull())
        {
            final String assigneeName = assignee.get("name").asText();
            final String assigneeDisplayName = assignee.get("displayName").asText();
            final URI assigneeAvatarUrl = URI.create(assignee.get("avatarUrls").get("32x32").asText());
            issueAssignee = new UserIssueField(assigneeName, assigneeDisplayName, assigneeAvatarUrl);
        }
        else
        {
            issueAssignee = null;
        }

        final String createdDateString = saveText(fields, "created");
        final Date createdDate = JIRA_DATE_TIME_FORMATTER.parseDateTime(createdDateString).toDate();
        final String formattedCreatedDate = new SimpleDateFormat(READABLE_DATE_TIME_PATTERN).format(createdDate);

        return new Issue(issueId, key, summary, project, desc, issueType, issueReporter, issueAssignee, issuePriority, formattedCreatedDate);
    }

    private static String saveText(JsonNode node, String field)
    {
        if (node.has(field) && node.get(field).isTextual())
        {
            return node.get(field).asText();
        }
        return "";
    }

    public static class Project
    {
        private final String key;
        private final String name;
        private final long id;

        public Project(long id, String key, String name)
        {
            this.key = key;
            this.name = name;
            this.id = id;
        }

        public String getKey()
        {
            return key;
        }

        public String getName()
        {
            return name;
        }

        public long getId()
        {
            return id;
        }
    }


    public static class IconisedIssueField
    {
        private final String name;
        private final URI iconUrl;

        public IconisedIssueField(String name, URI iconUrl)
        {
            this.name = name;
            this.iconUrl = iconUrl;
        }

        public String getName()
        {
            return name;
        }

        public URI getIconUrl()
        {
            return iconUrl;
        }
    }

    public static class UserIssueField
    {
        private final String username;
        private final String displayName;
        private final URI iconUrl;

        public UserIssueField(String username, String displayName, URI iconUrl)
        {
            this.username = username;
            this.displayName = displayName;
            this.iconUrl = iconUrl;
        }

        public String getUsername()
        {
            return username;
        }

        public String getDisplayName()
        {
            return displayName;
        }

        public URI getIconUrl()
        {
            return iconUrl;
        }
    }
}




